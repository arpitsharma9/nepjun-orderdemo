(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-admin-admin-module"],{

/***/ "./src/app/admin/admin-menu.ts":
/*!*************************************!*\
  !*** ./src/app/admin/admin-menu.ts ***!
  \*************************************/
/*! exports provided: MENU_ITEMS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MENU_ITEMS", function() { return MENU_ITEMS; });
var MENU_ITEMS = [
    {
        title: 'Menu',
        icon: 'nb-list',
        link: '/admin/menu',
    },
    {
        title: 'Sales',
        icon: 'nb-bar-chart',
        link: '/admin/analytics',
        home: true,
    },
    {
        title: 'Orders',
        icon: 'nb-compose',
        link: '/admin/orders',
        home: true,
    },
    {
        title: 'Users',
        icon: 'nb-person',
        link: '/admin/users',
    },
    {
        title: 'Settings',
        icon: 'nb-gear',
        link: '/admin/settings',
    },
];


/***/ }),

/***/ "./src/app/admin/admin-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/admin/admin-routing.module.ts ***!
  \***********************************************/
/*! exports provided: AdminRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRoutingModule", function() { return AdminRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./users/users.component */ "./src/app/admin/users/users.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/admin/settings/settings.component.ts");
/* harmony import */ var _analytics_analytics_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../analytics/analytics.component */ "./src/app/analytics/analytics.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _admin_component__WEBPACK_IMPORTED_MODULE_2__["AdminComponent"],
        children: [{
                path: 'analytics',
                component: _analytics_analytics_component__WEBPACK_IMPORTED_MODULE_5__["AnalyticsComponent"],
            }, {
                path: 'users',
                component: _users_users_component__WEBPACK_IMPORTED_MODULE_3__["UsersComponent"],
            }, {
                path: 'menu',
                loadChildren: 'app/menu/menu.module#MenuModule'
            }, {
                path: 'settings',
                component: _settings_settings_component__WEBPACK_IMPORTED_MODULE_4__["SettingsComponent"],
            }, {
                path: 'orders',
                loadChildren: 'app/orders/orders.module#OrdersModule'
            }, {
                path: '**',
                component: _users_users_component__WEBPACK_IMPORTED_MODULE_3__["UsersComponent"],
            }],
    }
];
var AdminRoutingModule = /** @class */ (function () {
    function AdminRoutingModule() {
    }
    AdminRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AdminRoutingModule);
    return AdminRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/admin.component.css":
/*!*******************************************!*\
  !*** ./src/app/admin/admin.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/admin.component.html":
/*!********************************************!*\
  !*** ./src/app/admin/admin.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<food-ordering-layout>\r\n  <nb-menu [items]=\"menu\"></nb-menu>\r\n  <router-outlet></router-outlet>\r\n</food-ordering-layout>"

/***/ }),

/***/ "./src/app/admin/admin.component.ts":
/*!******************************************!*\
  !*** ./src/app/admin/admin.component.ts ***!
  \******************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _admin_menu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./admin-menu */ "./src/app/admin/admin-menu.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AdminComponent = /** @class */ (function () {
    function AdminComponent() {
        this.menu = _admin_menu__WEBPACK_IMPORTED_MODULE_1__["MENU_ITEMS"];
    }
    AdminComponent.prototype.ngOnInit = function () {
    };
    AdminComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin',
            template: __webpack_require__(/*! ./admin.component.html */ "./src/app/admin/admin.component.html"),
            styles: [__webpack_require__(/*! ./admin.component.css */ "./src/app/admin/admin.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AdminComponent);
    return AdminComponent;
}());



/***/ }),

/***/ "./src/app/admin/admin.module.ts":
/*!***************************************!*\
  !*** ./src/app/admin/admin.module.ts ***!
  \***************************************/
/*! exports provided: AdminModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _admin_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-routing.module */ "./src/app/admin/admin-routing.module.ts");
/* harmony import */ var ngx_echarts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-echarts */ "./node_modules/ngx-echarts/ngx-echarts.es5.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/index.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/admin/dashboard/dashboard.component.ts");
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./users/users.component */ "./src/app/admin/users/users.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/admin/settings/settings.component.ts");
/* harmony import */ var _admin_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _users_drop_down_drop_down_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./users/drop-down/drop-down.component */ "./src/app/admin/users/drop-down/drop-down.component.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _analytics_analytics_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../analytics/analytics.module */ "./src/app/analytics/analytics.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_9__["ThemeModule"],
                _admin_routing_module__WEBPACK_IMPORTED_MODULE_2__["AdminRoutingModule"],
                ngx_echarts__WEBPACK_IMPORTED_MODULE_3__["NgxEchartsModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_10__["Ng2SmartTableModule"],
                _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_4__["NgxChartsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_13__["FileUploadModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatCheckboxModule"],
                _analytics_analytics_module__WEBPACK_IMPORTED_MODULE_15__["AnalyticsModule"]
            ],
            declarations: [
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"],
                _users_users_component__WEBPACK_IMPORTED_MODULE_6__["UsersComponent"],
                _settings_settings_component__WEBPACK_IMPORTED_MODULE_7__["SettingsComponent"],
                _admin_component__WEBPACK_IMPORTED_MODULE_8__["AdminComponent"],
                _users_drop_down_drop_down_component__WEBPACK_IMPORTED_MODULE_12__["DropDownComponent"]
            ],
            entryComponents: [
                _users_drop_down_drop_down_component__WEBPACK_IMPORTED_MODULE_12__["DropDownComponent"],
            ]
        })
    ], AdminModule);
    return AdminModule;
}());



/***/ }),

/***/ "./src/app/admin/dashboard/dashboard.component.css":
/*!*********************************************************!*\
  !*** ./src/app/admin/dashboard/dashboard.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".page-dashboard {\r\n    position: relative;\r\n}\r\n.page-dashboard #sales {\r\n    font-size: 30px;\r\n    border-bottom: 3px solid;\r\n}\r\n.page-dashboard .tab-item {\r\n    float: right;\r\n    cursor: pointer;\r\n    margin-left: 20px;\r\n}\r\n.page-dashboard .active {\r\n    color: green;\r\n}\r\n.page-dashboard .price {\r\n    position: absolute;\r\n    top: 50%; \r\n    -webkit-transform: translate(0%, -50%); \r\n            transform: translate(0%, -50%); \r\n    padding-left: 10px;\r\n    width: 90%;\r\n}\r\n.top-selling-item {\r\n    margin-left: 5px;\r\n}\r\n.top-selling-item div {\r\n    margin-bottom: 10px;\r\n    font-size: 15px;\r\n    padding: 0;\r\n}\r\n.page-dashboard nb-card-body {\r\n    overflow: none;\r\n}\r\n.flex-space-between {\r\n    display: flex;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n}\r\n.flex-start {\r\n    display: flex;\r\n    justify-content: flex-start;\r\n    align-items: center;\r\n}\r\np {\r\n    margin-bottom: 0;\r\n    font-size: 14px;\r\n    margin-right: 5px;\r\n}\r\n.p-bold {\r\n    font-weight: 600;\r\n    color: #5b5b61;\r\n    font-size: 15px;\r\n}\r\n.item-count {\r\n    background-color: #9b92ff;\r\n    border-radius: 50%;\r\n    color: white;\r\n    padding: 2px;\r\n    font-size: 13px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/admin/dashboard/dashboard.component.html":
/*!**********************************************************!*\
  !*** ./src/app/admin/dashboard/dashboard.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12 col-lg-12 page-dashboard\">\r\n  <span id=\"sales\">Sales</span>\r\n  <span class=\"tab-item active\" [class.active]=\"active_tab_item == 1\" (click)=\"onClickTabItem(1)\">30 Days</span>\r\n  <span class=\"tab-item active\" [class.active]=\"active_tab_item == 0\" (click)=\"onClickTabItem(0)\">7 Days</span>\r\n</div><br>\r\n\r\n<!-- <custom-menu></custom-menu> -->\r\n\r\n<nb-card class=\"page-dashboard\">\r\n  <nb-card-body>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-3\">\r\n        <div class=\"flex-start\">\r\n            <p class=\"p-bold\">Subtotal: </p>\r\n            <p>${{ subtotal.toFixed(2) }}</p>\r\n        </div>\r\n        <div class=\"flex-start\">\r\n            <p class=\"p-bold\">Tax: </p>\r\n            <p>${{ tax.toFixed(2) }}</p>\r\n        </div>\r\n        <div class=\"flex-start\">\r\n            <p class=\"p-bold\">Service Fee: </p>\r\n            <p>${{ serviceFee }}</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <div echarts [options]=\"chartOptions\" class=\"echart\"></div>\r\n      </div>\r\n    </div><br><br>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\"><h3 style=\"font-size: 16px;font-weight: 600;color: #5b5b61;\">Top Selling Menu Items</h3></div>\r\n    </div>\r\n    <div class=\"row top-selling-item\">\r\n      <div *ngFor=\"let item of topSellingItems\" class=\"col-md-4\">\r\n        <span class=\"item-count\">{{ item.qty }}</span>\r\n        {{ item.name }}\r\n      </div>\r\n    </div>\r\n  </nb-card-body>\r\n</nb-card>\r\n<br><br>"

/***/ }),

/***/ "./src/app/admin/dashboard/dashboard.component.ts":
/*!********************************************************!*\
  !*** ./src/app/admin/dashboard/dashboard.component.ts ***!
  \********************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../@nebular/theme */ "./src/app/@nebular/theme/index.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(apiService, theme, cookieService, router) {
        this.apiService = apiService;
        this.theme = theme;
        this.cookieService = cookieService;
        this.router = router;
        this.chartOptions = {};
        this.active_tab_item = 0;
        this.totalPrice = 0;
        this.subtotal = 0;
        this.tax = 0;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_5__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
    };
    DashboardComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.themeSubscription = this.theme.getJsTheme().subscribe(function (config) {
            _this.colors = config.variables;
            _this.echarts = config.variables.echarts;
            _this.getStatistic();
            _this.getSoldItems();
        });
    };
    DashboardComponent.prototype.ngOnDestroy = function () {
        this.themeSubscription.unsubscribe();
    };
    DashboardComponent.prototype.onClickTabItem = function (index) {
        this.active_tab_item = index;
        this.getStatistic();
        this.getSoldItems();
    };
    DashboardComponent.prototype.getStatistic = function () {
        var _this = this;
        this.apiService.getStatistics(this.currentUser.restaurant.id, this.active_tab_item == 0 ? 7 : 30).subscribe(function (res) {
            _this.topSellingItems = res.response;
            if (_this.topSellingItems.length > 6) {
                _this.topSellingItems.slice(6, _this.topSellingItems.length - 6);
            }
        });
    };
    DashboardComponent.prototype.getSoldItems = function () {
        var _this = this;
        var duration = this.active_tab_item == 0 ? 7 : 30;
        this.apiService.getSoldItems(this.currentUser.restaurant.id, duration).subscribe(function (res) {
            _this.allSoldItemsByDate = [];
            for (var i = 0; i < duration; i++) {
                var date = new Date(Date.now() - i * 24 * 3600 * 1000);
                var strDate = (date.getMonth() + 1) + "/" + date.getDate();
                _this.allSoldItemsByDate.unshift({ date: strDate, price: 0, tax: 0, serviceFee: 0 });
            }
            var totalPrice = 0;
            var serviceFee = 0;
            var _loop_1 = function (item) {
                var date = new Date(item.created_at * 1000);
                var strDate = (date.getMonth() + 1) + "/" + date.getDate();
                var itemSubtotal = item.amounts * item.price;
                var itemTax = item.amounts * item.tax;
                var itemServiceFee = itemSubtotal * _this.currentUser.restaurant.booking_fee / 100;
                totalPrice += _this.subtotal + _this.tax;
                serviceFee += itemServiceFee;
                var index = _this.allSoldItemsByDate.findIndex(function (x) { return x.date === strDate; });
                if (index > -1) {
                    _this.allSoldItemsByDate[index].price += itemSubtotal;
                    _this.allSoldItemsByDate[index].tax += itemTax;
                    _this.allSoldItemsByDate[index].serviceFee += itemServiceFee;
                }
            };
            for (var _i = 0, _a = res.response; _i < _a.length; _i++) {
                var item = _a[_i];
                _loop_1(item);
            }
            _this.totalPrice = totalPrice.toFixed(2);
            _this.serviceFee = serviceFee.toFixed(2);
            _this.chartOptions = {
                legend: {
                    data: ['Item Subtotal', 'Service Fee', 'Item Tax'],
                    align: 'left'
                },
                backgroundColor: _this.echarts.bg,
                // color: [this.colors.primaryLight],
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow',
                    },
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true,
                },
                xAxis: [
                    {
                        type: 'category',
                        data: [],
                        axisTick: {
                            alignWithLabel: true,
                        },
                        axisLine: {
                            lineStyle: {
                                color: _this.echarts.axisLineColor,
                            },
                        },
                        axisLabel: {
                            textStyle: {
                                color: _this.echarts.textColor,
                            },
                        },
                    },
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLine: {
                            lineStyle: {
                                color: _this.echarts.axisLineColor,
                            },
                        },
                        splitLine: {
                            lineStyle: {
                                color: _this.echarts.splitLineColor,
                            },
                        },
                        axisLabel: {
                            textStyle: {
                                color: _this.echarts.textColor,
                            },
                        },
                    },
                ],
                series: [
                    {
                        name: 'Item Subtotal',
                        type: 'bar',
                        barWidth: '15%',
                        data: [],
                        animationDelay: function (idx) {
                            return idx * 10;
                        },
                        color: _this.colors.primaryLight,
                    },
                    {
                        name: 'Service Fee',
                        type: 'bar',
                        barWidth: '15%',
                        data: [],
                        animationDelay: function (idx) {
                            return idx * 10 + 100;
                        },
                        color: '#f9cfad',
                    },
                    {
                        name: 'Item Tax',
                        type: 'bar',
                        barWidth: '15%',
                        data: [],
                        animationDelay: function (idx) {
                            return idx * 10 + 100;
                        },
                        color: '#6c757d',
                    },
                ],
            };
            _this.subtotal = 0;
            _this.tax = 0;
            for (var _b = 0, _c = _this.allSoldItemsByDate; _b < _c.length; _b++) {
                var item = _c[_b];
                _this.chartOptions.xAxis[0].data.push(item.date);
                _this.chartOptions.series[0].data.push(item.price.toFixed(2));
                _this.chartOptions.series[1].data.push(item.serviceFee.toFixed(2));
                _this.chartOptions.series[2].data.push(item.tax.toFixed(2));
                _this.subtotal += item.price;
                _this.tax += item.tax;
            }
        });
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/admin/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/admin/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], _nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbThemeService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__["CookieService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/admin/settings/settings.component.css":
/*!*******************************************************!*\
  !*** ./src/app/admin/settings/settings.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/settings/settings.component.html":
/*!********************************************************!*\
  !*** ./src/app/admin/settings/settings.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <nb-card>\r\n  <nb-card-header>\r\n    Bank Information\r\n  </nb-card-header>\r\n\r\n  <nb-card-body>\r\n    <form [formGroup]=\"bankForm\" (ngSubmit)=\"submitBankInfo()\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n          <div class=\"form-group\">\r\n            <label for=\"inputAccount\">Account #</label>\r\n            <input type=\"text\" formControlName=\"account\" class=\"form-control\" [ngClass]=\"{'form-control-danger': bankForm.controls.account.invalid && (bankForm.controls.account.dirty || bankForm.controls.account.touched)}\" id=\"inputAccount\" placeholder=\"12345678\">\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <div class=\"form-group\">\r\n            <label for=\"inputRoutingNumber\">Routing Number</label>\r\n            <input type=\"text\" formControlName=\"routingNumber\" class=\"form-control\" [ngClass]=\"{'form-control-danger': bankForm.controls.routingNumber.invalid && (bankForm.controls.routingNumber.dirty || bankForm.controls.routingNumber.touched)}\" id=\"inputRoutingNumber\" placeholder=\"0000000\">\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <button type=\"submit\" class=\"btn btn-success btn-semi-round\">Submit</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </nb-card-body>\r\n</nb-card> -->\r\n\r\n<nb-card>\r\n    <nb-card-header>\r\n      Preparation Time (minutes)\r\n    </nb-card-header>\r\n  \r\n    <nb-card-body>\r\n      <form [formGroup]=\"prepTimeForm\" (ngSubmit)=\"submitPrepTimeInfo()\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-12\">\r\n            <div class=\"form-group\">\r\n              <input type=\"number\" formControlName=\"prepTime\" class=\"form-control\" [ngClass]=\"{'form-control-danger': prepTimeForm.controls.prepTime.invalid && (prepTimeForm.controls.prepTime.dirty || prepTimeForm.controls.prepTime.touched)}\" id=\"inputPreparationTime\" placeholder=\"0\" [value]=\"restaurant.prepare_time\">\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-12\">\r\n            <button type=\"submit\" class=\"btn btn-info btn-semi-round\">Submit</button>\r\n          </div>\r\n        </div>\r\n      </form>\r\n    </nb-card-body>\r\n  </nb-card>\r\n\r\n  <nb-card>\r\n      <nb-card-header>\r\n        Restaurant Status\r\n      </nb-card-header>\r\n    \r\n      <nb-card-body>\r\n          <ngx-switcher\r\n          [firstValue]=\"true\"\r\n          [secondValue]=\"false\"\r\n          [firstValueLabel]=\"'Available'\"\r\n          [secondValueLabel]=\"'Not available'\"\r\n          [value]=\"restaurantStatus()\"\r\n          (valueChange)=\"toggleRestaurantStatus($event)\"\r\n        >\r\n        </ngx-switcher>\r\n      </nb-card-body>\r\n  </nb-card>\r\n\r\n  <!-- <nb-card>\r\n      <nb-card-header>\r\n        Menu Type\r\n      </nb-card-header>\r\n    \r\n      <nb-card-body>\r\n          <ngx-switcher\r\n          [firstValue]=\"true\"\r\n          [secondValue]=\"false\"\r\n          [firstValueLabel]=\"'No Static'\"\r\n          [secondValueLabel]=\"'Static'\"\r\n          [value]=\"transactionStatus()\"\r\n          (valueChange)=\"toggleTransactionStatus($event)\"\r\n        >\r\n        </ngx-switcher>\r\n      </nb-card-body>\r\n  </nb-card> -->"

/***/ }),

/***/ "./src/app/admin/settings/settings.component.ts":
/*!******************************************************!*\
  !*** ./src/app/admin/settings/settings.component.ts ***!
  \******************************************************/
/*! exports provided: SettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsComponent", function() { return SettingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SettingsComponent = /** @class */ (function () {
    function SettingsComponent(fb, toastrService, cookieService, apiService, router) {
        this.fb = fb;
        this.toastrService = toastrService;
        this.cookieService = cookieService;
        this.apiService = apiService;
        this.router = router;
    }
    SettingsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.restaurant = new _models__WEBPACK_IMPORTED_MODULE_7__["Restaurant"]();
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
        this.bankForm = this.fb.group({
            account: [this.currentUser.restaurant.bank_account ? this.currentUser.restaurant.bank_account.account_number : "", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(1)]],
            routingNumber: [this.currentUser.restaurant.bank_account ? this.currentUser.restaurant.bank_account.routing_number : "", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(1)]],
        });
        this.prepTimeForm = this.fb.group({
            prepTime: [this.restaurant.prepare_time, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(1)]],
        });
        this.apiService.getRestaurant(this.currentUser.restaurant_id).subscribe(function (res) {
            // console.log(res);
            _this.restaurant = res.response[0];
        });
    };
    SettingsComponent.prototype.submitBankInfo = function () {
        var _this = this;
        if (this.bankForm.controls.account.invalid)
            return;
        if (this.bankForm.controls.routingNumber.invalid)
            return;
        this.apiService.addBankAccount(this.currentUser.restaurant.id, this.bankForm.value.account, this.bankForm.value.routingNumber, this.currentUser.username).subscribe(function (res) {
            if (res) {
                if (res.err) {
                    _this.toastrService.error('Bank was not added!');
                }
                else {
                    _this.toastrService.info('Bank was added successfully!');
                    _this.currentUser.restaurant.bank_account = res.response;
                    _this.cookieService.set('user', _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].encodeJwt(_this.currentUser), null, '/');
                }
            }
            else {
                _this.toastrService.error('Bank was not added!');
            }
        });
    };
    SettingsComponent.prototype.submitPrepTimeInfo = function () {
        var _this = this;
        if (this.prepTimeForm.controls.prepTime.invalid)
            return;
        var preptime = this.prepTimeForm.value.prepTime;
        this.apiService.updateRestaurantPrepareTime(this.currentUser.restaurant.id, preptime).subscribe(function (res) {
            if (res) {
                if (res.err) {
                    _this.toastrService.error('Restaurant Prepration time was not updated');
                }
                else {
                    _this.toastrService.info('Restaurant Prepration time was updated successfully');
                    _this.currentUser.restaurant.prepare_time = preptime;
                    _this.cookieService.set('user', _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].encodeJwt(_this.currentUser), null, '/');
                }
            }
            else {
                _this.toastrService.error('Restaurant Prepration time was not updated');
            }
        });
    };
    SettingsComponent.prototype.toggleRestaurantStatus = function (event) {
        var _this = this;
        var status = event ? 0 : 1;
        this.apiService.updateRestaurantStatus(this.currentUser.restaurant.id, status).subscribe(function (res) {
            if (res) {
                if (res.err) {
                    _this.toastrService.error('Restaurant Status was not updated');
                }
                else {
                    _this.toastrService.info('Restaurant Status was updated successfully');
                    _this.currentUser.restaurant.status = status;
                    _this.cookieService.set('user', _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].encodeJwt(_this.currentUser), null, '/');
                }
            }
            else {
                _this.toastrService.error('Restaurant Status was not updated');
            }
        });
    };
    SettingsComponent.prototype.restaurantStatus = function () {
        return this.restaurant.status === 0 ? true : false;
    };
    SettingsComponent.prototype.toggleTransactionStatus = function (event) {
        var _this = this;
        var status = event ? 'Yes' : 'No';
        this.apiService.updateTransactionStatus(this.currentUser.restaurant.id, status, this.restaurant.map_pin_id).subscribe(function (res) {
            if (res) {
                if (res.err) {
                    _this.toastrService.error('Menu type was not updated');
                }
                else {
                    _this.toastrService.info('Menu type was updated successfully');
                    _this.currentUser.restaurant.status = status;
                    _this.cookieService.set('user', _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].encodeJwt(_this.currentUser), null, '/');
                }
            }
            else {
                _this.toastrService.error('Menu type was not updated');
            }
        });
    };
    SettingsComponent.prototype.transactionStatus = function () {
        return (this.restaurant.tnx_enabled === 'Yes') ? true : false;
    };
    SettingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(/*! ./settings.component.html */ "./src/app/admin/settings/settings.component.html"),
            styles: [__webpack_require__(/*! ./settings.component.css */ "./src/app/admin/settings/settings.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"],
            _services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], SettingsComponent);
    return SettingsComponent;
}());



/***/ }),

/***/ "./src/app/admin/users/drop-down/drop-down.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/admin/users/drop-down/drop-down.component.ts ***!
  \**************************************************************/
/*! exports provided: DropDownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropDownComponent", function() { return DropDownComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DropDownComponent = /** @class */ (function () {
    function DropDownComponent() {
    }
    DropDownComponent.prototype.ngOnInit = function () {
        this.renderValue = this.value;
    };
    DropDownComponent.prototype.example = function () {
        alert(this.rowData.id);
    };
    DropDownComponent.prototype.toggled = function (event) {
        console.log(this.renderValue);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DropDownComponent.prototype, "value", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DropDownComponent.prototype, "rowData", void 0);
    DropDownComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: "\n  <div class=\"dropdown\" ngbDropdown (openChange)=\"toggled($event)\">\n  <button class=\"btn btn-primary\" type=\"button\" ngbDropdownToggle>\n    Dropdown\n  </button>\n  <ul class=\"dropdown-menu\" ngbDropdownMenu>\n    <li class=\"dropdown-item\">Icon Button</li>\n    <li class=\"dropdown-item\">Hero Button</li>\n    <li class=\"dropdown-item\">Default</li>\n  </ul>\n</div>\n  ",
        }),
        __metadata("design:paramtypes", [])
    ], DropDownComponent);
    return DropDownComponent;
}());



/***/ }),

/***/ "./src/app/admin/users/users.component.html":
/*!**************************************************!*\
  !*** ./src/app/admin/users/users.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\r\n    <nb-card-header>\r\n      Users\r\n    </nb-card-header>\r\n  \r\n    <nb-card-body>\r\n      <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (editConfirm)=\"onSaveConfirm($event)\" (createConfirm)=\"onCreateConfirm($event)\"></ng2-smart-table>\r\n    </nb-card-body>\r\n  </nb-card>"

/***/ }),

/***/ "./src/app/admin/users/users.component.scss":
/*!**************************************************!*\
  !*** ./src/app/admin/users/users.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  font-size: 1rem; }\n  :host /deep/ * {\n    box-sizing: border-box; }\n  :host /deep/ ng2-smart-table tr > td select {\n    height: calc(2rem + 10px); }\n"

/***/ }),

/***/ "./src/app/admin/users/users.component.ts":
/*!************************************************!*\
  !*** ./src/app/admin/users/users.component.ts ***!
  \************************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UsersComponent = /** @class */ (function () {
    function UsersComponent(api_service, toastrService, cookieService, router) {
        var _this = this;
        this.api_service = api_service;
        this.toastrService = toastrService;
        this.cookieService = cookieService;
        this.router = router;
        this.settings = {
            mode: 'inline',
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmCreate: true,
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmSave: true,
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            columns: {
                first_name: {
                    title: 'First Name',
                    type: 'string',
                },
                last_name: {
                    title: 'Last Name',
                    type: 'string',
                },
                username: {
                    title: 'Username',
                    type: 'string',
                    sort: true,
                },
                password: {
                    title: 'Password',
                    type: 'string',
                },
                role: {
                    title: 'Role',
                    type: 'html',
                    valuePrepareFunction: function (role) {
                        if (role == 0)
                            return 'Client';
                        if (role == 1)
                            return 'Manager';
                        if (role == 2)
                            return 'Front Desk';
                    },
                    editor: {
                        type: 'list',
                        config: {
                            list: [
                                { title: 'Manager', value: '1' },
                                { title: 'Front Desk', value: '2' }
                            ]
                        }
                    }
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
        this.api_service.getChildren(this.currentUser.restaurant_id).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    _this.source.load(res.response);
                }
                else {
                    _this.toastrService.error('Cannot fetch users!');
                }
            }
            else {
                _this.toastrService.error('Cannot fetch users!');
            }
        });
    }
    UsersComponent.prototype.ngOnInit = function () {
    };
    UsersComponent.prototype.onDeleteConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to delete???')) {
            this.api_service.deleteUser(event.data['id']).subscribe(function (res) {
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Successfully deleted!');
                        event.confirm.resolve();
                    }
                    else {
                        _this.toastrService.error(res.msg);
                        event.confirm.reject();
                    }
                }
                else {
                    _this.toastrService.error('"Delete user" was failed!');
                    event.confirm.reject();
                }
            });
        }
        else {
            event.confirm.reject();
        }
    };
    UsersComponent.prototype.onSaveConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to save changes ?')) {
            var username = event.newData['username'];
            if (username === '') {
                this.toastrService.error('Username field is empty!');
                event.confirm.reject();
                return;
            }
            var first_name = event.newData['first_name'];
            if (first_name === '') {
                this.toastrService.error('First Name field is empty!');
                event.confirm.reject();
                return;
            }
            var last_name = event.newData['last_name'];
            if (last_name === '') {
                this.toastrService.error('Last Name field is empty!');
                event.confirm.reject();
                return;
            }
            var email = event.newData['email'];
            if (email === '') {
                this.toastrService.error('Email field is empty!');
                event.confirm.reject();
                return;
            }
            var password = event.newData['password'];
            if (password === '') {
                this.toastrService.error('Password field is empty!');
                event.confirm.reject();
                return;
            }
            var role = event.newData['role'];
            if (role === '') {
                this.toastrService.error('Role field is empty!');
                event.confirm.reject();
                return;
            }
            this.api_service.updateUser(event.newData).subscribe(function (res) {
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Successfully updated!');
                        event.confirm.resolve();
                    }
                    else {
                        _this.toastrService.error(res.msg);
                        event.confirm.reject();
                    }
                }
                else {
                    _this.toastrService.error('Update failed!');
                    event.confirm.reject();
                }
            });
        }
        else {
            event.confirm.reject();
        }
    };
    UsersComponent.prototype.onCreateConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to create this user?')) {
            var username = event.newData['username'];
            if (username === '') {
                this.toastrService.error('Username field is empty!');
                event.confirm.reject();
                return;
            }
            var first_name = event.newData['first_name'];
            if (first_name === '') {
                this.toastrService.error('First Name field is empty!');
                event.confirm.reject();
                return;
            }
            var last_name = event.newData['last_name'];
            if (last_name === '') {
                this.toastrService.error('Last Name field is empty!');
                event.confirm.reject();
                return;
            }
            var email = event.newData['email'];
            if (email === '') {
                this.toastrService.error('Email field is empty!');
                event.confirm.reject();
                return;
            }
            var password = event.newData['password'];
            if (password === '') {
                this.toastrService.error('Password field is empty!');
                event.confirm.reject();
                return;
            }
            var role = event.newData['role'];
            if (role === '') {
                this.toastrService.error('Role field is empty!');
                event.confirm.reject();
                return;
            }
            var user = event.newData;
            user.restaurant_id = this.currentUser.restaurant_id;
            this.api_service.registerUser(user).subscribe(function (res) {
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Successfully registered!');
                        event.confirm.resolve();
                    }
                    else {
                        _this.toastrService.error(res.msg);
                        event.confirm.reject();
                    }
                }
                else {
                    _this.toastrService.error('Registration was failed!');
                    event.confirm.reject();
                }
            });
        }
        else {
            event.confirm.reject();
        }
    };
    UsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! ./users.component.html */ "./src/app/admin/users/users.component.html"),
            styles: [__webpack_require__(/*! ./users.component.scss */ "./src/app/admin/users/users.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ }),

/***/ "./src/app/models/index.ts":
/*!*********************************!*\
  !*** ./src/app/models/index.ts ***!
  \*********************************/
/*! exports provided: Restaurant */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _restaurant__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./restaurant */ "./src/app/models/restaurant.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Restaurant", function() { return _restaurant__WEBPACK_IMPORTED_MODULE_0__["Restaurant"]; });




/***/ }),

/***/ "./src/app/models/restaurant.ts":
/*!**************************************!*\
  !*** ./src/app/models/restaurant.ts ***!
  \**************************************/
/*! exports provided: Restaurant */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Restaurant", function() { return Restaurant; });
var Restaurant = /** @class */ (function () {
    function Restaurant() {
        this.id = 0;
        this.name = '';
        this.photo = '../../../assets/images/concession-stand.jpg';
        this.prepare_time = 5;
        this.booking_fee = 15;
        this.bank_account = '';
        this.status = 0;
        this.tnx_enabled = 'No';
        this.tnx_options = 'NONE';
        this.created_at = 0;
        this.updated_at = 0;
        this.map_pin_id = 0;
    }
    return Restaurant;
}());



/***/ })

}]);
//# sourceMappingURL=app-admin-admin-module.js.map