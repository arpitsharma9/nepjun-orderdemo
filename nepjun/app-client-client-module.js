(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-client-client-module"],{

/***/ "./src/app/client/client-menu.ts":
/*!***************************************!*\
  !*** ./src/app/client/client-menu.ts ***!
  \***************************************/
/*! exports provided: MENU_ITEMS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MENU_ITEMS", function() { return MENU_ITEMS; });
var MENU_ITEMS = [
    {
        title: 'Concessions',
        icon: 'nb-home',
        link: '/client/restaurant',
        home: true,
    }, {
        title: 'Menus',
        icon: 'nb-list',
        link: '/client/menu',
        home: true,
    }, {
        title: 'Sales',
        icon: 'nb-bar-chart',
        link: '/client/analytics',
        home: true,
    }, {
        title: 'Orders',
        icon: 'nb-compose',
        link: '/client/orders',
        home: true,
    },
];


/***/ }),

/***/ "./src/app/client/client.component.html":
/*!**********************************************!*\
  !*** ./src/app/client/client.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<food-ordering-layout>\r\n  <nb-menu [items]=\"menu\"></nb-menu>\r\n  <router-outlet></router-outlet>\r\n</food-ordering-layout>\r\n"

/***/ }),

/***/ "./src/app/client/client.component.scss":
/*!**********************************************!*\
  !*** ./src/app/client/client.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/client/client.component.ts":
/*!********************************************!*\
  !*** ./src/app/client/client.component.ts ***!
  \********************************************/
/*! exports provided: ClientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientComponent", function() { return ClientComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _client_menu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./client-menu */ "./src/app/client/client-menu.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ClientComponent = /** @class */ (function () {
    function ClientComponent() {
        this.menu = _client_menu__WEBPACK_IMPORTED_MODULE_1__["MENU_ITEMS"];
    }
    ClientComponent.prototype.ngOnInit = function () {
    };
    ClientComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'client',
            template: __webpack_require__(/*! ./client.component.html */ "./src/app/client/client.component.html"),
            styles: [__webpack_require__(/*! ./client.component.scss */ "./src/app/client/client.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ClientComponent);
    return ClientComponent;
}());



/***/ }),

/***/ "./src/app/client/client.module.ts":
/*!*****************************************!*\
  !*** ./src/app/client/client.module.ts ***!
  \*****************************************/
/*! exports provided: ClientModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientModule", function() { return ClientModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _restaurant_restaurant_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./restaurant/restaurant.component */ "./src/app/client/restaurant/restaurant.component.ts");
/* harmony import */ var _client_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./client.routing */ "./src/app/client/client.routing.ts");
/* harmony import */ var _client_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./client.component */ "./src/app/client/client.component.ts");
/* harmony import */ var _menu_menu_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../menu/menu.module */ "./src/app/menu/menu.module.ts");
/* harmony import */ var _restaurant_upload_photo_upload_photo_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./restaurant/upload-photo/upload-photo.component */ "./src/app/client/restaurant/upload-photo/upload-photo.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var ClientModule = /** @class */ (function () {
    function ClientModule() {
    }
    ClientModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _client_routing__WEBPACK_IMPORTED_MODULE_7__["ClientRoutingModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_2__["ThemeModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__["Ng2SmartTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_5__["FileUploadModule"],
                _menu_menu_module__WEBPACK_IMPORTED_MODULE_9__["MenuModule"]
            ],
            declarations: [
                _client_component__WEBPACK_IMPORTED_MODULE_8__["ClientComponent"],
                _restaurant_restaurant_component__WEBPACK_IMPORTED_MODULE_6__["RestaurantComponent"],
                _restaurant_upload_photo_upload_photo_component__WEBPACK_IMPORTED_MODULE_10__["UploadPhotoComponent"],
            ],
            entryComponents: [
                _restaurant_upload_photo_upload_photo_component__WEBPACK_IMPORTED_MODULE_10__["UploadPhotoComponent"]
            ]
        })
    ], ClientModule);
    return ClientModule;
}());



/***/ }),

/***/ "./src/app/client/client.routing.ts":
/*!******************************************!*\
  !*** ./src/app/client/client.routing.ts ***!
  \******************************************/
/*! exports provided: ClientRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientRoutingModule", function() { return ClientRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _client_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./client.component */ "./src/app/client/client.component.ts");
/* harmony import */ var _restaurant_restaurant_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./restaurant/restaurant.component */ "./src/app/client/restaurant/restaurant.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [{
        path: '',
        component: _client_component__WEBPACK_IMPORTED_MODULE_2__["ClientComponent"],
        children: [{
                path: 'restaurant',
                component: _restaurant_restaurant_component__WEBPACK_IMPORTED_MODULE_3__["RestaurantComponent"],
            }, {
                path: 'menu',
                loadChildren: 'app/menu/menu.module#MenuModule'
            }, {
                path: 'analytics',
                loadChildren: 'app/analytics/analytics.module#AnalyticsModule'
            }, {
                path: 'orders',
                loadChildren: 'app/orders/orders.module#OrdersModule'
            }, {
                path: '**',
                component: _restaurant_restaurant_component__WEBPACK_IMPORTED_MODULE_3__["RestaurantComponent"],
            }],
    }];
var ClientRoutingModule = /** @class */ (function () {
    function ClientRoutingModule() {
    }
    ClientRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ClientRoutingModule);
    return ClientRoutingModule;
}());



/***/ }),

/***/ "./src/app/client/restaurant/restaurant.component.html":
/*!*************************************************************!*\
  !*** ./src/app/client/restaurant/restaurant.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\r\n  <nb-card-header>\r\n    Concessions\r\n  </nb-card-header>\r\n\r\n  <nb-card-body class=\"restaurant-table\">\r\n    <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (editConfirm)=\"onSaveConfirm($event)\" (createConfirm)=\"onCreateConfirm($event)\"></ng2-smart-table>\r\n  </nb-card-body>\r\n</nb-card>\r\n\r\n"

/***/ }),

/***/ "./src/app/client/restaurant/restaurant.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/client/restaurant/restaurant.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/client/restaurant/restaurant.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/client/restaurant/restaurant.component.ts ***!
  \***********************************************************/
/*! exports provided: RestaurantComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestaurantComponent", function() { return RestaurantComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
/* harmony import */ var _upload_photo_upload_photo_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./upload-photo/upload-photo.component */ "./src/app/client/restaurant/upload-photo/upload-photo.component.ts");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../models */ "./src/app/models/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var RestaurantComponent = /** @class */ (function () {
    function RestaurantComponent(apiService, toastrService, cookieService, router) {
        this.apiService = apiService;
        this.toastrService = toastrService;
        this.cookieService = cookieService;
        this.router = router;
        this.settings = {
            mode: 'inline',
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmCreate: true,
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmSave: true,
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            columns: {
                name: {
                    title: 'Name',
                    type: 'string',
                },
                location: {
                    title: 'Location',
                    type: 'string',
                },
                photo: {
                    title: 'Image (350-150px)',
                    type: 'html',
                    valuePrepareFunction: function (photo) { return "<img id=\"my-restaurant-img\" height=\"85px\" width=\"200px\" src=\"" + photo + "\" />"; },
                    filter: false,
                    editor: {
                        type: 'custom',
                        component: _upload_photo_upload_photo_component__WEBPACK_IMPORTED_MODULE_7__["UploadPhotoComponent"],
                    },
                },
                tnx_enabled: {
                    title: 'Transaction Enabled',
                    type: 'string',
                    editor: {
                        type: 'list',
                        config: {
                            list: [
                                { title: 'No', value: 'No' },
                                { title: 'Yes', value: 'Yes' }
                            ]
                        }
                    },
                    filter: false,
                },
                tnx_options: {
                    title: 'Transaction Options',
                    type: 'string',
                    valuePrepareFunction: function (tnx_options) {
                        switch (tnx_options) {
                            case 'BOTH':
                                return 'All';
                            case 'FAST-LINE':
                                return 'Fast-Line';
                            case 'IN-SEAT':
                                return 'In-Seat';
                            default:
                                return 'None';
                        }
                    },
                    editor: {
                        type: 'list',
                        config: {
                            list: [
                                { title: 'None', value: 'NONE' },
                                { title: 'Fast-Line', value: 'FAST-LINE' },
                                { title: 'In-Seat', value: 'IN-SEAT' },
                                { title: 'All', value: 'BOTH' },
                            ]
                        }
                    },
                    filter: false,
                },
                prepare_time: {
                    title: 'Preparation Time(min)',
                    type: 'number',
                    filter: false,
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
    }
    RestaurantComponent.prototype.ngOnInit = function () {
        this.restaurant = new _models__WEBPACK_IMPORTED_MODULE_8__["Restaurant"]();
        if (!(this.cookieService.check('user') && this.cookieService.get('user') !== '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
        this.getRestaurants();
    };
    RestaurantComponent.prototype.getRestaurants = function () {
        var _this = this;
        this.apiService.getRestaurantsByClientId(this.currentUser.client_id).subscribe(function (res) {
            // console.log(res);
            if (res) {
                if (!res.err) {
                    _this.restaurants = res.response;
                    _this.source.load(_this.restaurants);
                }
                else {
                    _this.toastrService.error('Cannot fetch users!');
                }
            }
            else {
                _this.toastrService.error('Cannot fetch users!');
            }
        });
    };
    RestaurantComponent.prototype.onDeleteConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to delete this concession?')) {
            this.apiService.deleteRestaurant(event.data.id).subscribe(function (res) {
                // console.log(res);
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Successfully deleted!');
                        event.confirm.resolve();
                    }
                    else {
                        _this.toastrService.error(res.msg);
                        event.confirm.reject();
                    }
                }
                else {
                    _this.toastrService.error('Delete concession failed!');
                    event.confirm.reject();
                }
            });
        }
        else {
            event.confirm.reject();
        }
    };
    RestaurantComponent.prototype.onSaveConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to save changes?')) {
            var name_1 = event.newData['name'];
            if (name_1 === '') {
                this.toastrService.error('You must enter a name.');
                event.confirm.reject();
                return;
            }
            event.newData['name'] = event.newData['name'].replace(/'/g, "''");
            // console.log(event.newData['name'])
            var preparationTime = event.newData['prepare_time'];
            if (preparationTime === '') {
                event.newData['prepare_time'] = 5;
                // this.toastrService.error('You must enter a preparation time.');
                // event.confirm.reject();
                // return;
            }
            event.newData['restaurant_id'] = event.newData['id'];
            this.apiService.updateRestaurant(event.newData).subscribe(function (res) {
                // console.log(res);
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Successfully updated!');
                        event.confirm.resolve();
                    }
                    else {
                        _this.toastrService.error(res.msg);
                        event.confirm.reject();
                    }
                }
                else {
                    _this.toastrService.error('Update failed!');
                    event.confirm.reject();
                }
            });
        }
        else {
            event.confirm.reject();
        }
    };
    RestaurantComponent.prototype.onCreateConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to create this concession?')) {
            var name_2 = event.newData['name'];
            if (name_2 === '') {
                this.toastrService.error('You must enter a name.');
                event.confirm.reject();
                return;
            }
            var photo = event.newData['photo'];
            if (photo === '') {
                this.toastrService.error('You must upload a cover image.');
                event.confirm.reject();
                return;
            }
            var preparationTime = event.newData['prepare_time'];
            if (preparationTime === '') {
                event.newData['prepare_time'] = 5;
            }
            var tnxEnabled = event.newData['tnx_enabled'];
            if (tnxEnabled === '') {
                event.newData['tnx_enabled'] = 'No';
                event.newData['tnx_options'] = 'NONE';
            }
            var tnxOptions = event.newData['tnx_options'];
            if (tnxOptions === '') {
                event.newData['tnx_options'] = (tnxEnabled === 'Yes') ? 'BOTH' : 'NONE';
            }
            event.newData['client_id'] = this.currentUser.client_id;
            this.apiService.addRestaurant(event.newData).subscribe(function (res) {
                // console.log(res);
                if (res) {
                    if (!res.err) {
                        _this.getRestaurants();
                        _this.toastrService.info('Successfully updated!');
                        event.confirm.resolve();
                    }
                    else {
                        _this.toastrService.error(res.msg);
                        event.confirm.reject();
                    }
                }
                else {
                    _this.toastrService.error('Update failed!');
                    event.confirm.reject();
                }
            });
        }
        else {
            event.confirm.reject();
        }
    };
    RestaurantComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'restaurant',
            template: __webpack_require__(/*! ./restaurant.component.html */ "./src/app/client/restaurant/restaurant.component.html"),
            styles: [__webpack_require__(/*! ./restaurant.component.scss */ "./src/app/client/restaurant/restaurant.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__["CookieService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], RestaurantComponent);
    return RestaurantComponent;
}());



/***/ }),

/***/ "./src/app/client/restaurant/upload-photo/upload-photo.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/client/restaurant/upload-photo/upload-photo.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"comp-wrapper\">\r\n  <div *ngIf=\"!showPhoto\" [innerHTML]=\"cell.getValue()\" #htmlValue></div>\r\n  <img *ngIf=\"showPhoto\" [src]=\"photo\">\r\n  <input id=\"fileInput\" type=\"file\" ng2FileSelect [uploader]=\"uploader\" accept=\"image/*\" (change)=\"uploadPhoto($event)\" style=\"display:none;\" #file>\r\n  <button mat-stroked-button color=\"primary\" (click)=\"file.click()\">Upload Image</button>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/client/restaurant/upload-photo/upload-photo.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/client/restaurant/upload-photo/upload-photo.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".comp-wrapper {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column; }\n\n.comp-wrapper > div {\n  margin-bottom: 5px; }\n\n.comp-wrapper > img {\n  width: 200px;\n  margin-bottom: 5px; }\n"

/***/ }),

/***/ "./src/app/client/restaurant/upload-photo/upload-photo.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/client/restaurant/upload-photo/upload-photo.component.ts ***!
  \**************************************************************************/
/*! exports provided: UploadPhotoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadPhotoComponent", function() { return UploadPhotoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UploadPhotoComponent = /** @class */ (function (_super) {
    __extends(UploadPhotoComponent, _super);
    function UploadPhotoComponent() {
        var _this = _super.call(this) || this;
        _this.showPhoto = false;
        return _this;
    }
    UploadPhotoComponent.prototype.ngOnInit = function () {
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__["FileUploader"]({
            url: "https://api.paranoidfan.com/api/sdk/upload",
            isHTML5: true,
            headers: [{
                    name: 'Authkey',
                    value: 'NCta}*XTV1R6SCta}*XTV1R0'
                }]
        });
    };
    UploadPhotoComponent.prototype.uploadPhoto = function (event) {
        var _this = this;
        // document.getElementById('restaurant-img').setAttribute('src', URL.createObjectURL(event.target.files[0]));
        var _that = this;
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            // console.log(item)
            console.log(status);
            if (status === 200) {
                var res = JSON.parse(response);
                if (item.alias === 'photo') {
                    _that.photo = res.URL;
                    _that.showPhoto = true;
                    _that.cell.newValue = _that.photo;
                }
                // console.log(item, res, status);
            }
            else {
                item.upload();
            }
        };
        if (this.uploader.queue.length > 0) {
            this.uploader.queue.forEach(function (item) {
                if (item.file.type.match(/image/)) {
                    item.alias = 'photo';
                    _this.uploader.onBuildItemForm = function (item, form) {
                        form.append('path', 'uploads/');
                    };
                    item.upload();
                }
            });
        }
    };
    UploadPhotoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'upload-photo',
            template: __webpack_require__(/*! ./upload-photo.component.html */ "./src/app/client/restaurant/upload-photo/upload-photo.component.html"),
            styles: [__webpack_require__(/*! ./upload-photo.component.scss */ "./src/app/client/restaurant/upload-photo/upload-photo.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UploadPhotoComponent);
    return UploadPhotoComponent;
}(ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["DefaultEditor"]));

// Custom Editor Component example:
// https://github.com/akveo/ng2-smart-table/blob/master/src/app/pages/examples/custom-edit-view/advanced-example-custom-editor.component.ts


/***/ })

}]);
//# sourceMappingURL=app-client-client-module.js.map