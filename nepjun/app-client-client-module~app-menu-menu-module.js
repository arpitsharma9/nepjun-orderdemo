(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-client-client-module~app-menu-menu-module"],{

/***/ "./src/app/custom-component/custom-component.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/custom-component/custom-component.module.ts ***!
  \*************************************************************/
/*! exports provided: CustomModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomModule", function() { return CustomModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _custom_confirm_custom_confirm_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./custom-confirm/custom-confirm.component */ "./src/app/custom-component/custom-confirm/custom-confirm.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { CustomEditComponent } from './custom-edit/custom-edit.component';
// import { CustomMenuComponent } from './custom-menu/custom-menu.component';
var CustomModule = /** @class */ (function () {
    function CustomModule() {
    }
    CustomModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_2__["ThemeModule"],
            ],
            declarations: [
                _custom_confirm_custom_confirm_component__WEBPACK_IMPORTED_MODULE_3__["CustomConfirmComponent"],
            ],
            entryComponents: [
                _custom_confirm_custom_confirm_component__WEBPACK_IMPORTED_MODULE_3__["CustomConfirmComponent"],
            ]
        })
    ], CustomModule);
    return CustomModule;
}());



/***/ }),

/***/ "./src/app/custom-component/custom-confirm/custom-confirm.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/custom-component/custom-confirm/custom-confirm.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <span>{{ title }}</span>\r\n  <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  {{ description }}\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button class=\"btn btn-md btn-success\" (click)=\"onClickOk()\">Ok</button>\r\n  <button class=\"btn btn-md btn-danger\" (click)=\"closeModal()\">Cancel</button>\r\n</div>"

/***/ }),

/***/ "./src/app/custom-component/custom-confirm/custom-confirm.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/custom-component/custom-confirm/custom-confirm.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/custom-component/custom-confirm/custom-confirm.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/custom-component/custom-confirm/custom-confirm.component.ts ***!
  \*****************************************************************************/
/*! exports provided: CustomConfirmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomConfirmComponent", function() { return CustomConfirmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal-ref */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal-ref.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CustomConfirmComponent = /** @class */ (function () {
    function CustomConfirmComponent(activeModal, api_service, cookieService, toastrService, fb) {
        this.activeModal = activeModal;
        this.api_service = api_service;
        this.cookieService = cookieService;
        this.toastrService = toastrService;
        this.fb = fb;
    }
    CustomConfirmComponent.prototype.onClickOk = function () {
        this.activeModal.close();
        this.closeModal();
    };
    CustomConfirmComponent.prototype.closeModal = function () {
        this.activeModal.dismiss();
    };
    CustomConfirmComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CustomConfirmComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CustomConfirmComponent.prototype, "description", void 0);
    CustomConfirmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'custom-confirm',
            template: __webpack_require__(/*! ./custom-confirm.component.html */ "./src/app/custom-component/custom-confirm/custom-confirm.component.html"),
            styles: [__webpack_require__(/*! ./custom-confirm.component.scss */ "./src/app/custom-component/custom-confirm/custom-confirm.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"], _services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]])
    ], CustomConfirmComponent);
    return CustomConfirmComponent;
}());



/***/ }),

/***/ "./src/app/custom-component/custom-edit/custom-edit.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/custom-component/custom-edit/custom-edit.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <span>{{ title }}</span>\r\n  <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<form [formGroup]=\"editForm\" (ngSubmit)=\"onClickOk()\">\r\n  <div class=\"modal-body\">\r\n    <div class=\"form-group\">\r\n      <input type=\"text\" formControlName=\"newName\" class=\"form-control\" [ngClass]=\"{'form-control-danger': editForm.controls.newName.invalid && (editForm.controls.newName.dirty || editForm.controls.newName.touched)}\" placeholder=\"Please fill this\">\r\n    </div>\r\n    <div  class=\"form-group\">\r\n        <!-- *ngIf=\"source === 'NCR'\" -->\r\n        <label>Vendor ID</label>\r\n        <input type=\"text\" formControlName=\"newVendor\" class=\"form-control\">\r\n    </div>\r\n    <!-- Thumbnail Image -->\r\n    <div *ngIf=\"hasFileInput\" class=\"form-group\">\r\n      <label for=\"thumbnail\">Thumbnail</label>\r\n      <i *ngIf=\"imageUploaded\" class=\"fa fa-check-circle\" style=\"margin-left: 20px;\"></i>\r\n      <!-- <label *ngIf=\"!imageUploaded && !imageUploadFailed\" style=\"margin-left: 20px;\">Uploading ({{ uploadPercent }}%)</label> -->\r\n      <label *ngIf=\"imageUploadFailed\" style=\"margin-left: 20px;color:red;\">Upload failed!</label>\r\n      <input type=\"file\" formControlName=\"newThumbnail\" class=\"form-control\" id=\"thumbnail\" placeholder=\"Thumbnail\" (change)=\"onChange($event)\">\r\n    </div>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"submit\" class=\"btn btn-md btn-success\">Ok</button>\r\n    <button class=\"btn btn-md btn-danger\" (click)=\"closeModal()\">Cancel</button>\r\n  </div>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/custom-component/custom-edit/custom-edit.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/custom-component/custom-edit/custom-edit.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/custom-component/custom-edit/custom-edit.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/custom-component/custom-edit/custom-edit.component.ts ***!
  \***********************************************************************/
/*! exports provided: CustomEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomEditComponent", function() { return CustomEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal-ref */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal-ref.js");
/* harmony import */ var _services_file_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/file.service */ "./src/app/services/file.service.ts");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CustomEditComponent = /** @class */ (function () {
    function CustomEditComponent(activeModal, fb, fileService) {
        this.activeModal = activeModal;
        this.fb = fb;
        this.fileService = fileService;
        this.linkThunbnail = '';
        this.hasFileInput = false;
        this.imageUploaded = true;
        this.imageUploadFailed = false;
        this.uploadPercent = 100;
        console.log(this.source);
    }
    CustomEditComponent.prototype.ngOnInit = function () {
        if (this.hasFileInput) {
            this.editForm = this.fb.group({
                newName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(1)]],
                newThumbnail: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(1)]],
                newVendor: null,
            });
            this.editForm.setValue({ newName: this.content, newThumbnail: '', newVendor: this.vendor });
        }
        else {
            this.editForm = this.fb.group({
                newName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(1)]],
                newVendor: null,
            });
            this.editForm.setValue({ newName: this.content, newVendor: this.vendor });
        }
    };
    CustomEditComponent.prototype.onClickOk = function () {
        if (this.editForm.controls.newName.valid) {
            if (this.linkThunbnail == undefined || this.linkThunbnail == null)
                this.linkThunbnail = '';
            if (this.hasFileInput) {
                if (this.imageUploaded) {
                    this.activeModal.close({ newName: this.editForm.value.newName, newVendor: this.editForm.value.newVendor, newThumbnail: this.linkThunbnail });
                    this.closeModal();
                }
            }
            else {
                this.activeModal.close({ newName: this.editForm.value.newName, newVendor: this.editForm.value.newVendor });
                this.closeModal();
            }
        }
    };
    CustomEditComponent.prototype.closeModal = function () {
        this.activeModal.dismiss();
    };
    CustomEditComponent.prototype.onChange = function (x) {
        var _this = this;
        var self = this;
        this.imageUploaded = false;
        this.imageUploadFailed = false;
        if (x.target.files.length == 0) {
            return;
        }
        this.tempThumbnail = x.target.files[0];
        var formData = new FormData();
        var image_name = this.tempThumbnail['name'];
        var image_file_extensions = ['.jpg', '.png', '.gif', '.jpeg',];
        formData.append("file", this.tempThumbnail, image_name);
        this.fileService.addFile(formData).subscribe(function (event) {
            if (event.status == "true") {
                self.linkThunbnail = event['URL'];
                self.imageUploaded = true;
            }
            else {
                _this.imageUploadFailed = true;
            }
        }, function (err) {
            console.error(err);
            _this.imageUploadFailed = true;
        }, function () {
            _common__WEBPACK_IMPORTED_MODULE_4__["Utils"].logs("Image has been uploaded!");
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CustomEditComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CustomEditComponent.prototype, "source", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CustomEditComponent.prototype, "linkThunbnail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CustomEditComponent.prototype, "content", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], CustomEditComponent.prototype, "vendor", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], CustomEditComponent.prototype, "hasFileInput", void 0);
    CustomEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'custom-edit',
            template: __webpack_require__(/*! ./custom-edit.component.html */ "./src/app/custom-component/custom-edit/custom-edit.component.html"),
            styles: [__webpack_require__(/*! ./custom-edit.component.scss */ "./src/app/custom-component/custom-edit/custom-edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_2__["NgbActiveModal"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _services_file_service__WEBPACK_IMPORTED_MODULE_3__["FileService"]])
    ], CustomEditComponent);
    return CustomEditComponent;
}());



/***/ }),

/***/ "./src/app/custom-component/custom-menu/custom-menu.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/custom-component/custom-menu/custom-menu.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngFor=\"let item of items\" class=\"custom-menu-layout\">\r\n  <div class=\"vendor-id\" *ngIf=\"item.vendor_id\">\r\n    <p>Vendor Identifier: {{item.vendor_id}}</p>\r\n  </div>\r\n  <div class=\"row\">\r\n    <img *ngIf=\"item.thumbnail != '' && item.thumbnail != null && item.thumbnail != undefined\" class=\"thumbnail\" src = \"{{ item.thumbnail }}\" (click)=\"onClickSelect(item)\">\r\n  </div>\r\n  <div class=\"custom-menu-item\" [ngClass]=\"{ active: item.is_selected }\">\r\n    <div class=\"icon\"><i class=\"{{ item.icon }}\" (click)=\"onClickSelect(item)\"></i></div>\r\n    <span (click)=\"onClickSelect(item)\">{{ item.title }}</span>\r\n    <div class=\"action\">\r\n      <div *ngIf=\"edit_enabled\" class=\"icon\"><i class=\"nb-edit\" (click)=\"onClickEdit(item)\"></i></div>\r\n      <div class=\"icon\"><i class=\"nb-trash\" (click)=\"onClickDelete(item)\"></i></div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/custom-component/custom-menu/custom-menu.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/custom-component/custom-menu/custom-menu.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".custom-menu-layout {\n  padding: 0;\n  width: 100%;\n  padding-top: 10px;\n  border-bottom: 1px solid #ebeef2; }\n  .custom-menu-layout p {\n    margin-bottom: 0; }\n  .custom-menu-layout .vendor-id {\n    font-size: 14px;\n    color: #c2c2c2;\n    font-weight: 600;\n    padding-left: 10px; }\n  .custom-menu-layout .thumbnail {\n    width: 100px;\n    height: 100px;\n    margin: auto;\n    margin-bottom: 10px;\n    border-radius: 50%;\n    cursor: pointer; }\n  .custom-menu-layout i {\n    font-size: 25px; }\n  .custom-menu-layout .custom-menu-item {\n    box-sizing: border-box;\n    border-radius: 0.25rem;\n    width: 100%;\n    opacity: 0.8;\n    display: flex;\n    align-items: center;\n    padding: 5px;\n    cursor: pointer; }\n  .custom-menu-layout .custom-menu-item:hover {\n      font-weight: bold;\n      opacity: 1; }\n  .custom-menu-layout .custom-menu-item.active {\n      border: 2px solid #26A9E9;\n      color: #26A9E9;\n      font-weight: bold; }\n  .custom-menu-layout .custom-menu-item .icon {\n      width: 2.5rem;\n      font-size: 2.5rem;\n      display: flex; }\n  .custom-menu-layout .custom-menu-item .action {\n      display: flex;\n      margin-left: auto; }\n  .custom-menu-layout .custom-menu-item .action .icon {\n        width: 1.5rem;\n        font-size: 1.5rem;\n        display: flex; }\n  .custom-menu-layout .custom-menu-item .action .icon :hover {\n          animation: scaling 0.3s ease-in-out;\n          -webkit-animation: scaling 0.3s ease-in-out; }\n  .custom-menu-layout .custom-menu-item .action .icon :active {\n          opacity: 0.8; }\n  @-webkit-keyframes scaling {\n  0% {\n    transform: scale(1);\n    -webkit-transform: scale(1); }\n  40% {\n    transform: scale(1.5);\n    -webkit-transform: scale(1.5); }\n  70% {\n    transform: scale(0.8);\n    -webkit-transform: scale(0.8); }\n  100% {\n    transform: scale(1.5);\n    -webkit-transform: scale(1.5); } }\n  @keyframes scaling {\n  0% {\n    transform: scale(1);\n    -webkit-transform: scale(1); }\n  40% {\n    transform: scale(1.5);\n    -webkit-transform: scale(1.5); }\n  70% {\n    transform: scale(0.8);\n    -webkit-transform: scale(0.8); }\n  100% {\n    transform: scale(1.5);\n    -webkit-transform: scale(1.5); } }\n"

/***/ }),

/***/ "./src/app/custom-component/custom-menu/custom-menu.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/custom-component/custom-menu/custom-menu.component.ts ***!
  \***********************************************************************/
/*! exports provided: CustomMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomMenuComponent", function() { return CustomMenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal.js");
/* harmony import */ var _custom_confirm_custom_confirm_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../custom-confirm/custom-confirm.component */ "./src/app/custom-component/custom-confirm/custom-confirm.component.ts");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _custom_edit_custom_edit_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../custom-edit/custom-edit.component */ "./src/app/custom-component/custom-edit/custom-edit.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CustomMenuComponent = /** @class */ (function () {
    function CustomMenuComponent(modalService, apiService) {
        this.modalService = modalService;
        this.apiService = apiService;
        this.edit_enabled = true;
        this.onSelect = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onEdit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onDelete = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    CustomMenuComponent.prototype.ngOnInit = function () {
    };
    CustomMenuComponent.prototype.onClickSelect = function (event) {
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var item = _a[_i];
            item.is_selected = false;
        }
        event.is_selected = true;
        this.onSelect.emit(event);
    };
    CustomMenuComponent.prototype.onClickEdit = function (event) {
        var self = this;
        var activeModal = this.modalService.open(_custom_edit_custom_edit_component__WEBPACK_IMPORTED_MODULE_4__["CustomEditComponent"], { size: 'lg', container: 'nb-layout' });
        activeModal.close = function (newData) {
            self.callbackEdit(event, newData);
        };
        activeModal.componentInstance.title = " Edit " + (event.is_category ? "Category" : "Item");
        activeModal.componentInstance.hasFileInput = event.is_category;
        activeModal.componentInstance.content = event.title;
        activeModal.componentInstance.vendor = event.vendor_id;
        activeModal.componentInstance.source = event.source;
    };
    CustomMenuComponent.prototype.onClickDelete = function (event) {
        var self = this;
        var activeModal = this.modalService.open(_custom_confirm_custom_confirm_component__WEBPACK_IMPORTED_MODULE_2__["CustomConfirmComponent"], { size: 'lg', container: 'nb-layout' });
        activeModal.close = function () {
            self.callbackDelete(event);
        };
        activeModal.componentInstance.title = event.title;
        activeModal.componentInstance.description = "Would you like to delete this " + (event.is_category ? "category" : "item") + "?";
    };
    CustomMenuComponent.prototype.callbackDelete = function (event) {
        this.onDelete.emit(event);
    };
    CustomMenuComponent.prototype.callbackEdit = function (event, newData) {
        event.name = newData.newName;
        event.vendor_id = newData.newVendor;
        event.thumbnail = newData.newThumbnail;
        this.onEdit.emit(event);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CustomMenuComponent.prototype, "items", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CustomMenuComponent.prototype, "edit_enabled", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], CustomMenuComponent.prototype, "onSelect", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], CustomMenuComponent.prototype, "onEdit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], CustomMenuComponent.prototype, "onDelete", void 0);
    CustomMenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'custom-menu',
            template: __webpack_require__(/*! ./custom-menu.component.html */ "./src/app/custom-component/custom-menu/custom-menu.component.html"),
            styles: [__webpack_require__(/*! ./custom-menu.component.scss */ "./src/app/custom-component/custom-menu/custom-menu.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_1__["NgbModal"],
            _services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"]])
    ], CustomMenuComponent);
    return CustomMenuComponent;
}());



/***/ }),

/***/ "./src/app/menu/add-cagetory-modal/add-cagetory-modal.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/menu/add-cagetory-modal/add-cagetory-modal.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n    <span>Add Category</span>\r\n    <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <form [formGroup]=\"addCategoryForm\" (ngSubmit)=\"addCategory()\">\r\n    <div class=\"modal-body\">\r\n      <div class=\"form-group\">\r\n        <input type=\"text\" formControlName=\"newCategoryName\" class=\"form-control\" [ngClass]=\"{'form-control-danger': addCategoryForm.controls.newCategoryName.invalid && (addCategoryForm.controls.newCategoryName.dirty || addCategoryForm.controls.newCategoryName.touched)}\" placeholder=\"New category name\">\r\n      </div>\r\n      <!-- Thumbnail Image -->\r\n      <!-- <div class=\"form-group\">\r\n        <label for=\"thumbnail\">Thumbnail</label>\r\n        <i *ngIf=\"imageUploaded\" class=\"fa fa-check-circle\" style=\"margin-left: 20px;\"></i>\r\n        <label *ngIf=\"imageUploadFailed\" style=\"margin-left: 20px;color:red;\">Upload failed!</label>\r\n        <input type=\"file\" formControlName=\"thumbnail\" class=\"form-control\" id=\"thumbnail\" placeholder=\"Thumbnail\" (change)=\"onChange($event)\">\r\n      </div> -->\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n      <button type=\"submit\" class=\"btn btn-md btn-success\">Add</button>\r\n      <button class=\"btn btn-md btn-danger\" (click)=\"closeModal()\">Cancel</button>\r\n    </div>\r\n  </form>\r\n"

/***/ }),

/***/ "./src/app/menu/add-cagetory-modal/add-cagetory-modal.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/menu/add-cagetory-modal/add-cagetory-modal.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/menu/add-cagetory-modal/add-cagetory-modal.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/menu/add-cagetory-modal/add-cagetory-modal.component.ts ***!
  \*************************************************************************/
/*! exports provided: AddCagetoryModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCagetoryModalComponent", function() { return AddCagetoryModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal-ref */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal-ref.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_file_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/file.service */ "./src/app/services/file.service.ts");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AddCagetoryModalComponent = /** @class */ (function () {
    function AddCagetoryModalComponent(activeModal, api_service, fileService, toastrService, fb) {
        this.activeModal = activeModal;
        this.api_service = api_service;
        this.fileService = fileService;
        this.toastrService = toastrService;
        this.fb = fb;
        this.imageUploaded = false;
        this.imageUploadFailed = false;
        this.uploadPercent = 100;
        this.linkThunbnail = '';
        this.addCategoryForm = this.fb.group({
            newCategoryName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(1)]],
            thumbnail: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(1)]],
        });
    }
    AddCagetoryModalComponent.prototype.addCategory = function () {
        var _this = this;
        if (this.addCategoryForm.controls.newCategoryName.invalid)
            return;
        if (this.linkThunbnail == undefined || this.linkThunbnail == null)
            this.linkThunbnail = '';
        this.api_service.addCategory(this.addCategoryForm.value.newCategoryName, this.menu_id, this.linkThunbnail).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    _this.toastrService.info('Successfully added!');
                    _this.activeModal.close();
                    _this.activeModal.dismiss();
                }
                else {
                    _this.toastrService.error(res.msg);
                }
            }
            else {
                _this.toastrService.error('Cannot add this!');
            }
        });
    };
    AddCagetoryModalComponent.prototype.onChange = function (x) {
        var _this = this;
        var self = this;
        this.imageUploaded = false;
        this.imageUploadFailed = false;
        if (x.target.files.length == 0) {
            return;
        }
        this.tempThumbnail = x.target.files[0];
        var image_name = this.tempThumbnail['name'];
        var formData = new FormData();
        formData.append("photo", this.tempThumbnail, image_name);
        formData.append("path", "uploads/");
        this.fileService.addFile(formData).subscribe(function (event) {
            if (event.status == "true") {
                self.linkThunbnail = event['URL'];
                self.imageUploaded = true;
            }
            else {
                _this.imageUploadFailed = true;
            }
        }, function (err) {
            console.error(err);
            _this.imageUploadFailed = true;
        }, function () {
            _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs("Image has been uploaded!");
        });
    };
    AddCagetoryModalComponent.prototype.closeModal = function () {
        this.activeModal.dismiss();
    };
    AddCagetoryModalComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AddCagetoryModalComponent.prototype, "menu_id", void 0);
    AddCagetoryModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'add-cagetory-modal',
            template: __webpack_require__(/*! ./add-cagetory-modal.component.html */ "./src/app/menu/add-cagetory-modal/add-cagetory-modal.component.html"),
            styles: [__webpack_require__(/*! ./add-cagetory-modal.component.scss */ "./src/app/menu/add-cagetory-modal/add-cagetory-modal.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"],
            _services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            _services_file_service__WEBPACK_IMPORTED_MODULE_5__["FileService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]])
    ], AddCagetoryModalComponent);
    return AddCagetoryModalComponent;
}());



/***/ }),

/***/ "./src/app/menu/add-item-modal/add-item-modal.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/menu/add-item-modal/add-item-modal.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <span>Add Item</span>\r\n  <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"item-detail\">\r\n  <div class=\"spinner-wrapper\" *ngIf=\"showSpinner\">\r\n    <mat-spinner></mat-spinner>\r\n  </div>\r\n  <form [formGroup]=\"itemDetailForm\" (ngSubmit)=\"addItem()\">\r\n      <div class=\"modal-body\">\r\n      <!-- Item Name -->\r\n      <div class=\"form-group\">\r\n        <input type=\"text\" formControlName=\"itemName\" class=\"form-control\" [ngClass]=\"{'form-control-danger': itemDetailForm.controls.itemName.invalid && (itemDetailForm.controls.itemName.dirty || itemDetailForm.controls.itemName.touched)}\" id=\"exampleInputEmail1\" placeholder=\"Item name\">\r\n      </div>\r\n      <!-- Description -->\r\n      <div class=\"form-group\">\r\n        <textarea rows=\"2\" formControlName=\"itemDescription\" placeholder=\"Description, for example, ingredients, preparation, etc\" id=\"inputDescription\" class=\"form-control\"></textarea>\r\n      </div>\r\n\r\n      <div class=\"row\">\r\n        <!-- Price -->\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <label for=\"inputPrice\">Price ($)</label>\r\n            <input type=\"number\" formControlName=\"itemPrice\" class=\"form-control\" [ngClass]=\"{'form-control-danger': itemDetailForm.controls.itemPrice.invalid && (itemDetailForm.controls.itemPrice.dirty || itemDetailForm.controls.itemPrice.touched)}\" id=\"inputPrice\" placeholder=\"0.00\">\r\n          </div>\r\n        </div>\r\n        <!-- Tax -->\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <label for=\"inputTax\">Tax (%)</label>\r\n            <input type=\"number\" formControlName=\"itemTax\" class=\"form-control\" [ngClass]=\"{'form-control-danger': itemDetailForm.controls.itemTax.invalid && (itemDetailForm.controls.itemTax.dirty || itemDetailForm.controls.itemTax.touched)}\" id=\"inputTax\" placeholder=\"0.00\">\r\n          </div>\r\n        </div>\r\n        <!-- Thumbnail Image -->\r\n        <div class=\"col-md-12\">\r\n          <div class=\"form-group\">\r\n            <label for=\"thumbnail\">Image</label>\r\n            <div style=\"display: flex; flex-direction: column;\" class=\"form-control\" >\r\n              <img id=\"add-item-icon\" src=\"../../assets/images/transparent_200x200.png\" width=\"100\" height=\"100\" style=\"margin-bottom: 10px; border: 1px solid #dadfe6; border-radius: 50%;\">\r\n              <i *ngIf=\"imageUploaded\" class=\"fa fa-check-circle\" style=\"margin-left: 20px;\"></i>\r\n              <!-- <label *ngIf=\"tempThumbnail && !imageUploaded && !imageUploadFailed\" style=\"margin-left: 20px;\">Uploading ({{ uploadPercent }}%)</label> -->\r\n              <label *ngIf=\"imageUploadFailed\" style=\"margin-left: 20px;color:red;\">Upload failed!</label>\r\n              <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" accept=\"image/*\" formControlName=\"thumbnail\" placeholder=\"Thumbnail\" (change)=\"readURL($event)\">\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Item Status -->\r\n        <div class=\"col-md-12\">\r\n          <div class=\"form-group\">\r\n            <label for=\"item-status\">Item Status</label>\r\n            <ngx-switcher\r\n              [firstValue]=\"true\"\r\n              [secondValue]=\"false\"\r\n              [firstValueLabel]=\"'Available'\"\r\n              [secondValueLabel]=\"'Not available'\"\r\n              [value]=\"itemStatus\"\r\n              (valueChange)=\"toggleItemStatus($event)\" id=\"item-status\" class=\"form-control\"> \r\n            </ngx-switcher>\r\n          </div>\r\n        </div>\r\n        <!-- Item Type -->\r\n        <div class=\"col-md-12\">\r\n          <div class=\"form-group\">\r\n            <label>Item Type</label>\r\n            <div class=\"form-control flex-space-around\">\r\n                <!-- <mat-checkbox formControlName =\"isStatic\">Static</mat-checkbox> -->\r\n                <mat-checkbox formControlName =\"isFastLine\">Fast Line</mat-checkbox>\r\n                <mat-checkbox formControlName =\"isInSeat\">In Seat Delivery</mat-checkbox>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"submit\" class=\"btn btn-md btn-success\">Add</button>\r\n        <button class=\"btn btn-md btn-danger\" (click)=\"closeModal()\">Cancel</button>\r\n    </div>\r\n  </form>\r\n</div>"

/***/ }),

/***/ "./src/app/menu/add-item-modal/add-item-modal.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/menu/add-item-modal/add-item-modal.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.spinner-wrapper {\n  display: flex;\n  justify-content: center;\n  position: absolute;\n  width: 100%;\n  height: 800px;\n  align-items: center;\n  background: rgba(255, 255, 255, 0.3);\n  z-index: 5; }\n"

/***/ }),

/***/ "./src/app/menu/add-item-modal/add-item-modal.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/menu/add-item-modal/add-item-modal.component.ts ***!
  \*****************************************************************/
/*! exports provided: AddItemModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddItemModalComponent", function() { return AddItemModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _nebular_theme_components_menu_menu_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../@nebular/theme/components/menu/menu.service */ "./src/app/@nebular/theme/components/menu/menu.service.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal-ref */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal-ref.js");
/* harmony import */ var _services_file_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/file.service */ "./src/app/services/file.service.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var AddItemModalComponent = /** @class */ (function () {
    function AddItemModalComponent(activeModal, api_service, fileService, menuService, cookieService, router, toastrService, fb) {
        this.activeModal = activeModal;
        this.api_service = api_service;
        this.fileService = fileService;
        this.menuService = menuService;
        this.cookieService = cookieService;
        this.router = router;
        this.toastrService = toastrService;
        this.fb = fb;
        this.imageUploaded = false;
        this.imageUploadFailed = false;
        this.uploadPercent = 100;
        this.linkThunbnail = '';
        this.itemStatus = true;
        this.showSpinner = false;
        this.itemDetailForm = this.fb.group({
            itemName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].minLength(1)]],
            itemDescription: ['', null],
            itemPrice: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].min(0)]],
            itemTax: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].min(0)]],
            thumbnail: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].minLength(1)]],
            isStatic: [0, null],
            isFastLine: [1, null],
            isInSeat: [1, null],
        });
    }
    AddItemModalComponent.prototype.ngOnInit = function () {
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_9__["FileUploader"]({
            url: "https://api.paranoidfan.com/api/sdk/upload",
            isHTML5: true,
            headers: [{
                    name: 'Authkey',
                    value: 'NCta}*XTV1R6SCta}*XTV1R0'
                }]
        });
    };
    AddItemModalComponent.prototype.addItem = function () {
        var _this = this;
        if (this.itemDetailForm.controls.itemName.invalid)
            return;
        if (this.itemDetailForm.controls.itemPrice.invalid)
            return;
        if (this.itemDetailForm.controls.itemTax.invalid)
            return;
        this.showSpinner = true;
        var _that = this;
        var status = (this.itemStatus) ? 1 : 0;
        this.itemDetailForm.value.isStatic = (this.itemDetailForm.value.isStatic) ? 1 : 0;
        this.itemDetailForm.value.isFastLine = (this.itemDetailForm.value.isFastLine) ? 1 : 0;
        this.itemDetailForm.value.isInSeat = (this.itemDetailForm.value.isInSeat) ? 1 : 0;
        if (this.uploader.queue.length > 0) {
            this.uploader.queue.forEach(function (item) {
                if (item.file.type.match(/image/)) {
                    item.alias = 'photo';
                    _this.uploader.onBuildItemForm = function (item, form) {
                        form.append('path', 'uploads/');
                    };
                    item.upload();
                }
            });
        }
        else {
            this.callApi(_that);
        }
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            // console.log(item)
            console.log(status);
            if (status === 200) {
                var res = JSON.parse(response);
                if (item.alias === 'photo') {
                    _that.linkThunbnail = res.URL;
                }
                // console.log(item, res, status);
            }
            else {
                item.upload();
            }
        };
        this.uploader.onCompleteAll = function () {
            _that.callApi(_that);
        };
    };
    AddItemModalComponent.prototype.callApi = function (_that) {
        if (_that.linkThunbnail == undefined || _that.linkThunbnail == null) {
            _that.linkThunbnail = '';
        }
        _that.api_service.addItem(_that.category_id, _that.itemDetailForm.value.itemName, _that.itemDetailForm.value.itemDescription, _that.itemDetailForm.value.itemPrice, _that.itemDetailForm.value.itemTax, _that.linkThunbnail, status, _that.itemDetailForm.value.isStatic, _that.itemDetailForm.value.isFastLine, _that.itemDetailForm.value.isInSeat).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    _that.toastrService.info('Successfully added!');
                    _that.activeModal.close();
                    _that.activeModal.dismiss();
                }
                else {
                    _that.showSpinner = false;
                    _that.toastrService.error(res.msg);
                }
            }
            else {
                _that.showSpinner = false;
                _that.toastrService.error('An error has occurred, please try again later');
            }
        });
    };
    AddItemModalComponent.prototype.toggleItemStatus = function (status) {
        this.itemStatus = status;
    };
    AddItemModalComponent.prototype.readURL = function (event) {
        document.getElementById('add-item-icon').setAttribute('src', URL.createObjectURL(event.target.files[0]));
    };
    AddItemModalComponent.prototype.closeModal = function () {
        this.activeModal.dismiss();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AddItemModalComponent.prototype, "menu_id", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AddItemModalComponent.prototype, "category_id", void 0);
    AddItemModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'add-item-modal',
            template: __webpack_require__(/*! ./add-item-modal.component.html */ "./src/app/menu/add-item-modal/add-item-modal.component.html"),
            styles: [__webpack_require__(/*! ./add-item-modal.component.scss */ "./src/app/menu/add-item-modal/add-item-modal.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_7__["NgbActiveModal"],
            _services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            _services_file_service__WEBPACK_IMPORTED_MODULE_8__["FileService"],
            _nebular_theme_components_menu_menu_service__WEBPACK_IMPORTED_MODULE_5__["NbMenuService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__["CookieService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"]])
    ], AddItemModalComponent);
    return AddItemModalComponent;
}());



/***/ }),

/***/ "./src/app/menu/add-menu-modal/add-menu-modal.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/menu/add-menu-modal/add-menu-modal.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <span>Add Menu</span>\r\n  <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<form [formGroup]=\"addMenuForm\" (ngSubmit)=\"addMenu()\">\r\n  <div class=\"modal-body\">\r\n    <div class=\"form-group\">\r\n      <input type=\"text\" formControlName=\"newMenuName\" class=\"form-control\" [ngClass]=\"{'form-control-danger': addMenuForm.controls.newMenuName.invalid && (addMenuForm.controls.newMenuName.dirty || addMenuForm.controls.newMenuName.touched)}\" placeholder=\"New menu name\">\r\n    </div>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"submit\" class=\"btn btn-md btn-success\">Add</button>\r\n    <button class=\"btn btn-md btn-danger\" (click)=\"closeModal()\">Cancel</button>\r\n  </div>\r\n</form>"

/***/ }),

/***/ "./src/app/menu/add-menu-modal/add-menu-modal.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/menu/add-menu-modal/add-menu-modal.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/menu/add-menu-modal/add-menu-modal.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/menu/add-menu-modal/add-menu-modal.component.ts ***!
  \*****************************************************************/
/*! exports provided: AddMenuModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddMenuModalComponent", function() { return AddMenuModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal-ref */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal-ref.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AddMenuModalComponent = /** @class */ (function () {
    function AddMenuModalComponent(activeModal, api_service, cookieService, toastrService, fb) {
        this.activeModal = activeModal;
        this.api_service = api_service;
        this.cookieService = cookieService;
        this.toastrService = toastrService;
        this.fb = fb;
        this.currentUser = _common__WEBPACK_IMPORTED_MODULE_5__["Utils"].decodeJwt(this.cookieService.get('user'));
        this.addMenuForm = this.fb.group({
            newMenuName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(1)]],
        });
    }
    AddMenuModalComponent.prototype.addMenu = function () {
        var _this = this;
        if (this.addMenuForm.controls.newMenuName.invalid)
            return;
        // console.log(this.addMenuForm.value.newMenuName);
        this.api_service.addMenu(this.addMenuForm.value.newMenuName, this.currentUser.restaurant.id).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    _this.toastrService.info('Successfully added!');
                    _this.activeModal.close();
                    _this.activeModal.dismiss();
                }
                else {
                    _this.toastrService.error(res.msg);
                }
            }
            else {
                _this.toastrService.error('Cannot add this!');
            }
        });
    };
    AddMenuModalComponent.prototype.closeModal = function () {
        this.activeModal.dismiss();
    };
    AddMenuModalComponent.prototype.ngOnInit = function () {
    };
    AddMenuModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'add-menu-modal',
            template: __webpack_require__(/*! ./add-menu-modal.component.html */ "./src/app/menu/add-menu-modal/add-menu-modal.component.html"),
            styles: [__webpack_require__(/*! ./add-menu-modal.component.scss */ "./src/app/menu/add-menu-modal/add-menu-modal.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"], _services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_6__["CookieService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]])
    ], AddMenuModalComponent);
    return AddMenuModalComponent;
}());



/***/ }),

/***/ "./src/app/menu/menu.component.css":
/*!*****************************************!*\
  !*** ./src/app/menu/menu.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".page-menu #menu {\r\n    font-size: 25px;\r\n    border-bottom: 3px solid;\r\n}\r\n\r\n.page-menu .pending {\r\n    float: right;\r\n}\r\n\r\n.page-menu mat-select {\r\n    border-bottom: 2px solid;\r\n    font-size: 17px;\r\n}\r\n\r\n.row-height {\r\n    height: 60vh;\r\n    padding: 0.25rem;\r\n    overflow-y: scroll;\r\n    overflow-x: hidden !important;\r\n}\r\n\r\n.page-menu .category-menu, .page-menu .item-menu, .page-menu .item-detail {\r\n    overflow-y: scroll;\r\n    height: calc(100% - 65px);\r\n}\r\n\r\n.page-menu .item-detail {\r\n    padding-right: 15px;\r\n}\r\n\r\n.page-menu .bottom {\r\n    height: 10%;\r\n}\r\n\r\n.page-menu .bottom button {\r\n    margin-top: 20px;\r\n    margin-left: 10px;    \r\n    width: calc(100% - 20px);\r\n    height: 45px;\r\n}\r\n\r\n.page-menu .menu-item {\r\n    cursor: pointer;\r\n}\r\n\r\n.custom-font {\r\n    color: #26A9E9 !important;\r\n    padding: 1rem !important;\r\n}\r\n\r\n.btn-custom {\r\n    padding: 0.25rem !important;\r\n}\r\n\r\n.btn-custom > button {\r\n    padding-top: 12px !important;\r\n    padding-bottom: 12px !important;\r\n}\r\n\r\n.flex-space-around {\r\n    display: flex;\r\n    justify-content: space-around;\r\n}\r\n\r\n::ng-deep .mat-checkbox-checked.mat-accent .mat-checkbox-background, .mat-checkbox-indeterminate.mat-accent .mat-checkbox-background {\r\n    background-color: #2ab2ec !important;\r\n}\r\n\r\n.flex-center {\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    flex-direction: column;\r\n}\r\n\r\n.flex-space-between {\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: space-between;\r\n}\r\n\r\n.flex-end {\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: flex-end;\r\n\r\n}\r\n\r\n.flex-end i {\r\n    font-size: 45px;\r\n}\r\n\r\n.flex-end p {\r\n    margin-bottom: 0;\r\n}\r\n\r\nh4.modal-title {\r\n    font-weight: 500;\r\n    font-size: 1.125rem;\r\n}\r\n\r\n/* fix for modal with mat-select */\r\n\r\n::ng-deep .cdk-overlay-container {\r\n    z-index: 9999 !important;\r\n}\r\n\r\n.spinner-wrapper {\r\n    display: flex;\r\n    justify-content: center;\r\n    position: absolute;\r\n    width: 100%;\r\n    height: 800px;\r\n    align-items: center;\r\n    background: rgba(255, 255, 255, 0.3);\r\n    z-index: 5;\r\n}"

/***/ }),

/***/ "./src/app/menu/menu.component.html":
/*!******************************************!*\
  !*** ./src/app/menu/menu.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row page-menu\" *ngIf=\"isValid\">\r\n  <div class=\"col-12\" *ngIf=\"isClient; else isNotClient\">\r\n    <div class=\"row\">\r\n        <div class=\"col-6 col-lg-3\">\r\n            <mat-select [(ngModel)]=\"selectedRestaurantIndex\" (ngModelChange)=\"onSelectRestaurant($event)\">\r\n              <mat-option [value]=\"i\" *ngFor=\"let restaurant of restaurants; let i = index\">{{restaurant.name}}</mat-option>\r\n            </mat-select>\r\n        </div>\r\n        <div class=\"col-6 col-lg-9 flex-end\" *ngIf=\"currentUser.has_appetize === 'Yes'\">\r\n            <p (click)=\"smModal.show()\">Add items from Appetize</p>\r\n            <i class=\"nb-plus-circled\" (click)=\"smModal.show()\"></i>\r\n        </div>\r\n\r\n        <!-- <div class=\"col-6 col-lg-9 flex-end\" *ngIf=\"currentUser.has_ncr === 'Yes'\" (click)=\"onRefreshNCR()\" style=\"cursor:pointer;\">\r\n            <p>Refresh menu items</p>\r\n            <i class=\"nb-loop\" style=\"color: #1a8a1a;font-weight: bold; margin-left: 10px;\"></i>\r\n        </div> -->\r\n    </div>\r\n  </div>\r\n  <!-- <ng-template #isNotClient>\r\n    <div class=\"col-md-12 col-lg-12\">\r\n      <span id=\"menu\">\r\n        <span *ngIf=\"selectedRestaurant !== null\">{{ selectedRestaurant.name }}</span>\r\n        <span *ngIf=\"myMenu !== null\"> / </span>\r\n        <span *ngIf=\"myMenu !== null\">Menu</span> \r\n      </span>\r\n    </div>\r\n  </ng-template>\r\n  <br><br> -->\r\n</div>\r\n\r\n<div class=\"row\" *ngIf=\"isValid\">\r\n  <div class=\"col-6 col-md-6 col-lg-3\">\r\n    <nb-card>\r\n      <nb-card-header class=\"custom-font\">\r\n        CATEGORY\r\n      </nb-card-header>\r\n      <nb-card-body class=\"row-height\">\r\n        <div class=\"category-menu\">\r\n            <custom-menu \r\n              [items]=\"mnu_category\" \r\n              (onSelect)=\"onItemSelection($event)\" \r\n              (onEdit)=\"onEdit($event)\" \r\n              (onDelete)=\"onDelete($event)\">\r\n            </custom-menu>\r\n          </div>\r\n        </nb-card-body>\r\n        <nb-card-footer class=\"btn-custom\">\r\n          <button *ngIf=\"myMenu === null\" class=\"btn btn-info btn-semi-round btn-block\" (click)=\"addMenu()\">Add Menu</button>\r\n          <button *ngIf=\"myMenu !== null\" class=\"btn btn-info btn-semi-round btn-block\" (click)=\"addCategory()\">Add Category</button>\r\n        </nb-card-footer>\r\n    </nb-card>\r\n  </div>\r\n  <div class=\"col-6 col-md-6 col-lg-3\" *ngIf=\"category_id>0\">\r\n    <nb-card>\r\n      <nb-card-header class=\"custom-font\">\r\n        ITEM\r\n      </nb-card-header>\r\n      <nb-card-body class=\"row-height\">\r\n        <div class=\"item-menu\">\r\n            <custom-menu \r\n              [items]=\"mnu_item\" \r\n              [edit_enabled]=\"false\"\r\n              (onSelect)=\"onItemSelection($event)\" \r\n              (onEdit)=\"onEdit($event)\" \r\n              (onDelete)=\"onDelete($event)\">\r\n            </custom-menu>\r\n          </div>\r\n        </nb-card-body>\r\n        <nb-card-footer class=\"btn-custom\">\r\n          <button class=\"btn btn-info btn-semi-round btn-block\" (click)=\"addItem()\">Add Item</button>\r\n        </nb-card-footer>\r\n    </nb-card>\r\n  </div>\r\n  <div class=\"col-12 col-md-12 col-lg-6\" *ngIf=\"item_id>0 && category_id>0\">\r\n    <nb-card>\r\n      <nb-card-header class=\"custom-font\">\r\n        ITEM DETAIL\r\n      </nb-card-header>\r\n      <nb-card-body class=\"row-height\" style=\"padding: 15px;\">\r\n        <div class=\"spinner-wrapper\" *ngIf=\"showSpinner\">\r\n          <mat-spinner></mat-spinner>\r\n        </div>\r\n        <div class=\"item-detail\">\r\n            <form [formGroup]=\"itemDetailForm\" (ngSubmit)=\"submitChange()\" enctype=\"multipart/form-data\">\r\n              <!-- Item Name -->\r\n              <div class=\"form-group\">\r\n                <input type=\"text\" formControlName=\"itemName\" class=\"form-control\" [ngClass]=\"{'form-control-danger': itemDetailForm.controls.itemName.invalid && (itemDetailForm.controls.itemName.dirty || itemDetailForm.controls.itemName.touched)}\" id=\"exampleInputEmail1\" placeholder=\"Item name\">\r\n              </div>\r\n              <!-- Description -->\r\n              <div class=\"form-group\">\r\n                <textarea rows=\"8\" formControlName=\"itemDescription\" placeholder=\"Description, for example, ingredients, preparation, etc\" id=\"inputDescription\" class=\"form-control\"></textarea>\r\n              </div>\r\n        \r\n              <div class=\"row\">\r\n                <!-- Price -->\r\n                <div class=\"col-md-6\">\r\n                  <div class=\"form-group\">\r\n                    <label for=\"inputPrice\">Price ($)</label>\r\n                    <input type=\"number\" step=\"1.00\" formControlName=\"itemPrice\" class=\"form-control\" [ngClass]=\"{'form-control-danger': itemDetailForm.controls.itemPrice.invalid && (itemDetailForm.controls.itemPrice.dirty || itemDetailForm.controls.itemPrice.touched)}\" id=\"inputPrice\" placeholder=\"0.00\">\r\n                  </div>\r\n                </div>\r\n                <!-- Tax -->\r\n                <div class=\"col-md-6\">\r\n                  <div class=\"form-group\">\r\n                    <label for=\"inputTax\">Tax (%)</label>\r\n                    <input type=\"number\" step=\"0.01\" formControlName=\"itemTax\" class=\"form-control\" [ngClass]=\"{'form-control-danger': itemDetailForm.controls.itemTax.invalid && (itemDetailForm.controls.itemTax.dirty || itemDetailForm.controls.itemTax.touched)}\" id=\"inputTax\" placeholder=\"0.00\">\r\n                  </div>\r\n                </div>\r\n                <!-- Thumbnail Image -->\r\n                <div class=\"col-md-12\">\r\n                    <div class=\"form-group\">\r\n                      <label for=\"thumbnail\">Image</label>\r\n                      <div style=\"display: flex; flex-direction: column;\" class=\"form-control\" >\r\n                        <img id=\"menu-item-icon\" [src]=\"linkThunbnail || '../../assets/images/transparent_200x200.png'\" width=\"100\" height=\"100\" style=\"margin-bottom: 10px; border: 1px solid #dadfe6; border-radius: 50%;\">\r\n                        <!-- <i *ngIf=\"imageUploaded\" class=\"fa fa-check-circle\" style=\"margin-left: 20px;\"></i> -->\r\n                        <!-- <label *ngIf=\"imageUploadFailed\" style=\"margin-left: 20px;color:red;\">Upload failed!</label> -->\r\n                        <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" accept=\"image/*\" formControlName=\"thumbnail\" placeholder=\"Thumbnail\" (change)=\"readURL($event)\">\r\n                      </div>\r\n                    </div>\r\n                </div>\r\n                <!-- Item Status -->\r\n                <div class=\"col-md-12\">\r\n                  <div class=\"form-group\">\r\n                    <label for=\"item-status\">Item Status</label>\r\n                    <ngx-switcher\r\n                      [firstValue]=\"1\"\r\n                      [secondValue]=\"0\"\r\n                      [firstValueLabel]=\"'Available'\"\r\n                      [secondValueLabel]=\"'Not available'\"\r\n                      [value]=\"itemStatus\"\r\n                      (valueChange)=\"toggleItemStatus($event)\" id=\"item-status\" class=\"form-control\"> \r\n                    </ngx-switcher>\r\n                  </div>\r\n                </div>\r\n\r\n                <!-- Item Type -->\r\n                <div class=\"col-md-12\" *ngIf=\"selectedRestaurant.tnx_enabled === 'Yes'\">\r\n                  <div class=\"form-group\">\r\n                    <label>Item Type</label>\r\n                    <div class=\"form-control flex-space-around\">\r\n                        <!-- <mat-checkbox formControlName =\"isStatic\">Static</mat-checkbox> -->\r\n                        <mat-checkbox formControlName =\"isFastLine\">Fast Line</mat-checkbox>\r\n                        <mat-checkbox formControlName =\"isInSeat\">In Seat Delivery</mat-checkbox>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </form>\r\n        </div>\r\n        <!-- Submit Change Button -->\r\n      </nb-card-body>\r\n      <nb-card-footer class=\"btn-custom\">\r\n        <button class=\"btn btn-info btn-semi-round btn-block\" [ngClass]=\"{'disabled' : !imageUploaded}\" (click)=\"submitChanges()\">Submit Changes</button>\r\n      </nb-card-footer>\r\n    </nb-card>\r\n  </div>\r\n</div>\r\n\r\n  <div bsModal #smModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"dialog-sizes-name2\">\r\n    <div class=\"modal-dialog modal-sm\">\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n          <h4 id=\"dialog-sizes-name2\" class=\"modal-title pull-left\"> Appetize Request</h4>\r\n          <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"smModal.hide()\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n        </div>\r\n        <div class=\"modal-body flex-center\">\r\n            <mat-form-field>\r\n              <input matInput type=\"number\" [(ngModel)]=\"vendorID\" placeholder=\"Vendor Identifier\">\r\n            </mat-form-field>\r\n            <mat-form-field>\r\n              <mat-select placeholder=\"Select Category\" [(ngModel)]=\"selectedCategoryIndex\" (ngModelChange)=\"onSelectCategory($event)\">\r\n                <mat-option [value]=\"i\" *ngFor=\"let category of mnu_category; let i = index\">{{category.title}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"modal-footer\" style=\"padding: 5px;\">\r\n            <button type=\"submit\" class=\"btn btn-md btn-success\" style=\"width: 100%;\" (click)=\"addItemsFromAppetize()\" [disabled]=\"!vendorID\">Save</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n \r\n\r\n  "

/***/ }),

/***/ "./src/app/menu/menu.component.ts":
/*!****************************************!*\
  !*** ./src/app/menu/menu.component.ts ***!
  \****************************************/
/*! exports provided: MenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuComponent", function() { return MenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal.js");
/* harmony import */ var _add_menu_modal_add_menu_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./add-menu-modal/add-menu-modal.component */ "./src/app/menu/add-menu-modal/add-menu-modal.component.ts");
/* harmony import */ var _add_item_modal_add_item_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-item-modal/add-item-modal.component */ "./src/app/menu/add-item-modal/add-item-modal.component.ts");
/* harmony import */ var _add_cagetory_modal_add_cagetory_modal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./add-cagetory-modal/add-cagetory-modal.component */ "./src/app/menu/add-cagetory-modal/add-cagetory-modal.component.ts");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../common */ "./src/app/common.ts");
/* harmony import */ var _nebular_theme_components_menu_menu_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../@nebular/theme/components/menu/menu.service */ "./src/app/@nebular/theme/components/menu/menu.service.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_file_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../services/file.service */ "./src/app/services/file.service.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../models */ "./src/app/models/index.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var MenuComponent = /** @class */ (function () {
    function MenuComponent(modalService, api_service, menuService, cookieService, router, toastrService, fileService, fb) {
        this.modalService = modalService;
        this.api_service = api_service;
        this.menuService = menuService;
        this.cookieService = cookieService;
        this.router = router;
        this.toastrService = toastrService;
        this.fileService = fileService;
        this.fb = fb;
        this.isPending = true;
        this.has_items = true;
        this.myMenu = '';
        this.category_id = -1;
        this.item_id = -1;
        this.imageUploaded = true;
        this.imageUploadFailed = false;
        this.uploadPercent = 100;
        this.isClient = false;
        this.selectedCategoryIndex = 0;
        this.isValid = false;
        this.isDataFromCookies = false;
        this.showSpinner = false;
    }
    MenuComponent.prototype.ngOnInit = function () {
        this.self = this;
        this.selectedRestaurant = new _models__WEBPACK_IMPORTED_MODULE_14__["Restaurant"]();
        this.mnu_category = [];
        this.mnu_item = [];
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
        this.fetcRestaurant();
        this.itemDetailForm = this.fb.group({
            itemName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].minLength(1)]],
            itemDescription: ['', null],
            itemPrice: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].min(0)]],
            itemTax: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].min(0)]],
            thumbnail: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_11__["Validators"].minLength(1)]],
            isStatic: [0, null],
            isFastLine: [1, null],
            isInSeat: [0, null],
        });
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_13__["FileUploader"]({
            url: "https://api.paranoidfan.com/api/sdk/upload",
            headers: [{
                    name: 'Authkey',
                    value: 'BGta}*X2V1M6SCta}*XTV1E8'
                }]
        });
    };
    MenuComponent.prototype.onItemSelection = function (event) {
        var item = event;
        this.selectedItem = item;
        this.uploader.clearQueue();
        if (item.is_menu) {
            _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs('menu clicked');
            return;
        }
        if (item.is_category) {
            _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs('category clicked');
            this.category_id = item.category_id;
            this.item_id = -1;
            this.fetchItems();
            return;
        }
        if (item.is_item) {
            _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs('item clicked');
            this.item_id = item.item_id;
            this.linkThunbnail = item.thumbnail;
            this.itemDetailForm.setValue({ itemName: item.title, itemDescription: item.description, itemPrice: item.price, itemTax: item.tax, thumbnail: '', isStatic: item.is_static, isFastLine: item.is_fast_line, isInSeat: item.is_in_seat });
            this.imageUploaded = true;
            this.imageUploadFailed = false;
            this.itemStatus = item.is_available;
        }
    };
    MenuComponent.prototype.toggleItemStatus = function (status) {
        _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs('toggleItemStatus ' + status);
        this.itemStatus = status;
    };
    MenuComponent.prototype.fetcRestaurant = function () {
        var _this = this;
        if (this.currentUser.role === 0) {
            // is a client
            this.isClient = true;
            if (this.cookieService.check('restaurants-' + this.currentUser.client_id) && this.cookieService.get('restaurants-' + this.currentUser.client_id) != ''
                && this.cookieService.check('selected-restaurant-index-' + this.currentUser.client_id) && this.cookieService.get('selected-restaurant-index-' + this.currentUser.client_id) != ''
                && this.cookieService.check('selected-restaurant-' + this.currentUser.client_id) && this.cookieService.get('selected-restaurant-' + this.currentUser.client_id) != '') {
                this.restaurants = _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].decodeJwt(this.cookieService.get('restaurants-' + this.currentUser.client_id));
                this.selectedRestaurant = _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].decodeJwt(this.cookieService.get('selected-restaurant-' + this.currentUser.client_id));
                this.selectedRestaurantIndex = _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].decodeJwt(this.cookieService.get('selected-restaurant-index-' + this.currentUser.client_id));
                this.isDataFromCookies = true;
                this.isValid = true;
            }
            this.api_service.getRestaurantsByClientId(this.currentUser.client_id).subscribe(function (res) {
                if (res) {
                    if (!res.err) {
                        if (res.response && res.response.length > 0) {
                            _this.restaurants = res.response;
                            _this.cookieService.set('restaurants-' + _this.currentUser.client_id, _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].encodeJwt(_this.restaurants), null, '/');
                            if (_this.isDataFromCookies) {
                                // update restaurant data
                                _this.selectedRestaurant = _this.restaurants[_this.selectedRestaurantIndex];
                            }
                            else {
                                _this.selectedRestaurant = _this.restaurants[0];
                                _this.selectedRestaurantIndex = 0;
                                _this.cookieService.set('selected-restaurant-index-' + _this.currentUser.client_id, _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].encodeJwt(_this.selectedRestaurantIndex), null, '/');
                            }
                            _this.cookieService.set('selected-restaurant-' + _this.currentUser.client_id, _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].encodeJwt(_this.selectedRestaurant), null, '/');
                            _this.fetchMenu();
                            _this.isValid = true;
                        }
                        else {
                            _this.toastrService.error('You must create at least one concession.');
                        }
                    }
                    else {
                        _this.toastrService.error('Cannot fetch restaurants!');
                    }
                }
                else {
                    _this.toastrService.error('Cannot fetch restaurants!');
                }
            });
        }
        else {
            this.isValid = true;
            this.selectedRestaurant = this.currentUser.restaurant;
            this.fetchMenu();
        }
    };
    MenuComponent.prototype.fetchMenu = function () {
        var _this = this;
        if (this.myMenu !== null)
            this.myMenu = null;
        this.category_id = -1;
        this.item_id = -1;
        var restaurat_id = this.selectedRestaurant.id;
        this.api_service.getMenu(restaurat_id).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    if (res.response !== null) {
                        var menu = res.response;
                        _this.myMenu = menu;
                        _this.fetchCategory(_this.myMenu.id);
                    }
                }
                else {
                    _this.toastrService.error(res.msg);
                }
            }
            else {
                _this.toastrService.error('Cannot fetch menu!');
            }
        });
    };
    MenuComponent.prototype.fetchCategory = function (event) {
        var _this = this;
        var menu_id = event;
        this.category_id = -1;
        this.item_id = -1;
        this.api_service.getCategory(menu_id).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    if (res.response !== null) {
                        _this.mnu_category = [];
                        for (var iterCategory in res.response) {
                            _this.mnu_category.push({
                                title: res.response[iterCategory].name,
                                icon: 'nb-grid-a-outline',
                                is_category: true,
                                menu_id: menu_id,
                                thumbnail: '',
                                source: res.response[iterCategory].source,
                                category_id: res.response[iterCategory].id,
                                is_selected: false,
                                vendor_id: res.response[iterCategory].vendor_id,
                            });
                        }
                    }
                }
                else {
                    _this.toastrService.error(res.msg);
                }
            }
            else {
                _this.toastrService.error('Cannot fetch menu!');
            }
        });
    };
    MenuComponent.prototype.fetchItems = function () {
        var _this = this;
        this.item_id = -1;
        this.api_service.getItems(this.category_id).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    if (res.response !== null) {
                        _this.mnu_item = [];
                        for (var _i = 0, _a = res.response; _i < _a.length; _i++) {
                            var item = _a[_i];
                            _this.mnu_item.push({
                                title: item.name,
                                icon: 'nb-arrow-thin-right',
                                is_item: true,
                                item_id: item.id,
                                menu_id: _this.myMenu.id,
                                category_id: _this.category_id,
                                thumbnail: item.photo,
                                description: item.description,
                                price: item.price.toFixed(2),
                                tax: item.tax.toFixed(2),
                                is_available: item.is_available,
                                is_selected: false,
                                is_static: item.static,
                                is_fast_line: item.fast_line,
                                is_in_seat: item.in_seat
                            });
                        }
                    }
                }
                else {
                    _this.toastrService.error(res.msg);
                }
            }
            else {
                _this.toastrService.error('Cannot fetch items!');
            }
        });
    };
    MenuComponent.prototype.onSelectRestaurant = function (index) {
        _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs(this.currentUser);
        _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs('onSelectRestaurant ' + index);
        this.selectedRestaurant = this.restaurants[index];
        this.cookieService.set('selected-restaurant-' + this.currentUser.client_id, _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].encodeJwt(this.selectedRestaurant), null, '/');
        this.cookieService.set('selected-restaurant-index-' + this.currentUser.client_id, _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].encodeJwt(index), null, '/');
        this.fetchMenu();
    };
    MenuComponent.prototype.onSelectCategory = function (index) {
        _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs('onSelectCategory ' + index);
    };
    MenuComponent.prototype.addCategory = function () {
        var self = this;
        var activeModal = this.modalService.open(_add_cagetory_modal_add_cagetory_modal_component__WEBPACK_IMPORTED_MODULE_4__["AddCagetoryModalComponent"], { size: 'lg', container: 'nb-layout' });
        activeModal.close = function () {
            self.fetchCategory(self.myMenu.id);
        };
        activeModal.componentInstance.menu_id = this.myMenu.id;
    };
    MenuComponent.prototype.addMenu = function () {
        var self = this;
        var activeModal = this.modalService.open(_add_menu_modal_add_menu_modal_component__WEBPACK_IMPORTED_MODULE_2__["AddMenuModalComponent"], { size: 'lg', container: 'nb-layout' });
        activeModal.componentInstance.currentUser = this.currentUser;
        activeModal.close = function () {
            self.fetchMenu();
        };
        activeModal.componentInstance.menu_id = this.myMenu.id;
        activeModal.componentInstance.category_id = this.category_id;
    };
    MenuComponent.prototype.addItem = function () {
        var activeModal = this.modalService.open(_add_item_modal_add_item_modal_component__WEBPACK_IMPORTED_MODULE_3__["AddItemModalComponent"], { size: 'lg', container: 'nb-layout' });
        var self = this;
        activeModal.close = function () {
            self.fetchItems();
        };
        activeModal.componentInstance.category_id = this.category_id;
    };
    MenuComponent.prototype.onDelete = function (event) {
        var _this = this;
        var self = this;
        if (event.is_category) {
            this.api_service.deleteCategory(event.category_id).subscribe(function (res) {
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Deleted successfully!');
                        self.fetchCategory(event.menu_id);
                    }
                    else {
                        _this.toastrService.error(res.msg);
                    }
                }
                else {
                    _this.toastrService.error('Cannot fetch items!');
                }
            });
        }
        else if (event.is_item) {
            this.api_service.deleteItem(event.item_id).subscribe(function (res) {
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Deleted successfully!');
                        self.fetchItems();
                    }
                    else {
                        _this.toastrService.error(res.msg);
                    }
                }
                else {
                    _this.toastrService.error('Cannot fetch items!');
                }
            });
        }
    };
    MenuComponent.prototype.onEdit = function (event) {
        var _this = this;
        _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs('on edit item');
        var self = this;
        if (event.is_category) {
            this.api_service.updateCategory(event.category_id, event.menu_id, event.name, event.thumbnail, 0, event.vendor_id).subscribe(function (res) {
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Updated successfully!');
                        self.fetchCategory(event.menu_id);
                    }
                    else {
                        _this.toastrService.error(res.msg);
                    }
                }
                else {
                    _this.toastrService.error('Cannot fetch items!');
                }
            });
        }
        else if (event.is_item) {
            this.api_service.updateItem(event.item_id, event.category_id, event.name, event.description, event.price, event.tax, event.thumbnail, event.is_available, event.is_static, event.is_fast_line, event.is_in_seat).subscribe(function (res) {
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Updated successfully!');
                        self.fetchItems();
                    }
                    else {
                        _this.toastrService.error(res.msg);
                    }
                }
                else {
                    _this.toastrService.error('Cannot fetch items!');
                }
            });
        }
    };
    MenuComponent.prototype.submitChanges = function () {
        var _this = this;
        if (this.itemDetailForm.controls.itemName.invalid)
            return;
        if (this.itemDetailForm.controls.itemDescription.invalid)
            return;
        if (this.itemDetailForm.controls.itemPrice.invalid)
            return;
        if (this.itemDetailForm.controls.itemTax.invalid)
            return;
        this.showSpinner = true;
        this.itemDetailForm.value.isStatic = (this.itemDetailForm.value.isStatic) ? 1 : 0;
        this.itemDetailForm.value.isFastLine = (this.itemDetailForm.value.isFastLine) ? 1 : 0;
        this.itemDetailForm.value.isInSeat = (this.itemDetailForm.value.isInSeat) ? 1 : 0;
        // console.log(this.itemDetailForm.value.isStatic + '  ' + this.itemDetailForm.value.isFastLine + '  ' + this.itemDetailForm.value.isInSeat);
        var _that = this;
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            // console.log(item)
            console.log(status);
            if (status === 200) {
                var res = JSON.parse(response);
                if (item.alias === 'photo') {
                    _that.linkThunbnail = res.URL;
                }
                // console.log(item, res, status);
            }
            else {
                item.upload();
            }
        };
        this.uploader.onCompleteAll = function () {
            _that.callApi(_that);
        };
        if (this.uploader.queue.length > 0) {
            this.uploader.queue.forEach(function (item) {
                if (item.file.type.match(/image/)) {
                    item.alias = 'photo';
                    _this.uploader.onBuildItemForm = function (item, form) {
                        form.append('path', 'uploads/');
                    };
                    item.upload();
                }
            });
        }
        else {
            this.callApi(_that);
        }
    };
    MenuComponent.prototype.callApi = function (_that) {
        var _this = this;
        if (_that.linkThunbnail == undefined || _that.linkThunbnail == null)
            _that.linkThunbnail = '';
        _that.api_service.updateItem(_that.item_id, _that.category_id, _that.itemDetailForm.value.itemName, _that.itemDetailForm.value.itemDescription, _that.itemDetailForm.value.itemPrice, _that.itemDetailForm.value.itemTax, _that.linkThunbnail, _that.itemStatus, _that.itemDetailForm.value.isStatic, _that.itemDetailForm.value.isFastLine, _that.itemDetailForm.value.isInSeat).subscribe(function (res) {
            _this.showSpinner = false;
            if (res) {
                if (!res.err) {
                    _that.toastrService.info('Updated successfully!');
                    for (var item in _that.mnu_item) {
                        if (_that.mnu_item[item].item_id == _that.item_id) {
                            _that.mnu_item[item] = { title: _that.itemDetailForm.value.itemName, is_item: true, item_id: _that.item_id,
                                menu_id: _that.myMenu.id, category_id: _that.category_id, icon: 'nb-arrow-thin-right',
                                thumbnail: _that.linkThunbnail, description: _that.itemDetailForm.value.itemDescription,
                                price: _that.itemDetailForm.value.itemPrice, tax: _that.itemDetailForm.value.itemTax, is_available: _that.itemStatus,
                                is_static: _that.itemDetailForm.value.isStatic, is_fast_line: _that.itemDetailForm.value.isFastLine, is_in_seat: _that.itemDetailForm.value.isInSeat };
                            return;
                        }
                    }
                }
                else {
                    _that.toastrService.error(res.msg);
                }
            }
            else {
                _that.toastrService.error('Cannot fetch items!');
            }
            _that.uploader.clearQueue();
        });
    };
    MenuComponent.prototype.readURL = function (event) {
        var tempItem = this.uploader.queue[this.uploader.queue.length - 1];
        this.uploader.clearQueue();
        this.uploader.queue[0] = tempItem;
        document.getElementById('menu-item-icon').setAttribute('src', URL.createObjectURL(event.target.files[0]));
    };
    MenuComponent.prototype.addItemsFromAppetize = function () {
        var _this = this;
        if (this.mnu_category.length > 0) {
            this.api_service.addItemsFromAppetize(this.vendorID, this.mnu_category[this.selectedCategoryIndex].category_id).subscribe(function (res) {
                if (res) {
                    if (!res.err && res.response) {
                        _this.fetchMenu();
                    }
                    else {
                        _this.toastrService.error('Cannot fetch items!');
                    }
                }
                else {
                    _this.toastrService.error('Cannot fetch items!');
                }
            });
            this.smModal.hide();
        }
        else {
            this.toastrService.error('Add at least one category.');
        }
    };
    MenuComponent.prototype.onRefreshNCR = function () {
        var _this = this;
        _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs('refresh items');
        this.api_service.refreshItems().subscribe(function (res) {
            if (res) {
                if (!res.err && res.response) {
                    _this.toastrService.info('Menu items has been updated!');
                }
                else {
                    _this.toastrService.error('Cannot fetch items!');
                }
            }
            else {
                _this.toastrService.error('Cannot fetch items!');
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('smModal'),
        __metadata("design:type", ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_15__["ModalDirective"])
    ], MenuComponent.prototype, "smModal", void 0);
    MenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menu',
            template: __webpack_require__(/*! ./menu.component.html */ "./src/app/menu/menu.component.html"),
            styles: [__webpack_require__(/*! ./menu.component.css */ "./src/app/menu/menu.component.css")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_1__["NgbModal"],
            _services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"],
            _nebular_theme_components_menu_menu_service__WEBPACK_IMPORTED_MODULE_10__["NbMenuService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_7__["CookieService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _services_file_service__WEBPACK_IMPORTED_MODULE_12__["FileService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormBuilder"]])
    ], MenuComponent);
    return MenuComponent;
}());



/***/ }),

/***/ "./src/app/menu/menu.module.ts":
/*!*************************************!*\
  !*** ./src/app/menu/menu.module.ts ***!
  \*************************************/
/*! exports provided: MenuModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuModule", function() { return MenuModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _menu_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./menu.routing */ "./src/app/menu/menu.routing.ts");
/* harmony import */ var _menu_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./menu.component */ "./src/app/menu/menu.component.ts");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _add_menu_modal_add_menu_modal_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-menu-modal/add-menu-modal.component */ "./src/app/menu/add-menu-modal/add-menu-modal.component.ts");
/* harmony import */ var _add_cagetory_modal_add_cagetory_modal_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./add-cagetory-modal/add-cagetory-modal.component */ "./src/app/menu/add-cagetory-modal/add-cagetory-modal.component.ts");
/* harmony import */ var _add_item_modal_add_item_modal_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./add-item-modal/add-item-modal.component */ "./src/app/menu/add-item-modal/add-item-modal.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _custom_component_custom_menu_custom_menu_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../custom-component/custom-menu/custom-menu.component */ "./src/app/custom-component/custom-menu/custom-menu.component.ts");
/* harmony import */ var _custom_component_custom_edit_custom_edit_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../custom-component/custom-edit/custom-edit.component */ "./src/app/custom-component/custom-edit/custom-edit.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var _custom_component_custom_component_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../custom-component/custom-component.module */ "./src/app/custom-component/custom-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var MenuModule = /** @class */ (function () {
    function MenuModule() {
    }
    MenuModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_4__["ThemeModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_13__["RouterModule"].forChild(_menu_routing__WEBPACK_IMPORTED_MODULE_2__["MenuRoutingModule"]),
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__["Ng2SmartTableModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_10__["FileUploadModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatInputModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatProgressSpinnerModule"],
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_16__["ModalModule"].forRoot(),
                _custom_component_custom_component_module__WEBPACK_IMPORTED_MODULE_17__["CustomModule"]
            ],
            declarations: [
                _menu_component__WEBPACK_IMPORTED_MODULE_3__["MenuComponent"],
                _add_menu_modal_add_menu_modal_component__WEBPACK_IMPORTED_MODULE_6__["AddMenuModalComponent"],
                _add_cagetory_modal_add_cagetory_modal_component__WEBPACK_IMPORTED_MODULE_7__["AddCagetoryModalComponent"],
                _add_item_modal_add_item_modal_component__WEBPACK_IMPORTED_MODULE_8__["AddItemModalComponent"],
                _custom_component_custom_menu_custom_menu_component__WEBPACK_IMPORTED_MODULE_14__["CustomMenuComponent"],
                _custom_component_custom_edit_custom_edit_component__WEBPACK_IMPORTED_MODULE_15__["CustomEditComponent"],
            ],
            entryComponents: [
                _add_menu_modal_add_menu_modal_component__WEBPACK_IMPORTED_MODULE_6__["AddMenuModalComponent"],
                _add_cagetory_modal_add_cagetory_modal_component__WEBPACK_IMPORTED_MODULE_7__["AddCagetoryModalComponent"],
                _add_item_modal_add_item_modal_component__WEBPACK_IMPORTED_MODULE_8__["AddItemModalComponent"],
                _custom_component_custom_menu_custom_menu_component__WEBPACK_IMPORTED_MODULE_14__["CustomMenuComponent"],
                _custom_component_custom_edit_custom_edit_component__WEBPACK_IMPORTED_MODULE_15__["CustomEditComponent"],
            ]
        })
    ], MenuModule);
    return MenuModule;
}());



/***/ }),

/***/ "./src/app/menu/menu.routing.ts":
/*!**************************************!*\
  !*** ./src/app/menu/menu.routing.ts ***!
  \**************************************/
/*! exports provided: MenuRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuRoutingModule", function() { return MenuRoutingModule; });
/* harmony import */ var _menu_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./menu.component */ "./src/app/menu/menu.component.ts");

var MenuRoutingModule = [{
        path: '',
        component: _menu_component__WEBPACK_IMPORTED_MODULE_0__["MenuComponent"]
    }];


/***/ }),

/***/ "./src/app/models/index.ts":
/*!*********************************!*\
  !*** ./src/app/models/index.ts ***!
  \*********************************/
/*! exports provided: Restaurant */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _restaurant__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./restaurant */ "./src/app/models/restaurant.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Restaurant", function() { return _restaurant__WEBPACK_IMPORTED_MODULE_0__["Restaurant"]; });




/***/ }),

/***/ "./src/app/models/restaurant.ts":
/*!**************************************!*\
  !*** ./src/app/models/restaurant.ts ***!
  \**************************************/
/*! exports provided: Restaurant */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Restaurant", function() { return Restaurant; });
var Restaurant = /** @class */ (function () {
    function Restaurant() {
        this.id = 0;
        this.name = '';
        this.photo = '../../../assets/images/concession-stand.jpg';
        this.prepare_time = 5;
        this.booking_fee = 15;
        this.bank_account = '';
        this.status = 0;
        this.tnx_enabled = 'No';
        this.tnx_options = 'NONE';
        this.created_at = 0;
        this.updated_at = 0;
        this.map_pin_id = 0;
    }
    return Restaurant;
}());



/***/ })

}]);
//# sourceMappingURL=app-client-client-module~app-menu-menu-module.js.map