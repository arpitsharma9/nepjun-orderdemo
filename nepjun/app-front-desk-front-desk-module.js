(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-front-desk-front-desk-module"],{

/***/ "./src/app/custom-component/custom-component.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/custom-component/custom-component.module.ts ***!
  \*************************************************************/
/*! exports provided: CustomModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomModule", function() { return CustomModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _custom_confirm_custom_confirm_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./custom-confirm/custom-confirm.component */ "./src/app/custom-component/custom-confirm/custom-confirm.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { CustomEditComponent } from './custom-edit/custom-edit.component';
// import { CustomMenuComponent } from './custom-menu/custom-menu.component';
var CustomModule = /** @class */ (function () {
    function CustomModule() {
    }
    CustomModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_2__["ThemeModule"],
            ],
            declarations: [
                _custom_confirm_custom_confirm_component__WEBPACK_IMPORTED_MODULE_3__["CustomConfirmComponent"],
            ],
            entryComponents: [
                _custom_confirm_custom_confirm_component__WEBPACK_IMPORTED_MODULE_3__["CustomConfirmComponent"],
            ]
        })
    ], CustomModule);
    return CustomModule;
}());



/***/ }),

/***/ "./src/app/custom-component/custom-confirm/custom-confirm.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/custom-component/custom-confirm/custom-confirm.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <span>{{ title }}</span>\r\n  <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  {{ description }}\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button class=\"btn btn-md btn-success\" (click)=\"onClickOk()\">Ok</button>\r\n  <button class=\"btn btn-md btn-danger\" (click)=\"closeModal()\">Cancel</button>\r\n</div>"

/***/ }),

/***/ "./src/app/custom-component/custom-confirm/custom-confirm.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/custom-component/custom-confirm/custom-confirm.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/custom-component/custom-confirm/custom-confirm.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/custom-component/custom-confirm/custom-confirm.component.ts ***!
  \*****************************************************************************/
/*! exports provided: CustomConfirmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomConfirmComponent", function() { return CustomConfirmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal-ref */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal-ref.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CustomConfirmComponent = /** @class */ (function () {
    function CustomConfirmComponent(activeModal, api_service, cookieService, toastrService, fb) {
        this.activeModal = activeModal;
        this.api_service = api_service;
        this.cookieService = cookieService;
        this.toastrService = toastrService;
        this.fb = fb;
    }
    CustomConfirmComponent.prototype.onClickOk = function () {
        this.activeModal.close();
        this.closeModal();
    };
    CustomConfirmComponent.prototype.closeModal = function () {
        this.activeModal.dismiss();
    };
    CustomConfirmComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CustomConfirmComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CustomConfirmComponent.prototype, "description", void 0);
    CustomConfirmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'custom-confirm',
            template: __webpack_require__(/*! ./custom-confirm.component.html */ "./src/app/custom-component/custom-confirm/custom-confirm.component.html"),
            styles: [__webpack_require__(/*! ./custom-confirm.component.scss */ "./src/app/custom-component/custom-confirm/custom-confirm.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"], _services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]])
    ], CustomConfirmComponent);
    return CustomConfirmComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/active/active.component.html":
/*!*********************************************************!*\
  !*** ./src/app/front-desk/active/active.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\r\n  <nb-card-header class=\"flex-space-between\">\r\n    <p style=\"margin:0;\">Active Orders <span class=\"date-span\">(Last Updated on {{today | date:'EE h:mm a'}})</span></p>\r\n    <button mat-raised-button (click)=\"runAlgorithm()\">Assign Runners</button>\r\n  </nb-card-header>\r\n\r\n  <nb-card-body>\r\n    <div class=\"flex-center\">\r\n      <div class=\"search-wrapper\">\r\n        <input #search class=\"search\" [(ngModel)]=\"inputValue\" type=\"text\" placeholder=\"Search...\" (change)=\"onSearch(search.value)\" \r\n          (keydown.backspace)=\"onSearch(search.value)\" (keydown)=\"onSearch(search.value)\" (keydown.enter)=\"onSearch(search.value)\">\r\n          <i class=\"nb-close close-search\" (click)=\"clearSearch()\"></i>\r\n      </div>\r\n    </div>\r\n    <ng2-smart-table [settings]=\"settings\" [source]=\"source\"></ng2-smart-table>\r\n  </nb-card-body>\r\n</nb-card>\r\n"

/***/ }),

/***/ "./src/app/front-desk/active/active.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/front-desk/active/active.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n.search-wrapper {\n  position: relative;\n  width: 70%;\n  margin-bottom: 20px; }\n\n.search-wrapper input {\n    width: 100%;\n    border-radius: 4px; }\n\n.search-wrapper .close-search {\n    position: absolute;\n    right: 5px;\n    z-index: 2;\n    color: black;\n    top: 5px;\n    font-weight: bold;\n    cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/front-desk/active/active.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/front-desk/active/active.component.ts ***!
  \*******************************************************/
/*! exports provided: ActiveComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActiveComponent", function() { return ActiveComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ng2_smart_table_lib_data_source_local_local_data_source__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table/lib/data-source/local/local.data-source */ "./node_modules/ng2-smart-table/lib/data-source/local/local.data-source.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_observable_IntervalObservable__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/observable/IntervalObservable */ "./node_modules/rxjs-compat/_esm5/observable/IntervalObservable.js");
/* harmony import */ var _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../node_modules/@angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs_add_operator_takeWhile__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/add/operator/takeWhile */ "./node_modules/rxjs-compat/_esm5/add/operator/takeWhile.js");
/* harmony import */ var _customer_customer_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../customer/customer.component */ "./src/app/front-desk/customer/customer.component.ts");
/* harmony import */ var _shared_textarea_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../shared/textarea.component */ "./src/app/front-desk/shared/textarea.component.ts");
/* harmony import */ var _shared_buttonview_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../shared/buttonview.component */ "./src/app/front-desk/shared/buttonview.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var ActiveComponent = /** @class */ (function () {
    function ActiveComponent(apiService, cookieService, toastrService, router) {
        var _this = this;
        this.apiService = apiService;
        this.cookieService = cookieService;
        this.toastrService = toastrService;
        this.router = router;
        this.settings = {
            mode: 'inline',
            noDataMessage: '',
            hideSubHeader: true,
            actions: {
                add: false,
                edit: false,
                delete: false,
                save: false,
                cancel: false,
            },
            columns: {
                status: {
                    title: 'Status',
                    type: 'string',
                    filter: false,
                    sort: false,
                    valuePrepareFunction: function (status) {
                        if (status == 1)
                            return 'Accepted';
                        else if (status == 3)
                            return 'Preparing';
                        else if (status == 4)
                            return 'Assigned';
                        else if (status == 5)
                            return 'Out for Delivery';
                    },
                },
                order_id: {
                    title: 'Order#',
                    type: 'string',
                    filter: false,
                    sort: false,
                    sortDirection: 'desc',
                },
                customer_name: {
                    title: 'Customer',
                    type: 'custom',
                    renderComponent: _customer_customer_component__WEBPACK_IMPORTED_MODULE_12__["CustomerComponent"],
                    filter: false,
                    sort: false,
                },
                location_zipcode: {
                    title: 'Location',
                    type: 'string',
                    filter: false,
                    sort: false,
                },
                notes: {
                    title: 'Notes',
                    type: 'custom',
                    filter: false,
                    sort: false,
                    renderComponent: _shared_textarea_component__WEBPACK_IMPORTED_MODULE_13__["TextAreaComponent"],
                },
                accepted_at: {
                    title: 'Accepted At',
                    type: 'string',
                    valuePrepareFunction: function (timestamp) {
                        if (timestamp == 0)
                            return '';
                        var now = _this.pipe.transform(Date.now(), 'M/d/yy');
                        var createdAt = _this.pipe.transform(new Date(timestamp * 1000), 'M/d/yy');
                        if (now === createdAt) {
                            return 'Today, ' + _this.pipe.transform(new Date(timestamp * 1000), 'h:mm a');
                        }
                        else {
                            return _this.pipe.transform(new Date(timestamp * 1000), 'short');
                        }
                    },
                    filter: false,
                    sort: false,
                },
                view: {
                    title: 'Action',
                    type: 'custom',
                    filter: false,
                    sort: false,
                    renderComponent: _shared_buttonview_component__WEBPACK_IMPORTED_MODULE_14__["ButtonViewComponent"],
                },
            },
        };
        this.source = new ng2_smart_table_lib_data_source_local_local_data_source__WEBPACK_IMPORTED_MODULE_3__["LocalDataSource"]();
        this.alive = true;
        this.pipe = new _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_10__["DatePipe"]('en-US');
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
        this.getInfo();
    }
    ActiveComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createSocket();
        this.today = Date.now();
        // get active orders every 10 seconds only for front desk users
        if (this.currentUser.role === 2) {
            rxjs_observable_IntervalObservable__WEBPACK_IMPORTED_MODULE_9__["IntervalObservable"].create(10000)
                .takeWhile(function () { return _this.alive; }) // only fires when component is alive
                .subscribe(function () {
                _this.getInfo();
                _this.today = Date.now();
            });
        }
    };
    ActiveComponent.prototype.ngOnDestroy = function () {
        this.socket.disconnect();
        this.alive = false;
    };
    ActiveComponent.prototype.createSocket = function () {
        var _this = this;
        this.socket = socket_io_client__WEBPACK_IMPORTED_MODULE_7__(_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].SERVER_URL);
        this.socket.on('message', function (data) {
            _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs('Order was updated, Refreshing data now...', data);
            _this.getInfo();
        });
        this.socket.on('onconnected', function (data) {
            _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs('Socket.io was connected, user_id = ' + data.id);
        });
        this.socket.on('disconnect', function () {
            _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs('Socket connection was disconnected');
        });
    };
    ActiveComponent.prototype.getInfo = function () {
        var _this = this;
        this.apiService.get("orders/active/restaurant/" + this.currentUser.restaurant_id).subscribe(function (res) {
            if (!res.err) {
                _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs(res.response);
                _this.activeOrders = res.response;
                _this.source.load(_this.activeOrders);
            }
            else {
                _this.toastrService.error('Cannot fetch orders!');
            }
        });
    };
    ActiveComponent.prototype.runAlgorithm = function () {
        var _this = this;
        _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs('runAlgorithm');
        this.apiService.get("order/cluster/" + this.currentUser.restaurant_id)
            .subscribe(function (res) {
            _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs(res);
            _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs('assignRunners');
            _this.apiService.get("order/batches/assignRunners/" + _this.currentUser.restaurant_id)
                .subscribe(function (res) {
                _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs(res);
                if (!res.err) {
                    _this.toastrService.info('Runners have been assigned');
                }
                else {
                    _this.toastrService.error('Runners assignment failed!');
                }
            });
        });
    };
    ActiveComponent.prototype.clearSearch = function () {
        this.inputValue = '';
        this.onSearch();
    };
    ActiveComponent.prototype.onSearch = function (query) {
        if (query === void 0) { query = ''; }
        _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs(query);
        var operation = false;
        if (query === '') {
            operation = true;
        }
        this.source.setFilter([
            // fields we want to include in the search
            {
                field: 'order_id',
                search: query
            },
            {
                field: 'customer_name',
                search: query
            },
            {
                field: 'location_zipcode',
                search: query
            },
            {
                field: 'status',
                search: query
            }
        ], operation);
        // second parameter specifying whether to perform 'AND' or 'OR' search 
        // (meaning all columns should contain search query or at least one)
        // 'AND' by default, so changing to 'OR' by setting false here
    };
    ActiveComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'active',
            template: __webpack_require__(/*! ./active.component.html */ "./src/app/front-desk/active/active.component.html"),
            styles: [__webpack_require__(/*! ./active.component.scss */ "./src/app/front-desk/active/active.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], ActiveComponent);
    return ActiveComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/add-order/add-order.component.html":
/*!***************************************************************!*\
  !*** ./src/app/front-desk/add-order/add-order.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\" >\r\n    <h5 class=\"modal-title\">Add New Order</h5>\r\n    <i class=\"nb-close close-modal\" (click)=\"closeModal()\"></i>\r\n  </div>\r\n  \r\n  <div class=\"modal-body\">\r\n    <div class=\"receipt-wrapper\">\r\n      <div>\r\n        <div class=\"form-group row\">\r\n            <label class=\"col-sm-3 col-form-label\">Customer</label>\r\n            <div class=\"col-sm-9\">\r\n                <select matNativeControl required class=\"form-control\" [formControl]=\"customer\">\r\n                    <option [value]=customer.id *ngFor=\"let customer of customers\">{{customer.runner_name}}</option>\r\n                  </select>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label class=\"col-sm-3 col-form-label\">Location</label>\r\n            <div class=\"col-sm-9\">\r\n              <input type=\"text\"  class=\"form-control\" [formControl]=\"location\">\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label class=\"col-sm-3 col-form-label\">Meal</label>\r\n            <div class=\"col-sm-9\">\r\n              <input type=\"text\"  class=\"form-control\" [formControl]=\"meal\">\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label class=\"col-sm-3 col-form-label\">Notes</label>\r\n            <div class=\"col-sm-9\">\r\n              <input type=\"text\"  class=\"form-control\" [formControl]=\"notes\">\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label class=\"col-sm-3 col-form-label\">Frequency</label>\r\n            <div class=\"col-sm-9\">\r\n                <select matNativeControl required class=\"form-control\" [formControl]=\"frequency\">\r\n                    <option value=\"M\">Monday</option>\r\n                    <option value=\"Tu\">Tuesday</option>\r\n                    <option value=\"W\">Wednesday</option>\r\n                    <option value=\"Th\">Thrusday</option>\r\n                    <option value=\"F\">Friday</option>\r\n                    <option value=\"Sa\">Saturday</option>\r\n                    <option value=\"Sun\">Sunday</option>\r\n                  </select>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  <div class=\"modal-footer\" style=\"justify-content: space-between;\">\r\n    <button mat-raised-button style=\"width: 120px; background-color:#9c1818;\"\r\n      (click)=\"this.closeModal()\">Cancle</button>\r\n  \r\n    <button mat-raised-button style=\"width: 120px; background-color:#006400; color:white;\"\r\n      (click)=\"this.addNewOrder();this.closeModal()\">Save</button>\r\n  </div>"

/***/ }),

/***/ "./src/app/front-desk/add-order/add-order.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/front-desk/add-order/add-order.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between, .row-detail, .receipt-checkout-items {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between, .row-detail, .receipt-checkout-items {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n.runner-chooser {\n  width: 100%;\n  padding: 10px;\n  border: 1px solid #eaeef2;\n  border-radius: 4px;\n  margin: 15px 0; }\n\n.row-detail p {\n  margin: 5px 0; }\n\n.row-detail p:first-child {\n  width: 25%;\n  font-weight: bold; }\n\n.row-detail p:nth-child(2) {\n  width: 75%; }\n\n.receipt-wrapper {\n  padding: 10px;\n  border: 1px solid #663ab7;\n  border-radius: 4px;\n  margin-top: 10px;\n  color: white; }\n\n.receipt-checkout-items {\n  border-bottom: 1px solid #663ab7;\n  padding: 7px 0; }\n\n.receipt-checkout-items p {\n  font-size: 14px;\n  margin: 0; }\n\n.receipt-checkout-items span {\n  margin-right: 5px; }\n\n.checkout-total {\n  font-size: 13px;\n  padding-top: 15px;\n  padding-bottom: 7px;\n  border-bottom: 1px solid grey; }\n\n.checkout-total p {\n  margin: 0; }\n\n.modal-body {\n  color: white; }\n\ni.fa.fa-phone.callIcon {\n  font-size: 25px;\n  /* width: 20px; */\n  margin-right: 6px; }\n\ntextarea#comment {\n  width: 100%;\n  min-height: 90px;\n  border-radius: 10px;\n  padding-left: 10px;\n  padding-top: 10px;\n  background: transparent;\n  color: #fff;\n  border: 1px solid #663ab7; }\n\n.yourComments {\n  position: relative; }\n\n.yourComments .nb-edit {\n    position: absolute;\n    top: 8px;\n    left: 110px;\n    font-size: 25px; }\n\n.receipt-checkout-items.notes {\n  border: 1px solid #663ab7;\n  border-radius: 10px;\n  padding-left: 10px;\n  min-height: 85px;\n  display: flex;\n  align-items: flex-start;\n  padding-top: 8px; }\n"

/***/ }),

/***/ "./src/app/front-desk/add-order/add-order.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/front-desk/add-order/add-order.component.ts ***!
  \*************************************************************/
/*! exports provided: AddOrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddOrderComponent", function() { return AddOrderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal-ref */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal-ref.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../node_modules/@angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_ordering_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/ordering.service */ "./src/app/services/ordering.service.ts");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var AddOrderComponent = /** @class */ (function () {
    function AddOrderComponent(activeModal, orderingService, cookieService, modelService, apiService, toastrService, router) {
        this.activeModal = activeModal;
        this.orderingService = orderingService;
        this.cookieService = cookieService;
        this.modelService = modelService;
        this.apiService = apiService;
        this.toastrService = toastrService;
        this.router = router;
        this.customer = new _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormControl"]();
        this.location = new _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormControl"]();
        this.meal = new _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormControl"]();
        this.notes = new _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormControl"]();
        this.frequency = new _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormControl"]();
        this.cart = {
            cart_id: 0,
            user_id: 0,
            restaurant_id: 0,
            cart_qty: 0,
            cart_subtotal: 0,
            cart_tax: 0,
            cart_service_fee: 0,
            cart_delivery_fee: 0,
            cart_total: 0,
            items: []
        };
        this.pipe = new _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_8__["DatePipe"]('en-US');
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_4__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
    }
    AddOrderComponent.prototype.ngOnInit = function () {
        // Utils.logs(this.order)
        this.getCustomers();
    };
    AddOrderComponent.prototype.getCustomers = function () {
        var _this = this;
        this.apiService.get("runners/" + this.currentUser.restaurant_id).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    _this.customers = res.response;
                    // console.log("users",res.response);
                }
                else {
                    _this.toastrService.error('Cannot fetch users!');
                }
            }
            else {
                _this.toastrService.error('Cannot fetch users!');
            }
        });
    };
    AddOrderComponent.prototype.emptyCart = function (updateCart) {
        // console.log('emptyCart')
        var cart = {
            cart_id: (this.cart && this.cart.cart_id) ? this.cart.cart_id : 0,
            user_id: this.customer.value,
            restaurant_id: this.currentUser.restaurant.id,
            cart_qty: 0,
            cart_subtotal: 0,
            cart_tax: 0,
            cart_service_fee: 0,
            cart_delivery_fee: 0,
            cart_total: 0,
            items: []
        };
        this.cart = cart;
    };
    AddOrderComponent.prototype.getCart = function () {
        var _this = this;
        if (this.customer.value != 0) {
            this.orderingService.get("cart/" + this.customer.value)
                .subscribe(function (res) {
                if (!res.err) {
                    var carts = res.response;
                    var hasCartBeenUpdated_1 = false;
                    carts.forEach(function (cart) {
                        if (cart.restaurant_id === _this.currentUser.restaurant.id) {
                            _this.cart = cart;
                            hasCartBeenUpdated_1 = true;
                        }
                    });
                    if (!hasCartBeenUpdated_1) {
                        _this.emptyCart(false);
                    }
                }
            });
        }
    };
    AddOrderComponent.prototype.addNewOrder = function () {
        var _this = this;
        debugger;
        //console.log(this.customer.value);
        var customerDetail = this.customers.filter(function (o) { return o.id == _this.customer.value; });
        // console.log("customer details",customerDetail);
        this.order = {
            user_id: this.customer.value,
            items: this.cart.items,
            restaurant_id: this.currentUser.restaurant.id,
            restaurant_service_fee: this.currentUser.restaurant.booking_fee,
            restaurant_delivery_fee: this.currentUser.restaurant.delivery_fee,
            order_items_qty: this.cart.cart_qty,
            order_total: this.cart.cart_total,
            order_subtotal: this.cart.cart_subtotal,
            order_tax: this.cart.cart_tax,
            order_service_fee: this.cart.cart_service_fee,
            order_delivery_fee: this.cart.cart_delivery_fee,
            currency: this.currentUser.restaurant.currency,
            currency_symbol: this.currentUser.restaurant.currency_symbol,
            order_latitude: this.currentUser.restaurant.latitude,
            order_longitude: this.currentUser.restaurant.longitude,
            card_type: '',
            last4: 0,
            cart_id: this.cart.cart_id,
            customer_location: this.location.value,
            order_type: '',
            source: 'NONE',
            apple_pay_id: '',
            customer_name: customerDetail[0].runner_name,
            client_id: 0
        };
        this.orderingService.post('order/new/add', __assign({}, this.order, { items: JSON.stringify(this.cart.items) }))
            .subscribe(function (res) {
            debugger;
            //console.log("result",res)
            if (!res.err) {
                debugger;
                _this.emptyCart(true);
            }
        });
        //  console.log("data",this.customer.value,this.frequency.value,this.meal.value,this.location.value)
    };
    AddOrderComponent.prototype.closeModal = function () {
        this.activeModal.close();
    };
    AddOrderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'add-order',
            template: __webpack_require__(/*! ./add-order.component.html */ "./src/app/front-desk/add-order/add-order.component.html"),
            styles: [__webpack_require__(/*! ./add-order.component.scss */ "./src/app/front-desk/add-order/add-order.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"],
            _services_ordering_service__WEBPACK_IMPORTED_MODULE_10__["OrderingService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_6__["CookieService"],
            _ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_5__["NgbModal"],
            _services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], AddOrderComponent);
    return AddOrderComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/completed/completed.component.html":
/*!***************************************************************!*\
  !*** ./src/app/front-desk/completed/completed.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\r\n    <nb-card-header>\r\n      <div class=\"flex-space-between\">\r\n        <p style=\"margin:0\">Completed Orders</p> \r\n      </div>\r\n    </nb-card-header>\r\n  \r\n    <nb-card-body>\r\n      <ng2-smart-table [settings]=\"settings\" [source]=\"source\"></ng2-smart-table>\r\n    </nb-card-body>\r\n</nb-card>"

/***/ }),

/***/ "./src/app/front-desk/completed/completed.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/front-desk/completed/completed.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n"

/***/ }),

/***/ "./src/app/front-desk/completed/completed.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/front-desk/completed/completed.component.ts ***!
  \*************************************************************/
/*! exports provided: CompletedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletedComponent", function() { return CompletedComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ng2_smart_table_lib_data_source_local_local_data_source__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table/lib/data-source/local/local.data-source */ "./node_modules/ng2-smart-table/lib/data-source/local/local.data-source.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
/* harmony import */ var _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../node_modules/@angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs_add_operator_takeWhile__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/add/operator/takeWhile */ "./node_modules/rxjs-compat/_esm5/add/operator/takeWhile.js");
/* harmony import */ var _customer_customer_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../customer/customer.component */ "./src/app/front-desk/customer/customer.component.ts");
/* harmony import */ var _shared_textarea_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../shared/textarea.component */ "./src/app/front-desk/shared/textarea.component.ts");
/* harmony import */ var _shared_buttonview_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../shared/buttonview.component */ "./src/app/front-desk/shared/buttonview.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var CompletedComponent = /** @class */ (function () {
    function CompletedComponent(apiService, cookieService, toastrService, router) {
        this.apiService = apiService;
        this.cookieService = cookieService;
        this.toastrService = toastrService;
        this.router = router;
        this.settings = {
            mode: 'inline',
            noDataMessage: '',
            hideSubHeader: true,
            actions: {
                add: false,
                edit: false,
                delete: false,
                save: false,
                cancel: false,
            },
            columns: {
                status: {
                    title: 'Status',
                    type: 'string',
                    filter: false,
                    sort: false,
                    valuePrepareFunction: function (status) {
                        if (status == 2)
                            return 'Cancelled';
                        else if (status == 7)
                            return 'Completed';
                        else if (status == 8)
                            return 'Rejected';
                    },
                },
                id: {
                    title: 'Order#',
                    type: 'string',
                    filter: false,
                    sort: false,
                    sortDirection: 'desc',
                },
                customer_name: {
                    title: 'Customer',
                    type: 'custom',
                    renderComponent: _customer_customer_component__WEBPACK_IMPORTED_MODULE_9__["CustomerComponent"],
                    filter: false,
                    sort: false,
                },
                location_zipcode: {
                    title: 'Location',
                    type: 'string',
                    filter: false,
                    sort: false,
                },
                notes: {
                    title: 'Notes',
                    type: 'custom',
                    filter: false,
                    sort: false,
                    renderComponent: _shared_textarea_component__WEBPACK_IMPORTED_MODULE_10__["TextAreaComponent"],
                },
                date: {
                    title: 'Date',
                    type: 'string',
                    filter: false,
                    sort: false,
                },
                view: {
                    title: 'Action',
                    type: 'custom',
                    filter: false,
                    sort: false,
                    renderComponent: _shared_buttonview_component__WEBPACK_IMPORTED_MODULE_11__["ButtonViewComponent"],
                },
            },
        };
        this.source = new ng2_smart_table_lib_data_source_local_local_data_source__WEBPACK_IMPORTED_MODULE_3__["LocalDataSource"]();
        this.pipe = new _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"]('en-US');
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
        this.getInfo();
    }
    CompletedComponent.prototype.getInfo = function () {
        var _this = this;
        this.apiService.get("orders/completed/restaurant/" + this.currentUser.restaurant_id).subscribe(function (res) {
            if (!res.err) {
                var orders = res.response;
                orders.forEach(function (order) {
                    order.date = _this.getDate(order);
                });
                _this.source.load(res.response);
            }
            else {
                _this.toastrService.error('Cannot fetch orders!');
            }
        });
    };
    CompletedComponent.prototype.getDate = function (order) {
        switch (order.status) {
            case 2:
                return this.transformDate(order.cancelled_at);
            case 7:
                return this.transformDate(order.completed_at);
            case 8:
                return this.transformDate(order.rejected_by_user_at);
        }
    };
    CompletedComponent.prototype.transformDate = function (time) {
        return this.pipe.transform(new Date(time * 1000), 'M/d/yy, h:mm a');
    };
    CompletedComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'completed',
            template: __webpack_require__(/*! ./completed.component.html */ "./src/app/front-desk/completed/completed.component.html"),
            styles: [__webpack_require__(/*! ./completed.component.scss */ "./src/app/front-desk/completed/completed.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], CompletedComponent);
    return CompletedComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/customer/customer.component.html":
/*!*************************************************************!*\
  !*** ./src/app/front-desk/customer/customer.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\" style=\"padding: 10px 16px;\">\r\n  <h5 class=\"modal-title\">Customer Info</h5>\r\n  <i class=\"nb-close close-modal\" (click)=\"closeModal()\"></i>\r\n</div>\r\n<div class=\"modal-body\" *ngIf=\"order\">\r\n  <div class=\"row-detail\">\r\n    <p>Name</p>\r\n    <p>{{order.customer_name}}</p>\r\n  </div>\r\n  <div class=\"row-detail\">\r\n    <p>Location</p>\r\n    <p>{{order.location}}</p>\r\n  </div>\r\n  <div class=\"row-detail\">\r\n    <p>Phone</p>\r\n    <p>{{ order.created_at }}</p>\r\n  </div>\r\n  <div class=\"row-detail\">\r\n    <p>Email</p>\r\n    <p>{{ order.email }}</p>\r\n  </div>\r\n  <div class=\"row-detail\" *ngIf=\"order.driver_licence\">\r\n    <p>Driver Licence</p>\r\n    <p><img [src]=\"order.driver_licence\"></p>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/front-desk/customer/customer.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/front-desk/customer/customer.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between, .row-detail {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between, .row-detail {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n.customer-name {\n  text-decoration: underline;\n  -webkit-text-decoration-color: #663ab7;\n          text-decoration-color: #663ab7;\n  cursor: pointer; }\n\n.customer-name:hover {\n  color: #663ab7; }\n\n.row-detail {\n  align-items: flex-start;\n  width: 100%;\n  color: white; }\n\n.row-detail p {\n    margin: 5px 0; }\n\n.row-detail p:first-child {\n    width: 200px;\n    font-weight: bold; }\n\n.row-detail p:nth-child(2) {\n    width: 100%; }\n\n.row-detail img {\n    max-width: 350px;\n    height: 200px;\n    border-radius: 4px;\n    width: 100%; }\n"

/***/ }),

/***/ "./src/app/front-desk/customer/customer.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/front-desk/customer/customer.component.ts ***!
  \***********************************************************/
/*! exports provided: ModalComponent, CustomerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerComponent", function() { return CustomerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalComponent = /** @class */ (function () {
    function ModalComponent(activeModal) {
        this.activeModal = activeModal;
    }
    ModalComponent.prototype.ngOnInit = function () {
    };
    ModalComponent.prototype.closeModal = function () {
        this.activeModal.close();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ModalComponent.prototype, "order", void 0);
    ModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'customer-modal',
            template: __webpack_require__(/*! ./customer.component.html */ "./src/app/front-desk/customer/customer.component.html"),
            styles: [__webpack_require__(/*! ./customer.component.scss */ "./src/app/front-desk/customer/customer.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"]])
    ], ModalComponent);
    return ModalComponent;
}());

var CustomerComponent = /** @class */ (function () {
    function CustomerComponent(modalService) {
        this.modalService = modalService;
    }
    CustomerComponent.prototype.ngOnInit = function () {
    };
    CustomerComponent.prototype.openModal = function () {
        var activeModal = this.modalService.open(ModalComponent, { size: 'lg', container: 'nb-layout' });
        activeModal.componentInstance.order = this.rowData;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CustomerComponent.prototype, "value", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CustomerComponent.prototype, "rowData", void 0);
    CustomerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'customer',
            template: "<p class=\"customer-name\" (click)=\"openModal()\">{{rowData.customer_name}}</p>",
            styles: [__webpack_require__(/*! ./customer.component.scss */ "./src/app/front-desk/customer/customer.component.scss")],
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbModal"]])
    ], CustomerComponent);
    return CustomerComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/front-desk-menu.ts":
/*!***********************************************!*\
  !*** ./src/app/front-desk/front-desk-menu.ts ***!
  \***********************************************/
/*! exports provided: MENU_ITEMS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MENU_ITEMS", function() { return MENU_ITEMS; });
var MENU_ITEMS = [
    {
        title: 'New Orders',
        icon: 'nb-notifications',
        link: '/frontdesk/pending',
        home: true,
    },
    {
        title: 'Active Orders',
        icon: 'nb-compose',
        link: '/frontdesk/active',
    },
    {
        title: 'Completed Orders',
        icon: 'nb-checkmark',
        link: '/frontdesk/completed',
    },
    {
        title: 'Runners Map',
        icon: 'nb-location',
        link: '/frontdesk/map/runners',
    },
    {
        title: 'Runners',
        icon: 'nb-person',
        link: '/frontdesk/runners',
    },
    {
        title: 'Routes',
        icon: 'nb-person',
        link: '/frontdesk/routes',
    },
];


/***/ }),

/***/ "./src/app/front-desk/front-desk-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/front-desk/front-desk-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: FrontDeskRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrontDeskRoutingModule", function() { return FrontDeskRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _front_desk_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./front-desk.component */ "./src/app/front-desk/front-desk.component.ts");
/* harmony import */ var _pending_pending_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pending/pending.component */ "./src/app/front-desk/pending/pending.component.ts");
/* harmony import */ var _completed_completed_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./completed/completed.component */ "./src/app/front-desk/completed/completed.component.ts");
/* harmony import */ var _active_active_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./active/active.component */ "./src/app/front-desk/active/active.component.ts");
/* harmony import */ var _runners_map_runners_map_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./runners-map/runners-map.component */ "./src/app/front-desk/runners-map/runners-map.component.ts");
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./users/users.component */ "./src/app/front-desk/users/users.component.ts");
/* harmony import */ var _routes_routes_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./routes/routes.component */ "./src/app/front-desk/routes/routes.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [{
        path: '',
        component: _front_desk_component__WEBPACK_IMPORTED_MODULE_2__["FrontDeskComponent"],
        children: [{
                path: 'pending',
                component: _pending_pending_component__WEBPACK_IMPORTED_MODULE_3__["PendingComponent"],
            }, {
                path: 'active',
                component: _active_active_component__WEBPACK_IMPORTED_MODULE_5__["ActiveComponent"],
            }, {
                path: 'completed',
                component: _completed_completed_component__WEBPACK_IMPORTED_MODULE_4__["CompletedComponent"],
            }, {
                path: 'map/runners',
                component: _runners_map_runners_map_component__WEBPACK_IMPORTED_MODULE_6__["RunnersMapComponent"],
            }, {
                path: 'runners',
                component: _users_users_component__WEBPACK_IMPORTED_MODULE_7__["UsersComponent"],
            }, {
                path: 'routes',
                component: _routes_routes_component__WEBPACK_IMPORTED_MODULE_8__["RoutesComponent"],
            }, {
                path: '**',
                component: _pending_pending_component__WEBPACK_IMPORTED_MODULE_3__["PendingComponent"],
            }],
    }];
var FrontDeskRoutingModule = /** @class */ (function () {
    function FrontDeskRoutingModule() {
    }
    FrontDeskRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], FrontDeskRoutingModule);
    return FrontDeskRoutingModule;
}());



/***/ }),

/***/ "./src/app/front-desk/front-desk.component.ts":
/*!****************************************************!*\
  !*** ./src/app/front-desk/front-desk.component.ts ***!
  \****************************************************/
/*! exports provided: FrontDeskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrontDeskComponent", function() { return FrontDeskComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _front_desk_menu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./front-desk-menu */ "./src/app/front-desk/front-desk-menu.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FrontDeskComponent = /** @class */ (function () {
    function FrontDeskComponent() {
        this.menu = _front_desk_menu__WEBPACK_IMPORTED_MODULE_1__["MENU_ITEMS"];
    }
    FrontDeskComponent.prototype.ngOnInit = function () {
    };
    FrontDeskComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'front-desk',
            template: "\n    <food-ordering-layout>\n      <nb-menu [items]=\"menu\"></nb-menu>\n      <router-outlet></router-outlet>\n    </food-ordering-layout>\n  ",
        }),
        __metadata("design:paramtypes", [])
    ], FrontDeskComponent);
    return FrontDeskComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/front-desk.module.ts":
/*!*************************************************!*\
  !*** ./src/app/front-desk/front-desk.module.ts ***!
  \*************************************************/
/*! exports provided: FrontDeskModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrontDeskModule", function() { return FrontDeskModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _pending_pending_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pending/pending.component */ "./src/app/front-desk/pending/pending.component.ts");
/* harmony import */ var _completed_completed_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./completed/completed.component */ "./src/app/front-desk/completed/completed.component.ts");
/* harmony import */ var _front_desk_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./front-desk.component */ "./src/app/front-desk/front-desk.component.ts");
/* harmony import */ var _front_desk_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./front-desk-routing.module */ "./src/app/front-desk/front-desk-routing.module.ts");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./order-detail/order-detail.component */ "./src/app/front-desk/order-detail/order-detail.component.ts");
/* harmony import */ var _runner_dropdown_runner_dropdown_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./runner-dropdown/runner-dropdown.component */ "./src/app/front-desk/runner-dropdown/runner-dropdown.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _custom_component_custom_component_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../custom-component/custom-component.module */ "./src/app/custom-component/custom-component.module.ts");
/* harmony import */ var _customer_customer_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./customer/customer.component */ "./src/app/front-desk/customer/customer.component.ts");
/* harmony import */ var _shared_buttonview_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shared/buttonview.component */ "./src/app/front-desk/shared/buttonview.component.ts");
/* harmony import */ var _shared_textarea_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shared/textarea.component */ "./src/app/front-desk/shared/textarea.component.ts");
/* harmony import */ var _active_active_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./active/active.component */ "./src/app/front-desk/active/active.component.ts");
/* harmony import */ var _runners_map_runners_map_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./runners-map/runners-map.component */ "./src/app/front-desk/runners-map/runners-map.component.ts");
/* harmony import */ var _routes_routes_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./routes/routes.component */ "./src/app/front-desk/routes/routes.component.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./users/users.component */ "./src/app/front-desk/users/users.component.ts");
/* harmony import */ var _add_order_add_order_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./add-order/add-order.component */ "./src/app/front-desk/add-order/add-order.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var FrontDeskModule = /** @class */ (function () {
    function FrontDeskModule() {
    }
    FrontDeskModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _front_desk_routing_module__WEBPACK_IMPORTED_MODULE_5__["FrontDeskRoutingModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_6__["ThemeModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_7__["Ng2SmartTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"],
                _custom_component_custom_component_module__WEBPACK_IMPORTED_MODULE_11__["CustomModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_18__["AgmCoreModule"]
            ],
            declarations: [
                _pending_pending_component__WEBPACK_IMPORTED_MODULE_2__["PendingComponent"],
                _completed_completed_component__WEBPACK_IMPORTED_MODULE_3__["CompletedComponent"],
                _front_desk_component__WEBPACK_IMPORTED_MODULE_4__["FrontDeskComponent"],
                _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_8__["OrderDetailComponent"],
                _shared_buttonview_component__WEBPACK_IMPORTED_MODULE_13__["ButtonViewComponent"],
                _shared_textarea_component__WEBPACK_IMPORTED_MODULE_14__["TextAreaComponent"],
                _customer_customer_component__WEBPACK_IMPORTED_MODULE_12__["CustomerComponent"],
                _customer_customer_component__WEBPACK_IMPORTED_MODULE_12__["ModalComponent"],
                _shared_textarea_component__WEBPACK_IMPORTED_MODULE_14__["NotesModalComponent"],
                _active_active_component__WEBPACK_IMPORTED_MODULE_15__["ActiveComponent"],
                _runners_map_runners_map_component__WEBPACK_IMPORTED_MODULE_16__["RunnersMapComponent"],
                _users_users_component__WEBPACK_IMPORTED_MODULE_19__["UsersComponent"],
                _routes_routes_component__WEBPACK_IMPORTED_MODULE_17__["RoutesComponent"],
                _runner_dropdown_runner_dropdown_component__WEBPACK_IMPORTED_MODULE_9__["RunnerDropdownComponent"],
                _add_order_add_order_component__WEBPACK_IMPORTED_MODULE_20__["AddOrderComponent"]
            ],
            entryComponents: [
                _runner_dropdown_runner_dropdown_component__WEBPACK_IMPORTED_MODULE_9__["RunnerDropdownComponent"],
                _shared_buttonview_component__WEBPACK_IMPORTED_MODULE_13__["ButtonViewComponent"],
                _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_8__["OrderDetailComponent"],
                _add_order_add_order_component__WEBPACK_IMPORTED_MODULE_20__["AddOrderComponent"],
                _customer_customer_component__WEBPACK_IMPORTED_MODULE_12__["CustomerComponent"],
                _customer_customer_component__WEBPACK_IMPORTED_MODULE_12__["ModalComponent"],
                _shared_textarea_component__WEBPACK_IMPORTED_MODULE_14__["TextAreaComponent"],
                _shared_textarea_component__WEBPACK_IMPORTED_MODULE_14__["NotesModalComponent"],
            ]
        })
    ], FrontDeskModule);
    return FrontDeskModule;
}());



/***/ }),

/***/ "./src/app/front-desk/order-detail/order-detail.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/front-desk/order-detail/order-detail.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n    <h5 class=\"modal-title\">Order Detail</h5>\r\n    <i class=\"nb-close close-modal\" (click)=\"closeModal()\"></i>\r\n</div>\r\n<div class=\"modal-body\">\r\n      <ng2-smart-table [settings]=\"settings\" [source]=\"source\"></ng2-smart-table>\r\n</div>\r\n\r\n<div class=\"modal-footer flex-space-between\" style=\"padding: 0.7rem 1.25rem;\">\r\n    <div *ngIf=\"order.status == 4 || order.status == 1\" style=\"min-width: 44%;\">\r\n        <p *ngIf=\"order.runner_id != 0\" style=\"padding-bottom: 12px;\"><strong>Assigned Runner: </strong> <span>{{order.runner_name}}</span></p>\r\n        <div class=\"runner-chooser\" *ngIf=\"order.is_order_accepted_by_runner !== 'Accepted'\">\r\n            <mat-select [(ngModel)]=\"selectedRunnerId\" [placeholder]=\"order.status == 1 ? 'Select Runner' : 'Change Runner'\">\r\n                <mat-option [value]=\"runner.id\" *ngFor=\"let runner of runners; let i = index\">{{runner.runner_name}} | Pending Orders: {{runner.pending_orders}}</mat-option>\r\n            </mat-select>\r\n        </div>\r\n        <button mat-raised-button style=\"width: 120px;margin-top: 10px;\" (click)=\"onAssignRunner()\">Save</button>\r\n    </div>\r\n\r\n    <button *ngIf=\"order.status == 0\" mat-raised-button style=\"width: 120px;\" (click)=\"onCancelOrder()\">Cancel Order</button>\r\n    <button *ngIf=\"order.status == 0\" mat-raised-button color=\"primary\" style=\"width: 120px;\" (click)=\"onAcceptOrder()\">Accept</button>\r\n    <button *ngIf=\"order.status == 2 || order.status == 5 || order.status == 6\" (click)=\"closeModal()\" \r\n        mat-raised-button style=\"width: 120px;\" (click)=\"closeModal()\">OK</button>\r\n</div>"

/***/ }),

/***/ "./src/app/front-desk/order-detail/order-detail.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/front-desk/order-detail/order-detail.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n.runner-chooser {\n  width: 100%;\n  padding: 10px;\n  border: 1px solid #eaeef2;\n  border-radius: 4px;\n  margin: 15px 0; }\n\n.alert-message {\n  width: 100%;\n  padding-top: 15px;\n  margin: 0; }\n\n.modal-footer {\n  color: white; }\n"

/***/ }),

/***/ "./src/app/front-desk/order-detail/order-detail.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/front-desk/order-detail/order-detail.component.ts ***!
  \*******************************************************************/
/*! exports provided: OrderDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailComponent", function() { return OrderDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal-ref */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal-ref.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ng2_smart_table_lib_data_source_local_local_data_source__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-smart-table/lib/data-source/local/local.data-source */ "./node_modules/ng2-smart-table/lib/data-source/local/local.data-source.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OrderDetailComponent = /** @class */ (function () {
    function OrderDetailComponent(activeModal, apiService, toastrService) {
        this.activeModal = activeModal;
        this.apiService = apiService;
        this.toastrService = toastrService;
        this.source = new ng2_smart_table_lib_data_source_local_local_data_source__WEBPACK_IMPORTED_MODULE_4__["LocalDataSource"]();
    }
    OrderDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.settings = {
            mode: 'inline',
            hideSubHeader: true,
            actions: {
                add: false,
                edit: false,
                delete: false,
                save: false,
                cancel: false,
            },
            columns: {
                name: {
                    title: 'Item',
                    type: 'string',
                    filter: false,
                    sort: false,
                },
                amounts: {
                    title: 'Qty',
                    type: 'string',
                    filter: false,
                    sort: false,
                },
                price: {
                    title: 'Price',
                    type: 'string',
                    filter: false,
                    sort: false,
                    valuePrepareFunction: function (price) {
                        return _this.order.currency_symbol + price.toFixed(2);
                    },
                }
            },
        };
        this.source.load(this.order.items);
        this.getRunners();
    };
    OrderDetailComponent.prototype.onCancelOrder = function () {
        this.closeModal();
        this.updateOrderStatus(2);
    };
    OrderDetailComponent.prototype.onAcceptOrder = function () {
        this.closeModal();
        this.updateOrderStatus(1);
    };
    OrderDetailComponent.prototype.onAssignRunner = function () {
        var _this = this;
        this.closeModal();
        console.log('onAssignRunner');
        console.log({ batch_id: this.order.batch_id, runner_id: this.selectedRunnerId });
        this.apiService.post("order/batch/assignRunner", { batch_id: this.order.batch_id, runner_id: this.selectedRunnerId })
            .subscribe(function (res) {
            if (!res.err) {
                _this.toastrService.info('Runner has been assigned');
            }
            else {
                _this.toastrService.error('Runner assignment failed!');
            }
        });
    };
    OrderDetailComponent.prototype.updateOrderStatus = function (status) {
        var _this = this;
        this.apiService.updateOrderStatus(this.order.id, status).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    _this.toastrService.info('Updated successfully');
                    _this.order.status = status;
                }
                else {
                    _this.toastrService.error('Update Failed!');
                }
            }
            else {
                _this.toastrService.error('Update Failed!');
            }
        });
    };
    OrderDetailComponent.prototype.closeModal = function () {
        this.activeModal.close();
    };
    OrderDetailComponent.prototype.getRunners = function () {
        var _this = this;
        this.apiService.get("runners/" + this.order.restaurant_id)
            .subscribe(function (res) {
            console.log(res);
            if (!res.err && res.response) {
                _this.runners = res.response;
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderDetailComponent.prototype, "order", void 0);
    OrderDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'order-detail',
            template: __webpack_require__(/*! ./order-detail.component.html */ "./src/app/front-desk/order-detail/order-detail.component.html"),
            styles: [__webpack_require__(/*! ./order-detail.component.scss */ "./src/app/front-desk/order-detail/order-detail.component.scss")],
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"],
            _services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]])
    ], OrderDetailComponent);
    return OrderDetailComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/pending/pending.component.html":
/*!***********************************************************!*\
  !*** ./src/app/front-desk/pending/pending.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\r\n  <nb-card-header class=\"flex-space-between\">\r\n    <p style=\"margin:0\">New Orders <span class=\"date-span\">(Last Updated on {{today | date:'EE h:mm a'}})</span></p>\r\n    <p><i class=\"nb-plus\" (click)=\"addNewOrderModal()\"></i></p>\r\n    <!-- <img src=\"../../../assets/images/volume_off.png\" alt=\"volume_off\" id=\"btn-enable-audio\" (click)=\"enableAudio()\" *ngIf=\"showEnableAudio\">\r\n    <img src=\"../../../assets/images/volume_up.png\" alt=\"volume_up\" (click)=\"disableAudio()\" *ngIf=\"!showEnableAudio\"> -->\r\n  </nb-card-header>\r\n\r\n  <nb-card-body>\r\n    <ng2-smart-table [settings]=\"settings\" [source]=\"source\"></ng2-smart-table>\r\n  </nb-card-body>\r\n</nb-card>"

/***/ }),

/***/ "./src/app/front-desk/pending/pending.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/front-desk/pending/pending.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n.nb-plus {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-size: 1.75rem !important;\n  padding: 0.375rem 0.5rem;\n  background: #c9b5da;\n  color: #000000;\n  font-weight: 600;\n  border-radius: 0.375rem;\n  width: 100px; }\n"

/***/ }),

/***/ "./src/app/front-desk/pending/pending.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/front-desk/pending/pending.component.ts ***!
  \*********************************************************/
/*! exports provided: PendingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PendingComponent", function() { return PendingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ng2_smart_table_lib_data_source_local_local_data_source__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table/lib/data-source/local/local.data-source */ "./node_modules/ng2-smart-table/lib/data-source/local/local.data-source.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_observable_IntervalObservable__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/observable/IntervalObservable */ "./node_modules/rxjs-compat/_esm5/observable/IntervalObservable.js");
/* harmony import */ var _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../node_modules/@angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs_add_operator_takeWhile__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/add/operator/takeWhile */ "./node_modules/rxjs-compat/_esm5/add/operator/takeWhile.js");
/* harmony import */ var _customer_customer_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../customer/customer.component */ "./src/app/front-desk/customer/customer.component.ts");
/* harmony import */ var _shared_textarea_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../shared/textarea.component */ "./src/app/front-desk/shared/textarea.component.ts");
/* harmony import */ var _shared_buttonview_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../shared/buttonview.component */ "./src/app/front-desk/shared/buttonview.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal.js");
/* harmony import */ var _add_order_add_order_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../add-order/add-order.component */ "./src/app/front-desk/add-order/add-order.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var PendingComponent = /** @class */ (function () {
    function PendingComponent(apiService, modelService, cookieService, toastrService, router) {
        var _this = this;
        this.apiService = apiService;
        this.modelService = modelService;
        this.cookieService = cookieService;
        this.toastrService = toastrService;
        this.router = router;
        this.settings = {
            mode: 'inline',
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmCreate: true,
            },
            noDataMessage: '',
            hideSubHeader: false,
            actions: {
                add: false,
                edit: false,
                delete: false,
                save: false,
                cancel: false,
            },
            columns: {
                status: {
                    title: 'Status',
                    type: 'string',
                    filter: false,
                    sort: false,
                    valuePrepareFunction: function (status) {
                        if (status == 0)
                            return 'New Order';
                    },
                },
                id: {
                    title: 'Order#',
                    type: 'string',
                    filter: false,
                    sort: false,
                    sortDirection: 'desc',
                },
                customer_name: {
                    title: 'Customer',
                    type: 'custom',
                    renderComponent: _customer_customer_component__WEBPACK_IMPORTED_MODULE_12__["CustomerComponent"],
                    filter: false,
                    sort: false,
                },
                location_zipcode: {
                    title: 'Location',
                    type: 'string',
                    filter: false,
                    sort: false,
                },
                notes: {
                    title: 'Notes',
                    type: 'custom',
                    filter: false,
                    sort: false,
                    renderComponent: _shared_textarea_component__WEBPACK_IMPORTED_MODULE_13__["TextAreaComponent"],
                },
                created_at: {
                    title: 'Created At',
                    type: 'string',
                    valuePrepareFunction: function (timestamp) {
                        if (timestamp == 0)
                            return '';
                        var now = _this.pipe.transform(Date.now(), 'M/d/yy');
                        var createdAt = _this.pipe.transform(new Date(timestamp * 1000), 'M/d/yy');
                        if (now === createdAt) {
                            return 'Today, ' + _this.pipe.transform(new Date(timestamp * 1000), 'h:mm a');
                        }
                        else {
                            return _this.pipe.transform(new Date(timestamp * 1000), 'short');
                        }
                    },
                    filter: false,
                    sort: false,
                },
                view: {
                    title: 'Action',
                    type: 'custom',
                    filter: false,
                    sort: false,
                    renderComponent: _shared_buttonview_component__WEBPACK_IMPORTED_MODULE_14__["ButtonViewComponent"],
                },
            },
            rowClassFunction: function (row) {
                if (row.data.status === 1 || row.data.status === 2) {
                    return 'pending';
                }
                else if (row.data.status === 3 || row.data.status === 4) {
                    return 'ready2pickup';
                }
            }
        };
        this.source = new ng2_smart_table_lib_data_source_local_local_data_source__WEBPACK_IMPORTED_MODULE_3__["LocalDataSource"]();
        this.pastOrders = [];
        this.alive = true;
        this.showAddOrder = false;
        this.pipe = new _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_10__["DatePipe"]('en-US');
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
        this.getInfo();
    }
    PendingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.audio = new Audio();
        this.createSocket();
        this.today = Date.now();
        this.hideSoundBtn = localStorage.getItem('hideSoundBtn');
        this.showEnableAudio = true;
        // get pending orders every 10 seconds only for front desk users
        if (this.currentUser.role === 2) {
            rxjs_observable_IntervalObservable__WEBPACK_IMPORTED_MODULE_9__["IntervalObservable"].create(10000)
                .takeWhile(function () { return _this.alive; }) // only fires when component is alive
                .subscribe(function () {
                _this.getInfo();
                _this.today = Date.now();
            });
        }
    };
    PendingComponent.prototype.ngOnDestroy = function () {
        this.socket.disconnect();
        this.alive = false;
    };
    PendingComponent.prototype.createSocket = function () {
        var _this = this;
        this.socket = socket_io_client__WEBPACK_IMPORTED_MODULE_7__(_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].SERVER_URL);
        this.socket.on('message', function (data) {
            _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs('Order was updated, Refreshing data now...', data);
            _this.getInfo();
        });
        this.socket.on('onconnected', function (data) {
            _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs('Socket.io was connected, user_id = ' + data.id);
        });
        this.socket.on('disconnect', function () {
            _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs('Socket connection was disconnected');
        });
    };
    PendingComponent.prototype.getInfo = function () {
        var _this = this;
        this.apiService.getPendingOrders(this.currentUser.restaurant_id).subscribe(function (res) {
            if (!res.err) {
                _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].logs(res.response);
                var newOrders = res.response;
                _this.source.load(res.response);
                if (_this.pastOrders.length > 0) {
                    newOrders.forEach(function (order) {
                        // check if order_id exists in past orders
                        if (_this.pastOrders.filter(function (o) { return o.id === order.id; }).length === 0) {
                            // Utils.logs('new order');
                            _this.audio.load();
                            _this.audio.play();
                        }
                    });
                    _this.pastOrders = newOrders;
                }
                else {
                    _this.pastOrders = newOrders;
                }
            }
            else {
                _this.toastrService.error('Cannot fetch orders!');
            }
        });
    };
    PendingComponent.prototype.addNewOrder = function (data) {
        this.apiService.addOrder(data).subscribe(function (res) {
            // console.log(res.response);
        });
    };
    PendingComponent.prototype.enableAudio = function () {
        localStorage.setItem('hideSoundBtn', 'true');
        this.audio.src = '../../assets/sounds/WoopWoop.wav';
        this.audio.load();
        this.audio.play();
        this.showEnableAudio = false;
    };
    PendingComponent.prototype.disableAudio = function () {
        this.audio.src = '';
        this.showEnableAudio = true;
    };
    PendingComponent.prototype.addNewOrderModal = function () {
        var activeModal = this.modelService.open(_add_order_add_order_component__WEBPACK_IMPORTED_MODULE_16__["AddOrderComponent"], { size: 'lg', container: 'nb-layout' });
        activeModal.componentInstance.order = this.currentUser;
    };
    PendingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'pending',
            template: __webpack_require__(/*! ./pending.component.html */ "./src/app/front-desk/pending/pending.component.html"),
            styles: [__webpack_require__(/*! ./pending.component.scss */ "./src/app/front-desk/pending/pending.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            _ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_15__["NgbModal"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], PendingComponent);
    return PendingComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/routes/routes.component.html":
/*!*********************************************************!*\
  !*** ./src/app/front-desk/routes/routes.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\r\n    <nb-card-header>\r\n        <div class=\"flex-space-between\">\r\n        <p>Routes</p>\r\n           <mat-form-field>\r\n            <input matInput [matDatepicker]=\"picker\" (dateChange)=\"filterOrders()\" [formControl]=\"searchDate\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker></mat-datepicker>\r\n          </mat-form-field>\r\n            </div>\r\n            \r\n    </nb-card-header>\r\n    <nb-card-body>\r\n      \r\n        <mat-paginator #paginator [length]=\"totalRows$ | async\" [pageIndex]=\"0\" [pageSize]=\"10\"></mat-paginator>\r\n\r\n        <mat-accordion displayMode=\"flat\" multi class=\"mat-table\">\r\n            <section matSort class=\"mat-header-row\">\r\n                <span class=\"mat-header-cell\" mat-sort-header=\"id\">Route ID</span>\r\n                <span class=\"mat-header-cell ml-4\" mat-sort-header=\"customer\">Runner</span>\r\n            </section>\r\n\r\n            <mat-expansion-panel  *ngFor=\"let route of displayedRows$ | async\" class=\"mat-expansion-custom\">\r\n                <mat-expansion-panel-header class=\"mat-row \">\r\n                    <span class=\"mat-cell \" >{{route.batch_id}}</span>\r\n                    <span class=\"mat-cell ml-4\">{{route.runner_name}}</span>\r\n                </mat-expansion-panel-header>\r\n                <div class=\"row\">\r\n                    <div class=\"receipt-wrapper\">\r\n                        <div class=\"checkout-total\" style=\"border-bottom: 0;\">\r\n                            <div *ngFor=\"let order of orders\">\r\n                            <div class=\"d-flex d-flex justify-content-between customrDeliveryInfo\" *ngIf=\"order.batch_id==route.batch_id\">\r\n                                <p class=\"item1 custLocation\">{{order.location_apt}} {{order.location}}</p>\r\n                                <p class=\"item2 custmrName\">{{order.customer_name}}</p>\r\n                        \r\n                                <p class=\"item3\" *ngIf=\"order.status==4\">Out for delivery</p>\r\n                                <p class=\"item3\" *ngIf=\"order.status==5\">Out for delivery</p>\r\n                                <p class=\"item3\" *ngIf=\"order.status==6\">Out for delivery</p>\r\n                         \r\n                                <div *ngIf=\"order.status==8\">\r\n                                <p>Rejected</p>\r\n                                <p class=\"item4\">{{order.rejected_by_user_at *1000 |  date:'dd/MM/yyyy h:mm' }}</p>\r\n                               </div>\r\n                               <div *ngIf=\"order.status==7\">\r\n                                \r\n                                <p>Completed</p>\r\n                                <p class=\"item5\">{{order.completed_at *1000 | date: 'dd/MM/yyyy h:mm'}}</p>\r\n                                \r\n                                \r\n                             </div>\r\n                            </div>\r\n                        </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </mat-expansion-panel>\r\n        </mat-accordion>\r\n    </nb-card-body>\r\n</nb-card>"

/***/ }),

/***/ "./src/app/front-desk/routes/routes.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/front-desk/routes/routes.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between, .receipt-checkout-items {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between, .receipt-checkout-items {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n.receipt-wrapper {\n  padding: 10px;\n  border-radius: 4px;\n  width: 100%; }\n\n.receipt-checkout-items {\n  border-bottom: 1px solid #e9ecef;\n  padding: 7px 0; }\n\n.receipt-checkout-items p {\n  font-size: 14px;\n  margin: 0; }\n\n.receipt-checkout-items span {\n  margin-right: 5px; }\n\n.checkout-total {\n  font-size: 13px;\n  padding-top: 15px;\n  padding-bottom: 7px;\n  border-bottom: 1px solid grey; }\n\n.checkout-total p {\n  margin: 0; }\n\n@media (max-width: 580px) {\n    .checkout-total p {\n      max-width: 80px !important;\n      margin: 7px 0; } }\n\n.customrDeliveryInfo p.item1.custLocation {\n  width: 33%; }\n\n@media (max-width: 767px) {\n    .customrDeliveryInfo p.item1.custLocation {\n      width: unset; } }\n\n.customrDeliveryInfo p.item2.custmrName {\n  width: 23%; }\n\n@media (max-width: 767px) {\n    .customrDeliveryInfo p.item2.custmrName {\n      width: unset; } }\n\n@media (max-width: 580px) {\n  .ng-star-inserted p:last-child {\n    text-align: right;\n    max-width: 100px; } }\n"

/***/ }),

/***/ "./src/app/front-desk/routes/routes.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/front-desk/routes/routes.component.ts ***!
  \*******************************************************/
/*! exports provided: RoutesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutesComponent", function() { return RoutesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_datasource_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/datasource-utils */ "./src/app/services/datasource-utils.ts");
/* harmony import */ var rxjs_observable_of__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/observable/of */ "./node_modules/rxjs-compat/_esm5/observable/of.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var RoutesComponent = /** @class */ (function () {
    function RoutesComponent(apiService, toastrService, cookieService, router) {
        this.apiService = apiService;
        this.toastrService = toastrService;
        this.cookieService = cookieService;
        this.router = router;
        this.orders = [];
        this.maxDate = new Date();
        this.bsValue = new Date();
        this.searchDate = new _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormControl"](new Date());
    }
    RoutesComponent.prototype.ngOnInit = function () {
        this.today = Date.now();
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
        this.getRunners();
    };
    RoutesComponent.prototype.getRunners = function () {
        var _this = this;
        this.apiService.get("runners/" + this.currentUser.restaurant_id).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    _this.runners = res.response;
                    console.log("runners", _this.runners);
                    _this.routes = _this.runners.filter(function (route) { return route.batch_id != ""; });
                    console.log("routes", _this.routes);
                    var rows$ = Object(rxjs_observable_of__WEBPACK_IMPORTED_MODULE_3__["of"])(_this.routes);
                    _this.sortEvents$ = Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_2__["fromMatSort"])(_this.sort);
                    _this.pageEvents$ = Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_2__["fromMatPaginator"])(_this.paginator);
                    _this.totalRows$ = rows$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (rows) { return rows.length; }));
                    _this.displayedRows$ = rows$.pipe(Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_2__["sortRows"])(_this.sortEvents$), Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_2__["paginateRows"])(_this.pageEvents$));
                }
                else {
                    _this.toastrService.error('Cannot fetch runners!');
                }
            }
            else {
                _this.toastrService.error('Cannot fetch runners!');
            }
        });
        this.getOrders();
    };
    RoutesComponent.prototype.onSelectDate = function (value) {
        alert(value);
    };
    RoutesComponent.prototype.filterOrders = function () {
        var _this = this;
        var searchdate = this.searchDate.value;
        searchdate = searchdate.getFullYear() + '/' + ('0' + (searchdate.getMonth() + 1)).slice(-2) + '/' + ('0' + searchdate.getDate()).slice(-2);
        this.orders.forEach(function (order) {
            var date = new Date(order.created_at * 1000);
            _this.orders.created_at = date.getFullYear() + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2);
            _this.orders = _this.orders.filter(function (order) { return order.created_at == searchdate; });
        });
    };
    RoutesComponent.prototype.getOrders = function () {
        return __awaiter(this, void 0, void 0, function () {
            var i, d;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        i = 4;
                        _a.label = 1;
                    case 1:
                        if (!(i <= 8)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.apiService.get("orders/map/" + this.currentUser.restaurant_id + "/" + i).toPromise()];
                    case 2:
                        d = _a.sent();
                        this.orders = this.orders.concat(d.response);
                        _a.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], RoutesComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], RoutesComponent.prototype, "paginator", void 0);
    RoutesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'routes',
            template: __webpack_require__(/*! ./routes.component.html */ "./src/app/front-desk/routes/routes.component.html"),
            styles: [__webpack_require__(/*! ./routes.component.scss */ "./src/app/front-desk/routes/routes.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_8__["CookieService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], RoutesComponent);
    return RoutesComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/runner-dropdown/runner-dropdown.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/front-desk/runner-dropdown/runner-dropdown.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"dropdown\" [ngClass]=\"order_type\" ngbDropdown (openChange)=\"toggled($event)\">\r\n    <button class=\"btn btn-primary\" type=\"button\" ngbDropdownToggle>\r\n       {{selectedRunner_Name?selectedRunner_Name:default}}\r\n    </button>\r\n    <i class=\"nb-close\" *ngIf=\"showCross\" (click)=\"deselectRunner()\"></i>\r\n    <ul class=\"dropdown-menu\" ngbDropdownMenu >\r\n        <li class=\"dropdown-item\"  *ngFor=\"let menu of menus\" (click)=\"getId(menu.id,menu.runner_name)\">{{menu.runner_name}}</li>\r\n        <!-- <li class=\"dropdown-item\"  *ngFor=\"let menu of menus\"><span (click)=\"getId(menu.id,menu.runner_name)\">{{menu.runner_name}}</span> <span *ngIf=\"showCross\"><i class=\"nb-close\" *ngIf=\"menu.id===currentActiveRunner\" (click)=\"deselectRunner(menu.id)\"></i></span></li> -->\r\n    </ul>\r\n  </div>"

/***/ }),

/***/ "./src/app/front-desk/runner-dropdown/runner-dropdown.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/front-desk/runner-dropdown/runner-dropdown.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::-webkit-scrollbar {\n  width: 10px; }\n\n::-webkit-scrollbar-track {\n  background: #fff !important; }\n\n::-webkit-scrollbar-thumb {\n  background: #fff !important; }\n\n::-webkit-scrollbar-thumb:hover {\n  background: #555; }\n\n.dropdown {\n  width: 100%;\n  max-width: 250px;\n  right: 44px;\n  position: absolute;\n  z-index: 1;\n  margin-top: 2px; }\n\n@media (max-width: 1024px) {\n    .dropdown {\n      right: 33px; } }\n\n@media (max-width: 767px) {\n    .dropdown {\n      right: 20px; } }\n\nli.dropdown-item span i {\n  font-size: 22px;\n  cursor: pointer; }\n\nul.dropdown-menu.show li {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  font-size: 0.700rem;\n  text-transform: capitalize; }\n\nul.dropdown-menu.show li:hover {\n  background-color: #4b0082 !important;\n  color: #fff !important; }\n\n.dropdown:hover i.nb-close {\n  background: indigo;\n  color: #fff; }\n\n::ng-deep i.nb-close {\n  /* background: #000; */\n  z-index: 99;\n  color: indigo;\n  position: absolute;\n  right: 13%;\n  top: 10px;\n  font-size: 23px;\n  font-weight: bold;\n  cursor: pointer; }\n\ni.nb-close:hover {\n  background-color: #fff !important;\n  color: indigo !important;\n  border-radius: 50%; }\n\n@media (max-width: 1024px) {\n  .dropdown:hover i.nb-close {\n    color: indigo;\n    background-color: transparent; }\n  .nb-theme-default .btn.btn-primary:active i.nb-close {\n    color: green; } }\n"

/***/ }),

/***/ "./src/app/front-desk/runner-dropdown/runner-dropdown.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/front-desk/runner-dropdown/runner-dropdown.component.ts ***!
  \*************************************************************************/
/*! exports provided: RunnerDropdownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RunnerDropdownComponent", function() { return RunnerDropdownComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RunnerDropdownComponent = /** @class */ (function () {
    function RunnerDropdownComponent() {
        this.onRunnerIdSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onRunnerDeselect = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.showCross = false;
        this.default = 'All';
    }
    RunnerDropdownComponent.prototype.ngOnInit = function () {
        this.renderValue = this.value;
    };
    RunnerDropdownComponent.prototype.getId = function (id, name) {
        this.onRunnerIdSelected.emit(id);
        this.selectedRunner_Name = name;
        this.currentActiveRunner = id;
        this.showCross = true;
    };
    RunnerDropdownComponent.prototype.toggled = function (event) {
        if (event) {
            this.menus = this.dropDownMenu;
        }
    };
    RunnerDropdownComponent.prototype.ngOnChanges = function () {
        this.order_type = this.orderType;
        this.showCross = !this.deselectActiveRunner;
        this.selectedRunner_Name = null;
    };
    RunnerDropdownComponent.prototype.deselectRunner = function () {
        this.onRunnerDeselect.emit();
        this.showCross = false;
        this.selectedRunner_Name = null;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], RunnerDropdownComponent.prototype, "dropDownMenu", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], RunnerDropdownComponent.prototype, "value", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], RunnerDropdownComponent.prototype, "rowData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], RunnerDropdownComponent.prototype, "orderType", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], RunnerDropdownComponent.prototype, "deselectActiveRunner", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], RunnerDropdownComponent.prototype, "onRunnerIdSelected", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], RunnerDropdownComponent.prototype, "onRunnerDeselect", void 0);
    RunnerDropdownComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'runner-dropdown',
            template: __webpack_require__(/*! ./runner-dropdown.component.html */ "./src/app/front-desk/runner-dropdown/runner-dropdown.component.html"),
            styles: [__webpack_require__(/*! ./runner-dropdown.component.scss */ "./src/app/front-desk/runner-dropdown/runner-dropdown.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], RunnerDropdownComponent);
    return RunnerDropdownComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/runners-map/runners-map.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/front-desk/runners-map/runners-map.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-control checkBoxxs position-relative\">\r\n  <!-- <mat-checkbox formControlName =\"isStatic\">Static</mat-checkbox> -->\r\n  <mat-checkbox class=\"boxBlock  \" [checked]=\"selectedOrder==='Clusters'\" (change)=\"orderOnMap($event,'Clusters')\">\r\n    Clusters</mat-checkbox>\r\n  <mat-checkbox class=\"boxBlock\" [checked]=\"selectedOrder==='New'\" (change)=\"orderOnMap($event,'New')\">New Orders\r\n  </mat-checkbox>\r\n  <mat-checkbox class=\"boxBlock position-relative\" [checked]=\"selectedOrder==='Active'\" (change)=\"orderOnMap($event,'Active')\">Active\r\n  </mat-checkbox>\r\n  <mat-checkbox class=\"boxBlock position-relative\" [checked]=\"selectedOrder==='Delivered'\" (change)=\"orderOnMap($event,'Delivered')\">\r\n    Delivered</mat-checkbox>\r\n  <mat-checkbox class=\"boxBlock position-relative\" [checked]=\"selectedOrder==='Rejected'\" (change)=\"orderOnMap($event,'Rejected')\">\r\n    Rejected</mat-checkbox>\r\n    \r\n</div>\r\n<runner-dropdown  *ngIf=\"!showClusters && !showNew\" [deselectActiveRunner]=\"deselectedRunner\" [orderType]=\"selectedOrder\" [dropDownMenu]=\"runners\" (onRunnerIdSelected)=\"runnerOrders($event)\" (onRunnerDeselect)=\"deselectRunner()\"></runner-dropdown>\r\n<agm-map *ngIf=\"showMap && showClusters\" [latitude]=\"restaurant.latitude\" [longitude]=\"restaurant.longitude\" [zoom]=\"12\"\r\n  [zoomControl]=\"true\" [streetViewControl]=\"false\" [minZoom]=\"3\">\r\n  <div *ngIf=\"orders && orders.length>0\">\r\n    <agm-marker *ngFor=\"let batch of orders\" [latitude]=\"batch.latitude\" [longitude]=\"batch.longitude\"\r\n      [iconUrl]=\"batch.icon\" [label]=\"batch.labelOptions\">\r\n      <agm-info-window>\r\n        <div>\r\n          <p style=\"color:pink;font-weight: 600;\">{{batch.customer_name}}</p>\r\n          <p style=\"color: pink; font-weight: 600;\">{{batch.location}}</p>\r\n        </div>\r\n\r\n      </agm-info-window>\r\n    </agm-marker>\r\n  </div>\r\n\r\n</agm-map>\r\n\r\n<agm-map *ngIf=\"showMap && showOrders\" [latitude]=\"restaurant.latitude\" [longitude]=\"restaurant.longitude\" [zoom]=\"12\"\r\n  [zoomControl]=\"true\" [streetViewControl]=\"false\" [minZoom]=\"3\">\r\n  <div *ngIf=\"orders && orders.length>0\">\r\n    <agm-marker *ngFor=\"let batch of orders\" [latitude]=\"batch.order_latitude\" [longitude]=\"batch.order_longitude\"\r\n      [iconUrl]=\"batch.icon\">\r\n      <agm-info-window>\r\n        <div>\r\n          <p style=\"color:pink;font-weight: 600;\">{{batch.customer_name}}</p>\r\n          <p style=\"color: pink;font-weight: 600;\">{{batch.location}}</p>\r\n        </div>\r\n\r\n      </agm-info-window>\r\n    </agm-marker>\r\n  </div>\r\n\r\n</agm-map>"

/***/ }),

/***/ "./src/app/front-desk/runners-map/runners-map.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/front-desk/runners-map/runners-map.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\nagm-map {\n  height: 85vh; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around; }\n\n.checkBoxxs {\n  display: flex;\n  justify-content: space-around; }\n\n@media (max-width: 767px) {\n    .checkBoxxs {\n      flex-direction: column;\n      justify-content: start; } }\n\n.mat-checkbox-layout .mat-checkbox-label {\n  line-height: 24px;\n  z-index: 99 !important; }\n\n::ng-deep .mat-checkbox-checked.mat-accent .mat-checkbox-background, .mat-checkbox-indeterminate.mat-accent .mat-checkbox-background {\n  background-color: #2ab2ec !important; }\n\n@media (max-width: 768px) {\n  .form-control .flex-space-around {\n    width: -webkit-fill-available !important; } }\n"

/***/ }),

/***/ "./src/app/front-desk/runners-map/runners-map.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/front-desk/runners-map/runners-map.component.ts ***!
  \*****************************************************************/
/*! exports provided: RunnersMapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RunnersMapComponent", function() { return RunnersMapComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RunnersMapComponent = /** @class */ (function () {
    function RunnersMapComponent(apiService, cookieService, toastrService, router) {
        this.apiService = apiService;
        this.cookieService = cookieService;
        this.toastrService = toastrService;
        this.router = router;
        this.showMap = false;
        this.showOrders = false;
        this.showClusters = false;
        this.showDelivered = false;
        this.showActive = false;
        this.showRejected = false;
        this.showNew = false;
        this.ShowNoData = false;
        this.showRunnerOrders = false;
        this.markerColors = ['purple', 'blue', 'green', 'red', 'orange', 'yellow'];
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_4__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
        this.selectedOrder = 'Clusters';
        this.showClusters = true;
    }
    RunnersMapComponent.prototype.ngOnInit = function () {
        this.getRunners();
        this.populateMap();
    };
    RunnersMapComponent.prototype.getRunners = function () {
        var _this = this;
        this.apiService.get("runners/" + this.currentUser.restaurant_id).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    _this.runners = res.response;
                }
                else {
                    _this.toastrService.error('Cannot fetch runners!');
                }
            }
            else {
                _this.toastrService.error('Cannot fetch runners!');
            }
        });
    };
    RunnersMapComponent.prototype.runnerOrders = function (runner_id) {
        this.runnerBatches = this.orders.filter(function (order) { return order.runner_id == runner_id; });
        this.showRunnerOrders = true;
        this.populateMap();
    };
    RunnersMapComponent.prototype.populateMap = function () {
        var _this = this;
        this.apiService.get("restaurant/get/" + this.currentUser.restaurant_id)
            .subscribe(function (res) {
            if (!res.err && res.response) {
                _this.restaurant = res.response[0];
                _this.showMap = true;
            }
        });
        if (this.showClusters) {
            this.showOrders = false;
            this.apiService.get("order/batchs/" + this.currentUser.restaurant_id)
                .subscribe(function (res) {
                if (!res.err && res.response) {
                    _this.orders = res.response;
                    if (_this.ShowNoData) {
                        _this.orders = [];
                        _this.ShowNoData = false;
                    }
                    var size_1 = 20;
                    _this.orders.forEach(function (batch) {
                        _this.markerIndex = _this.getRandomInt(_this.markerColors.length - 1);
                        batch.icon = {
                            url: "https://paranoid-cdn.s3-us-west-1.amazonaws.com/uploads/" + _this.markerColors[_this.markerIndex] + "-dot.png",
                            scaledSize: {
                                height: size_1,
                                width: size_1
                            }
                        };
                        batch.labelOptions = {
                            text: "" + batch.order_per_batch,
                            color: 'white', fontSize: '12px', fontWeight: 'bold'
                        };
                    });
                }
            });
        }
        else {
            this.showOrders = true;
            this.apiService.get("orders/map/" + this.currentUser.restaurant_id + "/" + this.status)
                .subscribe(function (res) {
                if (!res.err && res.response) {
                    if (!_this.showRunnerOrders) {
                        _this.orders = res.response;
                    }
                    else {
                        _this.orders = _this.runnerBatches;
                    }
                    var size_2 = 20;
                    _this.orders.forEach(function (batch) {
                        //this.markerIndex = this.getRandomInt(this.markerColors.length-1)
                        batch.icon = {
                            url: "https://paranoid-cdn.s3-us-west-1.amazonaws.com/uploads/" + _this.markerColors[_this.markerIndex] + "-dot.png",
                            scaledSize: {
                                height: size_2,
                                width: size_2
                            }
                        };
                        batch.labelOptions = {
                            text: "" + batch.order_per_batch,
                            color: 'white', fontSize: '12px', fontWeight: 'bold'
                        };
                    });
                    _this.showRunnerOrders = false;
                }
            });
        }
    };
    RunnersMapComponent.prototype.getRandomInt = function (max) {
        return Math.floor(Math.random() * Math.floor(max));
    };
    RunnersMapComponent.prototype.orderOnMap = function (event, orderType) {
        this.deselectedRunner = true;
        this.selectedOrder = orderType;
        if (orderType === 'Clusters' && event.checked == true) {
            this.showClusters = true;
            this.showNew = false;
            this.showActive = false;
            this.showDelivered = false;
            this.showRejected = false;
        }
        else if (orderType === 'New' && event.checked == true) {
            this.status = 0;
            this.markerIndex = 0;
            this.showNew = true;
            this.showClusters = false;
            this.showActive = false;
            this.showDelivered = false;
            this.showRejected = false;
        }
        else if (orderType === 'Active' && event.checked == true) {
            this.markerIndex = 1;
            this.status = 1;
            this.showActive = true;
            this.showClusters = false;
            this.showNew = false;
            this.showDelivered = false;
            this.showRejected = false;
        }
        else if (orderType === 'Delivered' && event.checked == true) {
            this.markerIndex = 2;
            this.status = 7;
            this.showDelivered = true;
            this.showClusters = false;
            this.showNew = false;
            this.showActive = false;
            this.showRejected = false;
        }
        else if (orderType === 'Rejected' && event.checked == true) {
            this.markerIndex = 3;
            this.status = 8;
            this.showRejected = true;
            this.showClusters = false;
            this.showNew = false;
            this.showActive = false;
            this.showDelivered = false;
        }
        else if (orderType === 'Clusters' && event.checked == false) {
            this.ShowNoData = true;
        }
        else {
            this.showClusters = true;
            this.showNew = false;
            this.showActive = false;
            this.showDelivered = false;
            this.showRejected = false;
            this.selectedOrder = 'Clusters';
        }
        this.populateMap();
    };
    RunnersMapComponent.prototype.deselectRunner = function () {
        this.showRunnerOrders = false;
        this.populateMap();
    };
    RunnersMapComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'runners-map',
            template: __webpack_require__(/*! ./runners-map.component.html */ "./src/app/front-desk/runners-map/runners-map.component.html"),
            styles: [__webpack_require__(/*! ./runners-map.component.scss */ "./src/app/front-desk/runners-map/runners-map.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__["CookieService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], RunnersMapComponent);
    return RunnersMapComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/shared/buttonview.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/front-desk/shared/buttonview.component.ts ***!
  \***********************************************************/
/*! exports provided: ButtonViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonViewComponent", function() { return ButtonViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../order-detail/order-detail.component */ "./src/app/front-desk/order-detail/order-detail.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ButtonViewComponent = /** @class */ (function () {
    function ButtonViewComponent(modalService) {
        this.modalService = modalService;
    }
    ButtonViewComponent.prototype.onClick = function () {
        var activeModal = this.modalService.open(_order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_1__["OrderDetailComponent"], { size: 'lg', container: 'nb-layout' });
        activeModal.componentInstance.order = this.rowData;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ButtonViewComponent.prototype, "value", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ButtonViewComponent.prototype, "rowData", void 0);
    ButtonViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'button-view',
            template: "\n    <div class=\"flex-center\"><button [class]=\"'btn-custom'\" (click)=\"onClick()\"><i class=\"nb-menu\"></i></button></div>\n    ",
            styles: [__webpack_require__(/*! ./shared.component.scss */ "./src/app/front-desk/shared/shared.component.scss")],
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_2__["NgbModal"]])
    ], ButtonViewComponent);
    return ButtonViewComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/shared/shared.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/front-desk/shared/shared.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\ntextarea {\n  border-radius: 4px;\n  width: 100%;\n  opacity: 0.7;\n  color: #280f54;\n  padding: 10px; }\n\n.btn-custom {\n  padding: 0 4px !important;\n  font-size: 2rem !important;\n  background-color: white;\n  color: #280f54;\n  border-radius: 4px; }\n"

/***/ }),

/***/ "./src/app/front-desk/shared/textarea.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/front-desk/shared/textarea.component.ts ***!
  \*********************************************************/
/*! exports provided: TextAreaComponent, NotesModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextAreaComponent", function() { return TextAreaComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotesModalComponent", function() { return NotesModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TextAreaComponent = /** @class */ (function () {
    function TextAreaComponent(modalService) {
        this.modalService = modalService;
    }
    TextAreaComponent.prototype.openModal = function () {
        var activeModal = this.modalService.open(NotesModalComponent, { size: 'lg', container: 'nb-layout' });
        activeModal.componentInstance.order = this.rowData;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], TextAreaComponent.prototype, "value", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], TextAreaComponent.prototype, "rowData", void 0);
    TextAreaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'textarea-view',
            template: "\n    <p class=\"flex-start\"><i class=\"nb-edit\" (click)=\"openModal()\" style=\"font-size: 20px; cursor: pointer;padding-right: 2px;\"></i>\n        <span>{{rowData.notes}}</span></p>\n    ",
            styles: [__webpack_require__(/*! ./shared.component.scss */ "./src/app/front-desk/shared/shared.component.scss")],
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"]])
    ], TextAreaComponent);
    return TextAreaComponent;
}());

var NotesModalComponent = /** @class */ (function () {
    function NotesModalComponent(apiService, activeModal, toastrService) {
        this.apiService = apiService;
        this.activeModal = activeModal;
        this.toastrService = toastrService;
    }
    NotesModalComponent.prototype.ngOnInit = function () {
        this.pOrder = __assign({}, this.order);
    };
    NotesModalComponent.prototype.onSave = function () {
        var _this = this;
        this.closeModal();
        this.apiService.post('order/notes', this.pOrder)
            .subscribe(function (res) {
            _common__WEBPACK_IMPORTED_MODULE_4__["Utils"].logs(res);
            if (!res.err) {
                _this.toastrService.info('Note was updated');
            }
            else {
                _this.toastrService.error('Note was not updated!');
            }
        });
    };
    NotesModalComponent.prototype.closeModal = function () {
        this.activeModal.close();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NotesModalComponent.prototype, "order", void 0);
    NotesModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'textarea-view',
            template: "\n    <div class=\"modal-header\" style=\"padding: 10px 16px;\">\n        <h5 class=\"modal-title\">Notes</h5>\n        <i class=\"nb-close close-modal\" (click)=\"closeModal()\"></i>\n    </div>\n    <div class=\"modal-body\" *ngIf=\"pOrder\">\n        <textarea [(ngModel)]=\"pOrder.notes\" rows=\"10\"></textarea>\n    </div>\n    <div class=\"modal-footer flex-space-between\" style=\"padding: 10px 16px;\">\n    <button mat-raised-button (click)=\"closeModal()\">Cancel</button>\n        <button mat-raised-button color=\"primary\" (click)=\"onSave()\">Save</button>\n    </div>\n    ",
            styles: [__webpack_require__(/*! ./shared.component.scss */ "./src/app/front-desk/shared/shared.component.scss")],
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbActiveModal"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]])
    ], NotesModalComponent);
    return NotesModalComponent;
}());



/***/ }),

/***/ "./src/app/front-desk/users/users.component.html":
/*!*******************************************************!*\
  !*** ./src/app/front-desk/users/users.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\r\n    <nb-card-header>\r\n      Users\r\n    </nb-card-header>\r\n  \r\n    <nb-card-body>\r\n      <ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (editConfirm)=\"onSaveConfirm($event)\" (createConfirm)=\"onCreateConfirm($event)\"></ng2-smart-table>\r\n    </nb-card-body>\r\n  </nb-card>"

/***/ }),

/***/ "./src/app/front-desk/users/users.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/front-desk/users/users.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n::ng-deep .nb-plus {\n  color: #280f54;\n  font-weight: bold; }\n"

/***/ }),

/***/ "./src/app/front-desk/users/users.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/front-desk/users/users.component.ts ***!
  \*****************************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UsersComponent = /** @class */ (function () {
    function UsersComponent(apiService, toastrService, cookieService, router) {
        this.apiService = apiService;
        this.toastrService = toastrService;
        this.cookieService = cookieService;
        this.router = router;
        this.settings = {
            mode: 'inline',
            add: {
                addButtonContent: '<i class="nb-plus"></i>',
                createButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmCreate: true,
            },
            edit: {
                editButtonContent: '<i class="nb-edit"></i>',
                saveButtonContent: '<i class="nb-checkmark"></i>',
                cancelButtonContent: '<i class="nb-close"></i>',
                confirmSave: true,
            },
            delete: {
                deleteButtonContent: '<i class="nb-trash"></i>',
                confirmDelete: true,
            },
            columns: {
                runner_name: {
                    title: 'Name',
                    type: 'string',
                    filter: false,
                },
                phone_number: {
                    title: 'Phone #',
                    type: 'string',
                    filter: false,
                },
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
    }
    UsersComponent.prototype.ngOnInit = function () {
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
        this.getRunners();
    };
    UsersComponent.prototype.getRunners = function () {
        var _this = this;
        this.apiService.get("runners/" + this.currentUser.restaurant_id).subscribe(function (res) {
            if (res) {
                if (!res.err) {
                    console.log("users", res.response);
                    _this.source.load(res.response);
                }
                else {
                    _this.toastrService.error('Cannot fetch runners!');
                }
            }
            else {
                _this.toastrService.error('Cannot fetch runners!');
            }
        });
    };
    UsersComponent.prototype.onDeleteConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to delete this runner???')) {
            this.apiService.get("runner/delete/" + event.data['id']).subscribe(function (res) {
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Successfully deleted!');
                        event.confirm.resolve();
                    }
                    else {
                        _this.toastrService.error(res.msg);
                        event.confirm.reject();
                    }
                }
                else {
                    _this.toastrService.error('"Delete runner" failed!');
                    event.confirm.reject();
                }
            });
        }
        else {
            event.confirm.reject();
        }
    };
    UsersComponent.prototype.onSaveConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to save changes?')) {
            var name_1 = event.newData['runner_name'];
            if (name_1 === '') {
                this.toastrService.error('You must enter a name!');
                event.confirm.reject();
                return;
            }
            var phone = event.newData['phone_number'];
            if (phone === '') {
                this.toastrService.error('You must enter a phone #!');
                event.confirm.reject();
                return;
            }
            this.apiService.post("runner/update", __assign({}, event.newData, { id: event.data['id'] })).subscribe(function (res) {
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Successfully updated!');
                        event.confirm.resolve();
                    }
                    else {
                        _this.toastrService.error(res.msg);
                        event.confirm.reject();
                    }
                }
                else {
                    _this.toastrService.error('Update failed!');
                    event.confirm.reject();
                }
            });
        }
        else {
            event.confirm.reject();
        }
    };
    UsersComponent.prototype.onCreateConfirm = function (event) {
        var _this = this;
        if (window.confirm('Are you sure you want to add this runner?')) {
            var name_2 = event.newData['runner_name'];
            if (name_2 === '') {
                this.toastrService.error('You must enter a name!');
                event.confirm.reject();
                return;
            }
            var phone = event.newData['phone_number'];
            if (phone === '') {
                this.toastrService.error('You must enter a phone #!');
                event.confirm.reject();
                return;
            }
            var runner = event.newData;
            runner.restaurant_id = this.currentUser.restaurant_id;
            this.apiService.post("runner/add", runner).subscribe(function (res) {
                if (res) {
                    if (!res.err) {
                        _this.toastrService.info('Successfully added!');
                        _this.getRunners();
                        event.confirm.resolve();
                    }
                    else {
                        _this.toastrService.error(res.msg);
                        event.confirm.reject();
                    }
                }
                else {
                    _this.toastrService.error('Registration failed!');
                    event.confirm.reject();
                }
            });
        }
        else {
            event.confirm.reject();
        }
    };
    UsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! ./users.component.html */ "./src/app/front-desk/users/users.component.html"),
            styles: [__webpack_require__(/*! ./users.component.scss */ "./src/app/front-desk/users/users.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ }),

/***/ "./src/app/services/ordering.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/ordering.service.ts ***!
  \**********************************************/
/*! exports provided: OrderingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderingService", function() { return OrderingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/internal/operators */ "./node_modules/rxjs/internal/operators/index.js");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OrderingService = /** @class */ (function () {
    function OrderingService(http) {
        this.http = http;
        this.BASE_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].SERVER_URL + '/api/';
    }
    OrderingService.prototype.getHeaders = function () {
        return {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authkey': 'T1rS54ZW-pHtV-2L2idP'
            })
        };
    };
    OrderingService.prototype.get = function (path) {
        return this.http
            .get("" + this.BASE_URL + path, this.getHeaders())
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (res) { return console.log("request is ok"); }), Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError(path)));
    };
    OrderingService.prototype.post = function (path, body) {
        if (body === void 0) { body = {}; }
        debugger;
        return this.http.post("" + this.BASE_URL + path, body, this.getHeaders())
            .pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (res) { return console.log("request is ok"); }), Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError(path)));
    };
    // Error handling
    OrderingService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            console.error(error);
            console.log(operation + " failed: " + error.message);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(result);
        };
    };
    OrderingService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], OrderingService);
    return OrderingService;
}());



/***/ })

}]);
//# sourceMappingURL=app-front-desk-front-desk-module.js.map