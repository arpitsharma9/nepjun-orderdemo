(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-orders-orders-module"],{

/***/ "./src/app/orders/orders.component.html":
/*!**********************************************!*\
  !*** ./src/app/orders/orders.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row select-client\" *ngIf=\"isValid\">\r\n    <div class=\"col-12\" *ngIf=\"isClient; else isNotClient\">\r\n        <div class=\"row select-client\">\r\n            <div class=\"col-6 col-lg-3\">\r\n                <mat-select [(ngModel)]=\"selectedRestaurantIndex\" (ngModelChange)=\"onSelectRestaurant($event)\">\r\n                    <mat-option [value]=\"i\" *ngFor=\"let restaurant of restaurants; let i = index\">{{restaurant.name}}</mat-option>\r\n                </mat-select>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- <ng-template #isNotClient>\r\n      <div class=\"col-md-12 col-lg-12\">\r\n        <span id=\"menu\">\r\n          <span *ngIf=\"selectedRestaurant !== null\">{{ selectedRestaurant.name }}</span>\r\n          <span *ngIf=\"myMenu !== null\"> / </span>\r\n          <span *ngIf=\"myMenu !== null\">Menu</span> \r\n        </span>\r\n      </div>\r\n    </ng-template>\r\n    <br><br> -->\r\n</div>\r\n\r\n<nb-card>\r\n    <nb-card-header class=\"flex-space-between\">\r\n        <div>\r\n            Orders <span class=\"date-span\">(Last Updated on {{today | date:'EE h:mm a'}})</span>\r\n        </div>\r\n        <img src=\"../../assets/images/volume_off.png\" alt=\"volume_off\" id=\"btn-enable-audio\" (click)=\"enableAudio()\" *ngIf=\"showEnableAudio\">\r\n        <img src=\"../../assets/images/volume_up.png\" alt=\"volume_up\" (click)=\"disableAudio()\" *ngIf=\"!showEnableAudio\">\r\n    </nb-card-header>\r\n\r\n    <nb-card-body>\r\n        <mat-paginator #paginator [length]=\"totalRows$ | async\" [pageIndex]=\"0\" [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\r\n\r\n        <mat-accordion displayMode=\"flat\" multi class=\"mat-table\">\r\n            <section matSort class=\"mat-header-row\">\r\n                <span class=\"mat-header-cell\" mat-sort-header=\"status\">Status</span>\r\n                <span class=\"mat-header-cell\" mat-sort-header=\"id\">Order #</span>\r\n                <span class=\"mat-header-cell\" mat-sort-header=\"customer\">Customer</span>\r\n                <span class=\"mat-header-cell\" mat-sort-header=\"paid\">Order Total</span>\r\n                <!-- <span class=\"mat-header-cell\" mat-sort-header=\"refund\">Refund</span> -->\r\n            </section>\r\n\r\n            <mat-expansion-panel *ngFor=\"let order of displayedRows$ | async\" class=\"mat-expansion-custom\">\r\n                <mat-expansion-panel-header class=\"mat-row\">\r\n                    <span class=\"mat-cell\">\r\n                <span>{{getOrderStatus(order)}}<br></span>\r\n                    <span style=\"font-size: 0.7rem;\">{{ order.time }}</span>\r\n                    </span>\r\n                    <span class=\"mat-cell\">{{order.id}}</span>\r\n                    <span class=\"mat-cell\">{{order.customer_name}}</span>\r\n                    <span class=\"mat-cell\">{{order.currency_symbol}}{{order.paid}}</span>\r\n                    <!-- <span class=\"mat-cell\"><button-refund *ngIf=\"order.status==5\" [order_id]=\"order.id\"></button-refund></span> -->\r\n                </mat-expansion-panel-header>\r\n                <div class=\"row\">\r\n                    <div class=\"col-lg-6 col-md-12\">\r\n                        <table *ngFor=\"let item of order.items\" style=\"width: 100%;\">\r\n                            <thead>\r\n                                <tr class=\"row\">\r\n                                    <th class=\"col-1\">{{ item.amounts }}</th>\r\n                                    <th class=\"col-7\">{{ item.name }}</th>\r\n                                    <th class=\"col-4\">{{order.currency_symbol}}{{ item.price }}</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                <tr class=\"row\">\r\n                                    <td class=\"col-1\"></td>\r\n                                    <td class=\"col-7\">{{ item.description }}</td>\r\n                                    <td class=\"col-4\"></td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </mat-expansion-panel>\r\n        </mat-accordion>\r\n    </nb-card-body>\r\n</nb-card>"

/***/ }),

/***/ "./src/app/orders/orders.component.scss":
/*!**********************************************!*\
  !*** ./src/app/orders/orders.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n"

/***/ }),

/***/ "./src/app/orders/orders.component.ts":
/*!********************************************!*\
  !*** ./src/app/orders/orders.component.ts ***!
  \********************************************/
/*! exports provided: OrdersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersComponent", function() { return OrdersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_observable_of__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/observable/of */ "./node_modules/rxjs-compat/_esm5/observable/of.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_datasource_utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/datasource-utils */ "./src/app/services/datasource-utils.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../common */ "./src/app/common.ts");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_observable_IntervalObservable__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs/observable/IntervalObservable */ "./node_modules/rxjs-compat/_esm5/observable/IntervalObservable.js");
/* harmony import */ var rxjs_add_operator_takeWhile__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! rxjs/add/operator/takeWhile */ "./node_modules/rxjs-compat/_esm5/add/operator/takeWhile.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var OrdersComponent = /** @class */ (function () {
    function OrdersComponent(apiService, cookieService, toastrService, router) {
        this.apiService = apiService;
        this.cookieService = cookieService;
        this.toastrService = toastrService;
        this.router = router;
        this.isClient = false;
        this.isValid = false;
        this.alive = false;
        this.isDataFromCookies = false;
        this.pastOrders = [];
        this.time = 0;
    }
    OrdersComponent.prototype.ngOnInit = function () {
        this.audio = new Audio();
        this.today = Date.now();
        this.hideSoundBtn = localStorage.getItem('hideSoundBtn');
        this.showEnableAudio = true;
        if (!(this.cookieService.check('user') && this.cookieService.get('user') != '')) {
            var redirect = '/auth/login';
            this.router.navigateByUrl(redirect);
            return;
        }
        else {
            this.currentUser = _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].decodeJwt(this.cookieService.get('user'));
        }
        this.fetcRestaurants();
        this.createSocket();
    };
    OrdersComponent.prototype.ngOnDestroy = function () {
        this.socket.disconnect();
        this.alive = false;
    };
    OrdersComponent.prototype.createSocket = function () {
        var _this = this;
        this.socket = socket_io_client__WEBPACK_IMPORTED_MODULE_9__(_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].SERVER_URL);
        this.socket.on('message', function (data) {
            _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].logs('Order was updated, Refreshing data now...', data);
            _this.getInfo();
        });
        this.socket.on('onconnected', function (data) {
            _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].logs('Socket.io was connected, user_id = ' + data.id);
        });
        this.socket.on('disconnect', function () {
            _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].logs('Socket connection was disconnected');
        });
    };
    OrdersComponent.prototype.onSelectRestaurant = function (index) {
        _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].logs('onSelectRestaurant ' + index);
        this.selectedRestaurant = this.restaurants[index];
        this.cookieService.set('selected-restaurant-' + this.currentUser.client_id, _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].encodeJwt(this.selectedRestaurant), null, '/');
        this.cookieService.set('selected-restaurant-index-' + this.currentUser.client_id, _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].encodeJwt(index), null, '/');
        this.getInfo();
    };
    OrdersComponent.prototype.fetcRestaurants = function () {
        var _this = this;
        if (this.currentUser.role === 0) {
            // is a client
            this.isClient = true;
            if (this.cookieService.check('restaurants-' + this.currentUser.client_id) && this.cookieService.get('restaurants-' + this.currentUser.client_id) != ''
                && this.cookieService.check('selected-restaurant-index-' + this.currentUser.client_id) && this.cookieService.get('selected-restaurant-index-' + this.currentUser.client_id) != ''
                && this.cookieService.check('selected-restaurant-' + this.currentUser.client_id) && this.cookieService.get('selected-restaurant-' + this.currentUser.client_id) != '') {
                this.restaurants = _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].decodeJwt(this.cookieService.get('restaurants-' + this.currentUser.client_id));
                this.selectedRestaurant = _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].decodeJwt(this.cookieService.get('selected-restaurant-' + this.currentUser.client_id));
                this.selectedRestaurantIndex = _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].decodeJwt(this.cookieService.get('selected-restaurant-index-' + this.currentUser.client_id));
                this.isDataFromCookies = true;
                this.isValid = true;
            }
            this.apiService.getRestaurantsByClientId(this.currentUser.client_id).subscribe(function (res) {
                // console.log(res);
                if (res) {
                    if (!res.err) {
                        if (res.response && res.response.length > 0) {
                            _this.restaurants = res.response;
                            _this.cookieService.set('restaurants-' + _this.currentUser.client_id, _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].encodeJwt(_this.restaurants), null, '/');
                            if (_this.isDataFromCookies) {
                                // update restaurant data
                                _this.selectedRestaurant = _this.restaurants[_this.selectedRestaurantIndex];
                            }
                            else {
                                _this.selectedRestaurant = _this.restaurants[0];
                                _this.selectedRestaurantIndex = 0;
                                _this.cookieService.set('selected-restaurant-index-' + _this.currentUser.client_id, _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].encodeJwt(_this.selectedRestaurantIndex), null, '/');
                            }
                            _this.cookieService.set('selected-restaurant-' + _this.currentUser.client_id, _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].encodeJwt(_this.selectedRestaurant), null, '/');
                            // console.log(this.restaurants);
                            _this.getInfo();
                            // get sold orders every 20 seconds only for client/manager users
                            _this.alive = true;
                            if (_this.currentUser.role === 0 || _this.currentUser.role === 1) {
                                rxjs_observable_IntervalObservable__WEBPACK_IMPORTED_MODULE_12__["IntervalObservable"].create(20000)
                                    .takeWhile(function () { return _this.alive; }) // only fires when component is alive
                                    .subscribe(function () {
                                    _this.getInfo();
                                });
                            }
                            _this.isValid = true;
                        }
                        else {
                            _this.toastrService.error('You must create at least one concession.');
                        }
                    }
                    else {
                        _this.toastrService.error('Cannot fetch restaurants!');
                    }
                }
                else {
                    _this.toastrService.error('Cannot fetch restaurants!');
                }
            });
        }
        else {
            this.selectedRestaurant = this.currentUser.restaurant;
            this.getInfo();
            // get sold orders every 20 seconds only for client/manager users
            this.alive = true;
            if (this.currentUser.role === 0 || this.currentUser.role === 1) {
                rxjs_observable_IntervalObservable__WEBPACK_IMPORTED_MODULE_12__["IntervalObservable"].create(20000)
                    .takeWhile(function () { return _this.alive; }) // only fires when component is alive
                    .subscribe(function () {
                    _this.getInfo();
                });
            }
            this.isValid = true;
        }
    };
    OrdersComponent.prototype.getInfo = function () {
        var _this = this;
        this.apiService.getPendingOrders(this.selectedRestaurant.id).subscribe(function (res) {
            // console.log(res);
            if (res) {
                if (!res.err) {
                    _this.today = Date.now();
                    _this.data = [];
                    var newOrders = res.response;
                    for (var _i = 0, _a = res.response; _i < _a.length; _i++) {
                        var order = _a[_i];
                        var price = 0;
                        for (var _b = 0, _c = order.items; _b < _c.length; _b++) {
                            var item = _c[_b];
                            price += (item.price * (1 + item.tax));
                        }
                        _this.getOrderStatus(order);
                        _this.data.push({
                            id: order.id,
                            customer_id: order.customer_id,
                            customer_name: order.customer_name,
                            customer_token: order.customer_token,
                            customer_location: order.customer_location,
                            currency_symbol: order.currency_symbol,
                            transaction_id: order.transaction_id,
                            status: order.status,
                            paid: order.order_total.toFixed(2),
                            created_at: order.created_at,
                            ready_at: order.ready_at,
                            items: order.items,
                            time: _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].getDate(_this.time),
                        });
                    }
                    // this.source.load(data);
                    if (_this.pastOrders.length > 0) {
                        newOrders.forEach(function (order) {
                            // check if order_id exists in past orders
                            if (_this.pastOrders.filter(function (o) { return o.id === order.id; }).length === 0) {
                                _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].logs('new order');
                                _this.audio.load();
                                _this.audio.play();
                            }
                        });
                        _this.pastOrders = newOrders;
                    }
                    else {
                        _this.pastOrders = newOrders;
                    }
                    // get completed orders
                    _this.apiService.getCompletedOrders(_this.selectedRestaurant.id).subscribe(function (res) {
                        // console.log(res);
                        if (res) {
                            if (!res.err) {
                                for (var _i = 0, _a = res.response; _i < _a.length; _i++) {
                                    var order = _a[_i];
                                    var price = 0;
                                    for (var _b = 0, _c = order.items; _b < _c.length; _b++) {
                                        var item = _c[_b];
                                        price += (item.price * (1 + item.tax));
                                    }
                                    _this.getOrderStatus(order);
                                    _this.data.push({
                                        id: order.id,
                                        status: order.status,
                                        customer_id: order.customer_id,
                                        customer_name: order.customer_name,
                                        customer_token: order.customer_token,
                                        transaction_id: order.transaction_id,
                                        paid: order.order_total.toFixed(2),
                                        currency_symbol: order.currency_symbol,
                                        created_at: order.created_at,
                                        ready_at: order.ready_at,
                                        items: order.items,
                                        time: _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].getDate(_this.time),
                                    });
                                }
                                // console.log(this.data)
                                var rows$ = Object(rxjs_observable_of__WEBPACK_IMPORTED_MODULE_2__["of"])(_this.data);
                                _this.sortEvents$ = Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_5__["fromMatSort"])(_this.sort);
                                _this.pageEvents$ = Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_5__["fromMatPaginator"])(_this.paginator);
                                _this.totalRows$ = rows$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (rows) { return rows.length; }));
                                _this.displayedRows$ = rows$.pipe(Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_5__["sortRows"])(_this.sortEvents$), Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_5__["paginateRows"])(_this.pageEvents$));
                            }
                            else {
                                _this.toastrService.error('Cannot fetch completed orders!');
                            }
                        }
                        else {
                            _this.toastrService.error('Cannot fetch completed orders!');
                        }
                    });
                }
                else {
                    _this.toastrService.error('Cannot fetch pending orders!');
                }
            }
            else {
                _this.toastrService.error('Cannot fetch pending orders!');
            }
        });
    };
    OrdersComponent.prototype.getOrderStatus = function (order) {
        var orderStatus = '';
        switch (order.status) {
            case 0:
                orderStatus = 'Processing Order';
                this.time = order.created_at;
                break;
            case 1:
                orderStatus = 'New Pending Order (Delivery)';
                this.time = order.created_at;
                break;
            case 2:
                orderStatus = 'New Pending Order (Pickup)';
                this.time = order.created_at;
                break;
            case 3:
                orderStatus = 'Ready To Pickup';
                this.time = order.ready_at;
                break;
            case 4:
                orderStatus = 'Ready To Delivery';
                this.time = order.ready_at;
                break;
            case 5:
                orderStatus = 'Completed';
                this.time = order.completed_at;
                break;
            case 6:
                orderStatus = 'Refunded';
                this.time = order.refunded_at;
                break;
            case 7:
                orderStatus = 'Cancelled';
                this.time = order.cancelled_at;
                break;
        }
        return orderStatus;
    };
    OrdersComponent.prototype.enableAudio = function () {
        localStorage.setItem('hideSoundBtn', 'true');
        this.audio.src = "../../assets/sounds/WoopWoop.wav";
        this.audio.load();
        this.audio.play();
        this.showEnableAudio = false;
    };
    OrdersComponent.prototype.disableAudio = function () {
        this.audio.src = "";
        this.showEnableAudio = true;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], OrdersComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], OrdersComponent.prototype, "paginator", void 0);
    OrdersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'orders',
            template: __webpack_require__(/*! ./orders.component.html */ "./src/app/orders/orders.component.html"),
            styles: [__webpack_require__(/*! ./orders.component.scss */ "./src/app/orders/orders.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_10__["ApiService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_6__["CookieService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], OrdersComponent);
    return OrdersComponent;
}());



/***/ }),

/***/ "./src/app/orders/orders.module.ts":
/*!*****************************************!*\
  !*** ./src/app/orders/orders.module.ts ***!
  \*****************************************/
/*! exports provided: OrdersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersModule", function() { return OrdersModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _orders_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orders.routing */ "./src/app/orders/orders.routing.ts");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _orders_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./orders.component */ "./src/app/orders/orders.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var OrdersModule = /** @class */ (function () {
    function OrdersModule() {
    }
    OrdersModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(_orders_routing__WEBPACK_IMPORTED_MODULE_2__["OrdersRoutingModule"]),
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_3__["ThemeModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__["Ng2SmartTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"],
            ],
            declarations: [
                _orders_component__WEBPACK_IMPORTED_MODULE_7__["OrdersComponent"],
            ],
            entryComponents: []
        })
    ], OrdersModule);
    return OrdersModule;
}());



/***/ }),

/***/ "./src/app/orders/orders.routing.ts":
/*!******************************************!*\
  !*** ./src/app/orders/orders.routing.ts ***!
  \******************************************/
/*! exports provided: OrdersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersRoutingModule", function() { return OrdersRoutingModule; });
/* harmony import */ var _orders_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./orders.component */ "./src/app/orders/orders.component.ts");

var OrdersRoutingModule = [{
        path: '',
        component: _orders_component__WEBPACK_IMPORTED_MODULE_0__["OrdersComponent"]
    }];


/***/ })

}]);
//# sourceMappingURL=app-orders-orders-module.js.map