(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-runners-runners-module"],{

/***/ "./src/app/custom-component/custom-component.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/custom-component/custom-component.module.ts ***!
  \*************************************************************/
/*! exports provided: CustomModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomModule", function() { return CustomModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _custom_confirm_custom_confirm_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./custom-confirm/custom-confirm.component */ "./src/app/custom-component/custom-confirm/custom-confirm.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { CustomEditComponent } from './custom-edit/custom-edit.component';
// import { CustomMenuComponent } from './custom-menu/custom-menu.component';
var CustomModule = /** @class */ (function () {
    function CustomModule() {
    }
    CustomModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_2__["ThemeModule"],
            ],
            declarations: [
                _custom_confirm_custom_confirm_component__WEBPACK_IMPORTED_MODULE_3__["CustomConfirmComponent"],
            ],
            entryComponents: [
                _custom_confirm_custom_confirm_component__WEBPACK_IMPORTED_MODULE_3__["CustomConfirmComponent"],
            ]
        })
    ], CustomModule);
    return CustomModule;
}());



/***/ }),

/***/ "./src/app/custom-component/custom-confirm/custom-confirm.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/custom-component/custom-confirm/custom-confirm.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <span>{{ title }}</span>\r\n  <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  {{ description }}\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button class=\"btn btn-md btn-success\" (click)=\"onClickOk()\">Ok</button>\r\n  <button class=\"btn btn-md btn-danger\" (click)=\"closeModal()\">Cancel</button>\r\n</div>"

/***/ }),

/***/ "./src/app/custom-component/custom-confirm/custom-confirm.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/custom-component/custom-confirm/custom-confirm.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/custom-component/custom-confirm/custom-confirm.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/custom-component/custom-confirm/custom-confirm.component.ts ***!
  \*****************************************************************************/
/*! exports provided: CustomConfirmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomConfirmComponent", function() { return CustomConfirmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal-ref */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal-ref.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CustomConfirmComponent = /** @class */ (function () {
    function CustomConfirmComponent(activeModal, api_service, cookieService, toastrService, fb) {
        this.activeModal = activeModal;
        this.api_service = api_service;
        this.cookieService = cookieService;
        this.toastrService = toastrService;
        this.fb = fb;
    }
    CustomConfirmComponent.prototype.onClickOk = function () {
        this.activeModal.close();
        this.closeModal();
    };
    CustomConfirmComponent.prototype.closeModal = function () {
        this.activeModal.dismiss();
    };
    CustomConfirmComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CustomConfirmComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CustomConfirmComponent.prototype, "description", void 0);
    CustomConfirmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'custom-confirm',
            template: __webpack_require__(/*! ./custom-confirm.component.html */ "./src/app/custom-component/custom-confirm/custom-confirm.component.html"),
            styles: [__webpack_require__(/*! ./custom-confirm.component.scss */ "./src/app/custom-component/custom-confirm/custom-confirm.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"], _services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]])
    ], CustomConfirmComponent);
    return CustomConfirmComponent;
}());



/***/ }),

/***/ "./src/app/runners/completed/completed.component.html":
/*!************************************************************!*\
  !*** ./src/app/runners/completed/completed.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\r\n    <nb-card-header>\r\n        Completed Orders\r\n    </nb-card-header>\r\n\r\n    <nb-card-body>\r\n        <mat-paginator #paginator [length]=\"totalRows$ | async\" [pageIndex]=\"0\" [pageSize]=\"10\"></mat-paginator>\r\n\r\n        <mat-accordion displayMode=\"flat\" multi class=\"mat-table\">\r\n            <section matSort class=\"mat-header-row\">\r\n                <span class=\"mat-header-cell\" mat-sort-header=\"id\">Order #</span>\r\n                <span class=\"mat-header-cell\" mat-sort-header=\"customer\">Customer</span>\r\n                <span class=\"mat-header-cell\" mat-sort-header=\"paid\">Date</span>\r\n            </section>\r\n\r\n            <mat-expansion-panel *ngFor=\"let order of displayedRows$ | async\" class=\"mat-expansion-custom\">\r\n                <mat-expansion-panel-header class=\"mat-row\">\r\n                    <span class=\"mat-cell\" [ngStyle]=\"{'color': order.status == 7 ?  '#80e680' : '#fb2e2e'}\">{{order.id}}</span>\r\n                    <span class=\"mat-cell\">{{order.customer_name}}</span>\r\n                    <span class=\"mat-cell\" style=\"padding-right: 5px\">{{getDate(order)}}</span>\r\n                </mat-expansion-panel-header>\r\n                <div class=\"row\">\r\n                    <div class=\"receipt-wrapper\">\r\n                        <div *ngFor=\"let item of order.items\">\r\n                            <div class=\"receipt-checkout-items\">\r\n                                <p><span>{{item.amounts}}</span>{{item.name}}</p>\r\n                                <p>{{order.currency_symbol}}{{(item.amounts*item.price).toFixed(2)}}</p>\r\n                            </div>\r\n                        </div>\r\n                \r\n                        <div class=\"checkout-total\" style=\"border-bottom: 0;\">\r\n                            <div class=\"flex-space-between\">\r\n                                <p translate>Subtotal</p>\r\n                                <p>{{order.currency_symbol}}{{order.order_subtotal.toFixed(2)}}</p>\r\n                            </div>\r\n                            <div class=\"flex-space-between\" *ngIf=\"order.order_tax != 0\">\r\n                                <p translate>Tax</p>\r\n                                <p>{{order.currency_symbol}}{{order.order_tax.toFixed(2)}}</p>\r\n                            </div>\r\n                            <div class=\"flex-space-between\">\r\n                                <p translate>Service Fee</p>\r\n                                <p>{{order.currency_symbol}}{{order.order_service_fee.toFixed(2)}}</p>\r\n                            </div>\r\n                            <div class=\"flex-space-between\" *ngIf=\"order.order_delivery_fee != 0\">\r\n                                <p translate>Delivery Fee</p>\r\n                                <p>{{order.currency_symbol}}{{order.order_delivery_fee.toFixed(2)}}</p>\r\n                            </div>\r\n                            <div class=\"flex-space-between\" style=\"font-weight: 600;\">\r\n                                <p translate>Total</p>\r\n                                <p>{{order.currency_symbol}}{{order.order_total.toFixed(2)}}</p>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </mat-expansion-panel>\r\n        </mat-accordion>\r\n    </nb-card-body>\r\n</nb-card>"

/***/ }),

/***/ "./src/app/runners/completed/completed.component.scss":
/*!************************************************************!*\
  !*** ./src/app/runners/completed/completed.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between, .receipt-checkout-items {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between, .receipt-checkout-items {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n.receipt-wrapper {\n  padding: 10px;\n  border-radius: 4px;\n  width: 100%; }\n\n.receipt-checkout-items {\n  border-bottom: 1px solid #e9ecef;\n  padding: 7px 0; }\n\n.receipt-checkout-items p {\n  font-size: 14px;\n  margin: 0; }\n\n.receipt-checkout-items span {\n  margin-right: 5px; }\n\n.checkout-total {\n  font-size: 13px;\n  padding-top: 15px;\n  padding-bottom: 7px;\n  border-bottom: 1px solid grey; }\n\n.checkout-total p {\n  margin: 0; }\n"

/***/ }),

/***/ "./src/app/runners/completed/completed.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/runners/completed/completed.component.ts ***!
  \**********************************************************/
/*! exports provided: CompletedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletedComponent", function() { return CompletedComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_observable_of__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/observable/of */ "./node_modules/rxjs-compat/_esm5/observable/of.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_datasource_utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/datasource-utils */ "./src/app/services/datasource-utils.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../node_modules/@angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var CompletedComponent = /** @class */ (function () {
    function CompletedComponent(apiService, cookieService, toastrService, route, router) {
        this.apiService = apiService;
        this.cookieService = cookieService;
        this.toastrService = toastrService;
        this.route = route;
        this.router = router;
        this.runnerId = route.snapshot.params['runnerId'];
        this.pipe = new _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_12__["DatePipe"]('en-US');
    }
    CompletedComponent.prototype.ngOnInit = function () {
        this.getInfo();
        this.createSocket();
    };
    CompletedComponent.prototype.ngOnDestroy = function () {
        this.socket.disconnect();
    };
    CompletedComponent.prototype.createSocket = function () {
        var _this = this;
        this.socket = socket_io_client__WEBPACK_IMPORTED_MODULE_10__(_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].SERVER_URL);
        this.socket.on('message', function (data) {
            _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs('Order was updated, Refreshing data now...', data);
            _this.getInfo();
        });
        this.socket.on('onconnected', function (data) {
            _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs('Socket.io was connected, user_id = ' + data.id);
        });
        this.socket.on('disconnect', function () {
            _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].logs('Socket connection was disconnected');
        });
    };
    CompletedComponent.prototype.getInfo = function () {
        var _this = this;
        this.apiService.getCompletedOrdersByRunnerId(this.runnerId).subscribe(function (res) {
            console.log(res);
            if (res) {
                if (!res.err && res.response) {
                    var rows$ = Object(rxjs_observable_of__WEBPACK_IMPORTED_MODULE_3__["of"])(res.response);
                    _this.sortEvents$ = Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_6__["fromMatSort"])(_this.sort);
                    _this.pageEvents$ = Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_6__["fromMatPaginator"])(_this.paginator);
                    _this.totalRows$ = rows$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (rows) { return rows.length; }));
                    _this.displayedRows$ = rows$.pipe(Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_6__["sortRows"])(_this.sortEvents$), Object(_services_datasource_utils__WEBPACK_IMPORTED_MODULE_6__["paginateRows"])(_this.pageEvents$));
                }
                else {
                    _this.toastrService.error('Cannot fetch orders!');
                }
            }
            else {
                _this.toastrService.error('Cannot fetch orders!');
            }
        });
    };
    CompletedComponent.prototype.getDate = function (order) {
        return order.status == 7 ? this.transformDate(order.completed_at) : this.transformDate(order.rejected_by_user_at);
    };
    CompletedComponent.prototype.transformDate = function (time) {
        return this.pipe.transform(new Date(time * 1000), 'M/d/yy, h:mm a');
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], CompletedComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], CompletedComponent.prototype, "paginator", void 0);
    CompletedComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'completed',
            template: __webpack_require__(/*! ./completed.component.html */ "./src/app/runners/completed/completed.component.html"),
            styles: [__webpack_require__(/*! ./completed.component.scss */ "./src/app/runners/completed/completed.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_7__["CookieService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]])
    ], CompletedComponent);
    return CompletedComponent;
}());



/***/ }),

/***/ "./src/app/runners/order-detail/order-detail.component.html":
/*!******************************************************************!*\
  !*** ./src/app/runners/order-detail/order-detail.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n    <h5 class=\"modal-title\">Order # {{order.order_id}}</h5>\r\n    <i class=\"nb-close close-modal\" (click)=\"closeModal()\"></i>\r\n</div>\r\n<div class=\"modal-body\">\r\n    <div>\r\n        <div class=\"row-detail\">\r\n            <p>Customer</p>\r\n            <p>{{order.customer_name}}</p>\r\n            <a *ngIf=\"showPhone\" href=\"tel://{{order.phone}}\"><i class=\"fa fa-phone callIcon\"></i></a>\r\n        </div>\r\n        <div class=\"row-detail\">\r\n            <p>Location</p>\r\n            <p>{{order.customer_location}}</p>\r\n        </div>\r\n        <div class=\"row-detail\">\r\n            <p>Date</p>\r\n            <p>{{ (order.created_at * 1000) | date:'medium' }}</p>\r\n        </div>\r\n    </div>\r\n    <div class=\"receipt-wrapper\">\r\n        <div>\r\n            <div class=\"receipt-checkout-items notes\">\r\n              <p>Devlivery Notes : {{(order.notes)?order.notes:'None'}}.</p>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"checkout-total\" style=\"border-bottom: 0;position:relative\">\r\n            <!-- <div class=\"flex-space-between\">\r\n                <textarea class=\"from-control\" disabled=\"true\" id=\"comment\" >{{order.notes}}</textarea>\r\n           </div> -->\r\n           <div class=\"flex-space-between yourComments\">\r\n            <textarea class=\"from-control\"  disabled=\"true\"  id=\"comment\" [(ngModel)]=\"comment\" placeholder=\"Your Comments\"></textarea>\r\n            <i class=\"nb-edit\" (click)=\"commentbox()\"></i>\r\n       </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"receipt-wrapper\">\r\n        <div *ngFor=\"let item of order.items\">\r\n            <div class=\"receipt-checkout-items\">\r\n                <p><span>{{item.amounts}}</span>{{item.name}}</p>\r\n                <p>{{order.currency_symbol}}{{(item.amounts*item.price).toFixed(2)}}</p>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"checkout-total\" style=\"border-bottom: 0;\">\r\n            <div class=\"flex-space-between\">\r\n                <p translate>Subtotal</p>\r\n                <p>{{order.currency_symbol}}{{order.order_subtotal.toFixed(2)}}</p>\r\n            </div>\r\n            <div class=\"flex-space-between\" *ngIf=\"order.order_tax != 0\">\r\n                <p translate>Tax</p>\r\n                <p>{{order.currency_symbol}}{{order.order_tax.toFixed(2)}}</p>\r\n            </div>\r\n            <div class=\"flex-space-between\">\r\n                <p translate>Service Fee</p>\r\n                <p>{{order.currency_symbol}}{{order.order_service_fee.toFixed(2)}}</p>\r\n            </div>\r\n            <div class=\"flex-space-between\"  *ngIf=\"order.order_delivery_fee != 0\">\r\n                <p translate>Delivery Fee</p>\r\n                <p>{{order.currency_symbol}}{{order.order_delivery_fee.toFixed(2)}}</p>\r\n            </div>\r\n            <div class=\"flex-space-between\" style=\"font-weight: 600;\">\r\n                <p translate>Total</p>\r\n                <p>{{order.currency_symbol}}{{order.order_total.toFixed(2)}}</p>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"modal-footer\" style=\"justify-content: space-between;\">\r\n    <button *ngIf=\"order.status == 5\" mat-raised-button style=\"width: 120px; background-color:#006400;\" \r\n        (click)=\"this.updateOrderStatus(7);this.closeModal()\">Delivered</button>\r\n    <button *ngIf=\"order.status == 5\" mat-raised-button style=\"width: 120px; background-color:#ff781f;\" \r\n        (click)=\"this.updateOrderStatus(8);this.closeModal()\">No Answer</button>\r\n    <button *ngIf=\"order.status == 5\" mat-raised-button style=\"width: 120px; background-color:#9c1818; color:white;\" \r\n        (click)=\"this.updateOrderStatus(8);this.closeModal()\">Refused</button>\r\n  \r\n        \r\n</div>"

/***/ }),

/***/ "./src/app/runners/order-detail/order-detail.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/runners/order-detail/order-detail.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between, .row-detail, .receipt-checkout-items {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between, .row-detail, .receipt-checkout-items {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n.runner-chooser {\n  width: 100%;\n  padding: 10px;\n  border: 1px solid #eaeef2;\n  border-radius: 4px;\n  margin: 15px 0; }\n\n.row-detail p {\n  margin: 5px 0; }\n\n.row-detail p:first-child {\n  width: 25%;\n  font-weight: bold; }\n\n.row-detail p:nth-child(2) {\n  width: 75%; }\n\n.receipt-wrapper {\n  padding: 10px;\n  border: 1px solid #663ab7;\n  border-radius: 4px;\n  margin-top: 10px;\n  color: white; }\n\n.receipt-checkout-items {\n  border-bottom: 1px solid #663ab7;\n  padding: 7px 0; }\n\n.receipt-checkout-items p {\n  font-size: 14px;\n  margin: 0; }\n\n.receipt-checkout-items span {\n  margin-right: 5px; }\n\n.checkout-total {\n  font-size: 13px;\n  padding-top: 15px;\n  padding-bottom: 7px;\n  border-bottom: 1px solid grey; }\n\n.checkout-total p {\n  margin: 0; }\n\n.modal-body {\n  color: white; }\n\ni.fa.fa-phone.callIcon {\n  font-size: 25px;\n  /* width: 20px; */\n  margin-right: 6px; }\n\ntextarea#comment {\n  width: 100%;\n  min-height: 90px;\n  border-radius: 10px;\n  padding-left: 10px;\n  padding-top: 10px;\n  background: transparent;\n  color: #fff;\n  border: 1px solid #663ab7; }\n\n.yourComments {\n  position: relative; }\n\n.yourComments .nb-edit {\n    position: absolute;\n    top: 8px;\n    left: 110px;\n    font-size: 25px; }\n\n.receipt-checkout-items.notes {\n  border: 1px solid #663ab7;\n  border-radius: 10px;\n  padding-left: 10px;\n  min-height: 85px;\n  display: flex;\n  align-items: flex-start;\n  padding-top: 8px; }\n\ntextarea::-webkit-input-placeholder {\n  color: #fff; }\n\ntextarea:-ms-input-placeholder {\n  color: #fff; }\n\ntextarea::-ms-input-placeholder {\n  color: #fff; }\n\ntextarea::placeholder {\n  color: #fff; }\n\ntextarea#comment:focus {\n  background: #fff;\n  color: black;\n  outline: 0; }\n"

/***/ }),

/***/ "./src/app/runners/order-detail/order-detail.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/runners/order-detail/order-detail.component.ts ***!
  \****************************************************************/
/*! exports provided: OrderDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailComponent", function() { return OrderDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal-ref */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal-ref.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OrderDetailComponent = /** @class */ (function () {
    function OrderDetailComponent(activeModal, apiService, toastrService) {
        this.activeModal = activeModal;
        this.apiService = apiService;
        this.toastrService = toastrService;
        this.showPhone = false;
    }
    OrderDetailComponent.prototype.ngOnInit = function () {
        // Utils.logs(this.order)
        if (this.order.status == 5 || this.order.status == 6 || this.order.status == 7) {
            this.showPhone = true;
        }
    };
    OrderDetailComponent.prototype.updateOrderStatus = function (status) {
        var _this = this;
        var data = {
            notes: this.comment,
            order_id: this.order.order_id
        };
        this.apiService.updateOrderStatus(this.order.order_id, status).subscribe(function (res) {
            // Utils.logs(res);
            if (res) {
                if (!res.err) {
                    _this.toastrService.info('Updated successfully');
                    _this.order.status = status;
                    _this.apiService.post("api/order/runner/" + data).subscribe(function (res) {
                    });
                    _this.updatePendingOrdersCount();
                }
                else {
                    _this.toastrService.error('Update Failed!');
                }
            }
            else {
                _this.toastrService.error('Update Failed!');
            }
        });
    };
    OrderDetailComponent.prototype.updatePendingOrdersCount = function () {
        this.apiService.updatePendingOrders(this.order.runner_id).subscribe(function (res) {
            if (res.err) {
                // console.error(res.err);
            }
        });
    };
    OrderDetailComponent.prototype.closeModal = function () {
        this.activeModal.close();
    };
    OrderDetailComponent.prototype.commentbox = function () {
        var element = document.getElementById("comment");
        element.disabled = false;
        document.getElementById("comment").focus();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], OrderDetailComponent.prototype, "order", void 0);
    OrderDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'order-detail',
            template: __webpack_require__(/*! ./order-detail.component.html */ "./src/app/runners/order-detail/order-detail.component.html"),
            styles: [__webpack_require__(/*! ./order-detail.component.scss */ "./src/app/runners/order-detail/order-detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal_ref__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"],
            _services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]])
    ], OrderDetailComponent);
    return OrderDetailComponent;
}());



/***/ }),

/***/ "./src/app/runners/pending/pending.component.html":
/*!********************************************************!*\
  !*** ./src/app/runners/pending/pending.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nb-card>\r\n  <agm-map *ngIf=\"showMap\" style=\"height:352px; z-index: 1!important;\"  [latitude]=\"lat\" [longitude]=\"long\" \r\n  [zoom]=\"15\" [zoomControl]=\"true\" [fullscreenControl]=\"true\" [fullscreenControlOptions]=\"screenOptions\" [streetViewControl]=\"false\" [minZoom]=\"10\">\r\n  <div *ngIf=\"pOrders && pOrders.length>0\" >\r\n    <agm-marker *ngFor=\"let order of pOrders\" [latitude]=\"order.latitude\" \r\n      [longitude]=\"order.longitude\" [iconUrl]=\"order.icon\"  (markerClick)=\"orderDetailsModal(order,'MapModal')\">\r\n  </agm-marker>\r\n  </div>\r\n</agm-map>\r\n  <nb-card-header>\r\n     <!-- <p>{{currentUser.first_name?currentUser.first_name:'Runner1'}}  {{currentUser.last_name?currentUser.last_name:''}}</p> -->\r\n    <div class=\"flex-space-between\">\r\n      <p style=\"margin:0\">Active Orders</p> \r\n      <!-- <img src=\"../../../assets/images/volume_off.png\" alt=\"volume_off\" id=\"btn-enable-audio\" (click)=\"enableAudio()\" *ngIf=\"showEnableAudio\"> -->\r\n      <!-- <img src=\"../../../assets/images/volume_up.png\" alt=\"volume_up\" (click)=\"disableAudio()\" *ngIf=\"!showEnableAudio\"> -->\r\n    </div>\r\n    <p class=\"date-span\" style=\"margin:0\">(Last Updated on {{today | date:'EE h:mm a'}})</p>\r\n  </nb-card-header>\r\n\r\n  <nb-card-body class=\"contactTable \">\r\n    <ng2-smart-table class=\"matTable\" [settings]=\"settings\"  (userRowSelect)=\"orderDetailsModal($event,'RowModal')\"  [source]=\"source\"></ng2-smart-table>\r\n  </nb-card-body>\r\n</nb-card>\r\n\r\n<!-- <audio src=\"../../assets/sounds/WoopWoop.wav\" id=\"audio\"></audio> -->"

/***/ }),

/***/ "./src/app/runners/pending/pending.component.scss":
/*!********************************************************!*\
  !*** ./src/app/runners/pending/pending.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.flex-start {\n  display: flex;\n  justify-content: flex-start; }\n\n.flex-space-between {\n  display: flex;\n  justify-content: space-between;\n  align-items: center; }\n\n.flex-space-around {\n  display: flex;\n  justify-content: space-around;\n  align-items: center; }\n\n.flex-center {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.flex-end {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n\ninput:focus {\n  outline: none; }\n\n.date-span {\n  font-size: 14px;\n  color: gray; }\n\n.select-client mat-select {\n  border-bottom: 2px solid;\n  font-size: 17px; }\n\np {\n  margin-bottom: 0; }\n\n.close-modal {\n  font-size: 30px;\n  font-weight: bold;\n  cursor: pointer; }\n\n/* fix for modal with mat-select */\n\n::ng-deep .cdk-overlay-container {\n  z-index: 9999 !important; }\n\n::ng-deep .nb-theme-default .modal-footer {\n  padding: 20px; }\n\n::ng-deep .nb-theme-default ng2-smart-table table tr th,\n::ng-deep .nb-theme-default ng2-smart-table table tr td {\n  padding: 7px 12px !important;\n  background-color: indigo; }\n\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active a:focus,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:hover,\n::ng-deep .nb-theme-default ng2-smart-table nav.ng2-smart-pagination-nav .pagination li.active > span:focus {\n  background-color: #280f54; }\n\n::ng-deep .mat-select-placeholder,\n::ng-deep .mat-select-arrow,\n::ng-deep .mat-select-value {\n  color: white; }\n\n::ng-deep .nb-theme-default ::ng-deep .dropdown.show ::ng-deep .dropdown-menu.show, ::ng-deep .nb-theme-default ::ng-deep .dropup.show ::ng-deep .dropdown-menu.show {\n  right: 0px !important;\n  width: 250px;\n  top: 41px !important;\n  background: #fff;\n  max-height: 206px;\n  overflow-y: scroll;\n  border-top: 0px;\n  padding-top: 13px; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary {\n  background-color: #ffffff;\n  color: indigo;\n  font-size: 0.8rem;\n  text-transform: capitalize; }\n\n::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:hover, ::ng-deep .nb-theme-default ::ng-deep .btn.btn-primary:focus {\n  background-color: #4b0082; }\n\n::ng-deep .main-content {\n  position: relative; }\n\n::ng-deep .nb-theme-default .btn.btn-primary:active {\n  color: #ffffff;\n  background-color: #4b0082 !important;\n  border-color: transparent;\n  box-shadow: none; }\n\n.mat-cell, .mat-header-cell {\n  flex: 1;\n  overflow: hidden;\n  word-wrap: break-word;\n  font-size: 15px;\n  color: white; }\n\n.mat-accordion .mat-header-row {\n  padding-left: 1.5rem;\n  padding-right: 2rem;\n  border-bottom: none; }\n\n.mat-expansion-panel-header.mat-row {\n  border-bottom: none;\n  background-color: indigo;\n  color: white; }\n\n.mat-expansion-panel {\n  background-color: #280f54;\n  color: white; }\n\n::ng-deep .mat-expansion-indicator::after {\n  color: white !important; }\n\n.mat-expansion-custom {\n  box-shadow: none !important;\n  border-bottom: 1px solid #663ab7;\n  border-radius: 4px; }\n\n.mat-table {\n  display: block;\n  background-color: indigo;\n  color: white; }\n\nmat-paginator {\n  background-color: indigo;\n  color: white; }\n\n.mat-header-row {\n  min-height: 56px;\n  border-bottom: 1px solid #663ab7 !important;\n  border-top: 1px solid #663ab7; }\n\n.mat-row {\n  min-height: 58px;\n  align-items: center; }\n\n.mat-row, .mat-header-row {\n  display: flex;\n  border-bottom-width: 1px;\n  border-bottom-style: solid;\n  align-items: center;\n  padding: 0 24px;\n  box-sizing: border-box; }\n\n.mat-row::after, .mat-header-row::after {\n    display: inline-block;\n    min-height: inherit;\n    content: ''; }\n\n::ng-deep .nb-theme-default .modal-header {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n.matTable table > tbody > tr > td, .matTable table > thead > tr > th {\n  position: relative;\n  padding: 0.875rem 1.25rem;\n  vertical-align: middle;\n  border-left: 0px solid;\n  border-right: 0px solid; }\n\n.matTable table > tbody > tr > td:nth-child(1) {\n  position: relative;\n  text-decoration: underline; }\n\nbutton.gm-control-active.gm-fullscreen-control {\n  left: 0;\n  top: unset !important;\n  bottom: 23px !important; }\n\n::ng-deep .nb-theme-default ng2-smart-table tbody tr:nth-child(2n) {\n  background-color: white; }\n\n:host /deep/ tr.pending {\n  background-color: white !important; }\n\n:host /deep/ tr.ready2pickup {\n  background-color: #f5f7fc !important; }\n\n.btn-custom {\n  padding: 0 4px !important;\n  font-size: 2rem !important;\n  background-color: white;\n  color: #280f54;\n  border-radius: 4px; }\n\n@media only screen and (max-device-width: 767px) and (min-device-width: 320px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2) {\n  .layout-container {\n    margin-top: 8em; } }\n"

/***/ }),

/***/ "./src/app/runners/pending/pending.component.ts":
/*!******************************************************!*\
  !*** ./src/app/runners/pending/pending.component.ts ***!
  \******************************************************/
/*! exports provided: ButtonViewComponent, PendingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonViewComponent", function() { return ButtonViewComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PendingComponent", function() { return PendingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ng2_smart_table_lib_data_source_local_local_data_source__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table/lib/data-source/local/local.data-source */ "./node_modules/ng2-smart-table/lib/data-source/local/local.data-source.js");
/* harmony import */ var _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../order-detail/order-detail.component */ "./src/app/runners/order-detail/order-detail.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap/modal/modal */ "./node_modules/@ng-bootstrap/ng-bootstrap/modal/modal.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../common */ "./src/app/common.ts");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_observable_IntervalObservable__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/observable/IntervalObservable */ "./node_modules/rxjs-compat/_esm5/observable/IntervalObservable.js");
/* harmony import */ var _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../node_modules/@angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs_add_operator_takeWhile__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs/add/operator/takeWhile */ "./node_modules/rxjs-compat/_esm5/add/operator/takeWhile.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var ButtonViewComponent = /** @class */ (function () {
    function ButtonViewComponent(modalService, apiService) {
        this.modalService = modalService;
        this.apiService = apiService;
        this.onUpdateStatus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ButtonViewComponent.prototype.ngOnInit = function () {
    };
    ButtonViewComponent.prototype.onClick = function () {
        var activeModal = this.modalService.open(_order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_4__["OrderDetailComponent"], { size: 'lg', container: 'nb-layout' });
        activeModal.componentInstance.order = this.rowData;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ButtonViewComponent.prototype, "value", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ButtonViewComponent.prototype, "rowData", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], ButtonViewComponent.prototype, "onUpdateStatus", void 0);
    ButtonViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'button-view',
            styles: [__webpack_require__(/*! ./pending.component.scss */ "./src/app/runners/pending/pending.component.scss")],
            template: "<div class=\"flex-center\"><button class=\"btn-custom\" (click)=\"onClick()\"><i class=\"nb-menu\"></i></button></div>",
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_5__["NgbModal"], _services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], ButtonViewComponent);
    return ButtonViewComponent;
}());

var PendingComponent = /** @class */ (function () {
    function PendingComponent(modalService, apiService, toastrService, route, router) {
        this.modalService = modalService;
        this.apiService = apiService;
        this.toastrService = toastrService;
        this.route = route;
        this.router = router;
        this.onUpdateStatus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.showMap = false;
        this.lat = 40.76148;
        this.long = -73.9856;
        this.settings = {
            mode: 'inline',
            hideSubHeader: true,
            actions: {
                add: false,
                edit: false,
                delete: false,
                save: false,
                cancel: false,
            },
            columns: {
                order_id: {
                    title: 'Order',
                    type: 'string',
                    filter: true,
                    sort: true,
                },
                customer_name: {
                    title: 'Customer',
                    type: 'string',
                    filter: false,
                    sort: true,
                },
                customer_location: {
                    title: 'Location',
                    type: 'string',
                    filter: false,
                    sort: true,
                },
            },
            rowClassFunction: function (row) {
                if (row.data.status === 1 || row.data.status === 2) {
                    return 'pending';
                }
                else if (row.data.status === 3 || row.data.status === 4) {
                    return 'ready2pickup';
                }
            }
        };
        this.source = new ng2_smart_table_lib_data_source_local_local_data_source__WEBPACK_IMPORTED_MODULE_3__["LocalDataSource"]();
        this.pastOrders = [];
        this.alive = true;
        this.pOrdersOrder_Id = [];
        this.newOrdersOrder_Id = [];
        this.pipe = new _node_modules_angular_common__WEBPACK_IMPORTED_MODULE_11__["DatePipe"]('en-US');
        this.runnerId = route.snapshot.params['runner_id'];
        console.log("rrrr", this.runnerId);
        localStorage.setItem('runnerId', this.runnerId);
    }
    PendingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.audio = new Audio();
        this.createSocket();
        this.today = Date.now();
        this.hideSoundBtn = localStorage.getItem('hideSoundBtn');
        this.showEnableAudio = true;
        this.getInfo();
        // get pending orders every 10 seconds only for runners
        rxjs_observable_IntervalObservable__WEBPACK_IMPORTED_MODULE_10__["IntervalObservable"].create(10000)
            .takeWhile(function () { return _this.alive; }) // only fires when component is alive
            .subscribe(function () {
            _this.getInfo();
            _this.today = Date.now();
        });
    };
    PendingComponent.prototype.ngOnDestroy = function () {
        this.socket.disconnect();
        this.alive = false;
    };
    PendingComponent.prototype.createSocket = function () {
        var _this = this;
        this.socket = socket_io_client__WEBPACK_IMPORTED_MODULE_8__(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].SERVER_URL);
        this.socket.on('message', function (data) {
            _common__WEBPACK_IMPORTED_MODULE_7__["Utils"].logs('Order was updated, Refreshing data now...', data);
            _this.getInfo();
        });
        this.socket.on('onconnected', function (data) {
            _common__WEBPACK_IMPORTED_MODULE_7__["Utils"].logs('Socket.io was connected, user_id = ' + data.id);
        });
        this.socket.on('disconnect', function () {
            _common__WEBPACK_IMPORTED_MODULE_7__["Utils"].logs('Socket connection was disconnected');
        });
    };
    PendingComponent.prototype.getInfo = function () {
        var _this = this;
        this.apiService.get("order/pending/runner/5").subscribe(function (res) {
            console.log("orders", res.response);
            _common__WEBPACK_IMPORTED_MODULE_7__["Utils"].logs(res);
            if (res) {
                if (!res.err) {
                    var newOrders = res.response;
                    _this.source.load(newOrders);
                    var newIds = newOrders.map(function (order) { return order.id; }).slice();
                    var pastIds = (_this.pastOrders && _this.pastOrders.map(function (order) { return order.id; }).slice()) || [];
                    if (JSON.stringify(newIds) !== JSON.stringify(pastIds)
                        || !_this.pastOrders) {
                        _this.updateMap(newOrders);
                    }
                    _this.pastOrders = newOrders;
                }
                else {
                    _this.toastrService.error('Cannot fetch orders!');
                }
            }
            else {
                _this.toastrService.error('Cannot fetch orders!');
            }
        });
    };
    PendingComponent.prototype.updateOrderStatus = function (order_id, status) {
        var _this = this;
        this.apiService.updateOrderStatus(order_id, status).subscribe(function (res) {
            // Utils.logs(res);
            if (res) {
                if (!res.err) {
                    _this.toastrService.info('Updated successfully');
                }
                else {
                    _this.toastrService.error('Update Failed!');
                }
            }
            else {
                _this.toastrService.error('Update Failed!');
            }
        });
    };
    PendingComponent.prototype.updateMap = function (newOrders) {
        var _this = this;
        this.pOrders = newOrders.slice();
        var size = 20;
        if (this.pOrders && this.pOrders.length > 0) {
            this.pOrders.forEach(function (order, i) {
                if (i == 0) {
                    _this.lat = order.latitude;
                    _this.long = order.longitude;
                }
                order.icon = {
                    url: "https://paranoid-cdn.s3-us-west-1.amazonaws.com/uploads/purple-dot.png",
                    scaledSize: {
                        height: size,
                        width: size
                    },
                };
                order.labelOptions = {
                    text: "" + order.order_id,
                    color: 'white', fontSize: '12px', fontWeight: 'bold'
                };
            });
        }
        this.showMap = true;
    };
    PendingComponent.prototype.orderDetailsModal = function (order, isModal) {
        var activeModal = this.modalService.open(_order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_4__["OrderDetailComponent"], { size: 'lg', container: 'nb-layout' });
        if (isModal === "MapModal") {
            activeModal.componentInstance.order = order;
        }
        else {
            activeModal.componentInstance.order = order.data;
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], PendingComponent.prototype, "onUpdateStatus", void 0);
    PendingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'pending',
            template: __webpack_require__(/*! ./pending.component.html */ "./src/app/runners/pending/pending.component.html"),
            styles: [__webpack_require__(/*! ./pending.component.scss */ "./src/app/runners/pending/pending.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap_modal_modal__WEBPACK_IMPORTED_MODULE_5__["NgbModal"],
            _services_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], PendingComponent);
    return PendingComponent;
}());



/***/ }),

/***/ "./src/app/runners/runners-menu.ts":
/*!*****************************************!*\
  !*** ./src/app/runners/runners-menu.ts ***!
  \*****************************************/
/*! exports provided: MENU_ITEMS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MENU_ITEMS", function() { return MENU_ITEMS; });
var MENU_ITEMS = [
    {
        title: 'Active Orders',
        icon: 'nb-compose',
        link: "/runners/pending/",
        home: true,
    },
    {
        title: 'Completed Orders',
        icon: 'nb-checkmark',
        link: "/runners/completed/",
    },
];


/***/ }),

/***/ "./src/app/runners/runners-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/runners/runners-routing.module.ts ***!
  \***************************************************/
/*! exports provided: RunnersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RunnersRoutingModule", function() { return RunnersRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _runners_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./runners.component */ "./src/app/runners/runners.component.ts");
/* harmony import */ var _pending_pending_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pending/pending.component */ "./src/app/runners/pending/pending.component.ts");
/* harmony import */ var _completed_completed_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./completed/completed.component */ "./src/app/runners/completed/completed.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [{
        path: '',
        component: _runners_component__WEBPACK_IMPORTED_MODULE_2__["RunnersComponent"],
        children: [{
                path: 'pending/:runnerId',
                component: _pending_pending_component__WEBPACK_IMPORTED_MODULE_3__["PendingComponent"],
            }, {
                path: 'completed/:runnerId',
                component: _completed_completed_component__WEBPACK_IMPORTED_MODULE_4__["CompletedComponent"],
            }, {
                path: '**',
                component: _pending_pending_component__WEBPACK_IMPORTED_MODULE_3__["PendingComponent"],
            }],
    }];
var RunnersRoutingModule = /** @class */ (function () {
    function RunnersRoutingModule() {
    }
    RunnersRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], RunnersRoutingModule);
    return RunnersRoutingModule;
}());



/***/ }),

/***/ "./src/app/runners/runners.component.ts":
/*!**********************************************!*\
  !*** ./src/app/runners/runners.component.ts ***!
  \**********************************************/
/*! exports provided: RunnersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RunnersComponent", function() { return RunnersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _runners_menu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./runners-menu */ "./src/app/runners/runners-menu.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RunnersComponent = /** @class */ (function () {
    function RunnersComponent(cookieService) {
        this.cookieService = cookieService;
        this.menu = _runners_menu__WEBPACK_IMPORTED_MODULE_1__["MENU_ITEMS"];
    }
    RunnersComponent.prototype.ngOnInit = function () {
        this.cookieService.delete('user', '/');
        var runnerId = localStorage.getItem('runnerId');
        this.menu.forEach(function (element) {
            element.link += runnerId;
        });
    };
    RunnersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'runners',
            template: "\n    <food-ordering-layout>\n      <nb-menu [items]=\"menu\"></nb-menu>\n      <router-outlet></router-outlet>\n    </food-ordering-layout>\n  ",
        }),
        __metadata("design:paramtypes", [ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__["CookieService"]])
    ], RunnersComponent);
    return RunnersComponent;
}());



/***/ }),

/***/ "./src/app/runners/runners.module.ts":
/*!*******************************************!*\
  !*** ./src/app/runners/runners.module.ts ***!
  \*******************************************/
/*! exports provided: RunnerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RunnerModule", function() { return RunnerModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _pending_pending_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pending/pending.component */ "./src/app/runners/pending/pending.component.ts");
/* harmony import */ var _completed_completed_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./completed/completed.component */ "./src/app/runners/completed/completed.component.ts");
/* harmony import */ var _runners_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./runners.component */ "./src/app/runners/runners.component.ts");
/* harmony import */ var _runners_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./runners-routing.module */ "./src/app/runners/runners-routing.module.ts");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./order-detail/order-detail.component */ "./src/app/runners/order-detail/order-detail.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _custom_component_custom_component_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../custom-component/custom-component.module */ "./src/app/custom-component/custom-component.module.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var RunnerModule = /** @class */ (function () {
    function RunnerModule() {
    }
    RunnerModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _runners_routing_module__WEBPACK_IMPORTED_MODULE_5__["RunnersRoutingModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_6__["ThemeModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_7__["Ng2SmartTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatSelectModule"],
                _custom_component_custom_component_module__WEBPACK_IMPORTED_MODULE_10__["CustomModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_11__["AgmCoreModule"]
            ],
            declarations: [
                _pending_pending_component__WEBPACK_IMPORTED_MODULE_2__["PendingComponent"],
                _completed_completed_component__WEBPACK_IMPORTED_MODULE_3__["CompletedComponent"],
                _runners_component__WEBPACK_IMPORTED_MODULE_4__["RunnersComponent"],
                _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_8__["OrderDetailComponent"],
                _pending_pending_component__WEBPACK_IMPORTED_MODULE_2__["ButtonViewComponent"],
                _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_8__["OrderDetailComponent"],
            ],
            entryComponents: [
                _pending_pending_component__WEBPACK_IMPORTED_MODULE_2__["ButtonViewComponent"],
                _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_8__["OrderDetailComponent"],
            ]
        })
    ], RunnerModule);
    return RunnerModule;
}());



/***/ })

}]);
//# sourceMappingURL=app-runners-runners-module.js.map